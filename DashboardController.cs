﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;


namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class DashboardController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public DashboardController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }


        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<BankDashboardViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult GetBank(int page, int pageSize)
        {
            int previous = ((DateTime.UtcNow.AddHours(5).AddMinutes(45).Month + 10) % 12) + 1;

            int current = DateTime.UtcNow.AddHours(5).AddMinutes(45).Month;

            try
            {
                
                var bank = _unitOfWork.Banks.GetAll().GroupJoin(_unitOfWork.Deployments.GetAll(), a => a.Id, b => b.BankId, (a, b) => new BankDashboardViewModel
                {
                    Id = a.Id,
                    BankName = a.BankName,
                    Total = b.Select(s => s.Id).Count(),
                    PreviousMonth = b.Where(s => s.DeployDate.Month == previous && s.Verification == "Verified").Select(s => s.Id).Count(),
                    CurrentMonth = b.Where(s => s.DeployDate.Month == current && s.Verification == "Verified").Select(s => s.Id).Count(),
                    PendingDeployment = b.Where(s => s.Verification == "Pending").Select(s => s.Id).Count(),
                    GEM = b.Where(s => s.TertiaryItem == "GEM" && s.Verification == "Verified").Select(s => s.Id).Count(),
                    EM = b.Where(s => s.TertiaryItem == "EM" && s.Verification == "Verified").Select(s => s.Id).Count(),
                    IWL = b.Where(s => s.TertiaryItem == "IWL" && s.Verification == "Verified").Select(s => s.Id).Count(),

                    
                }).GroupJoin(_unitOfWork.PosStocks.GetAll(), a => a.Id, b => b.BankId, (a, b) => new BankDashboardViewModel
                {
                    Id = a.Id,
                    BankName = a.BankName,
                    Total = a.Total,
                    PreviousMonth = a.PreviousMonth,
                    CurrentMonth = a.CurrentMonth,
                    PendingDeployment = a.PendingDeployment,
                    GEM = a.GEM + b.Where(s => s.TertiaryItem == "GEM").Select(s => s.Id).Count(),
                    EM = a.EM + b.Where(s => s.TertiaryItem == "EM").Select(s => s.Id).Count(),
                    IWL = a.IWL + b.Where(s => s.TertiaryItem == "IWL").Select(s => s.Id).Count(),
                   
                });

                var maintenance = _unitOfWork.Maintenances.GetAll().GroupJoin(_unitOfWork.DeploymentRequests.GetAll(), a => a.DeploymentRequestId, b => b.Id, (a, b) => new BankDashboardViewModel
                {
                    Id = a.Id,
                    BankId = b.Select(s => s.BankId).SingleOrDefault(),
                    InventoryId = b.Select(s => s.InventoryId).SingleOrDefault(),
                }).GroupJoin(_unitOfWork.Inventorys.GetAll(), a => a.InventoryId, b => b.Id, (a, b) => new BankDashboardViewModel
                {
                    Id = a.Id,
                    BankId = a.BankId,
                    InventoryId = a.InventoryId,
                    
                });

                var roll = _unitOfWork.OtherStocks.GetAll().GroupJoin(_unitOfWork.DeploymentRequests.GetAll(), a => a.DeploymentRequestId, b => b.Id, (a, b) => new BankDashboardViewModel
                {
                    Id = a.Id,
                    BankId = b.Select(s => s.BankId).SingleOrDefault(),
                    SoldNumber=a.SoldOut

                });

                var bankDashboard = bank.GroupJoin(maintenance, a => a.Id, b => b.BankId, (a, b) => new BankDashboardViewModel
                {
                    Id = a.Id,
                    BankName = a.BankName,
                    Total = a.Total,
                    PreviousMonth = a.PreviousMonth,
                    CurrentMonth = a.CurrentMonth,
                    PendingDeployment = a.PendingDeployment,
                    GEM = a.GEM,
                    EM = a.EM,
                    IWL = a.IWL,
                    //TotalGem = a.TotalGem,
                    //TotalEm = a.TotalEm,
                    //TotalIwl = a.TotalIwl,
                    MaintenanceId = b.Select(s => s.Id).SingleOrDefault(),
                    BankId = b.Select(s => s.BankId).SingleOrDefault(),
                    //Gem = b.Select(s => s.Gem).FirstOrDefault(),
                    //Em = b.Select(s => s.Em).FirstOrDefault(),
                    //Iwl = b.Select(s => s.Iwl).FirstOrDefault(),
                    MaintenanceNumber = b.Where(x => x.BankId == a.Id).Select(s => s.Id).Count(),
                    
                }).GroupJoin(roll, a => a.Id, b => b.BankId, (a, b) => new BankDashboardViewModel
                {
                    Id = a.Id,
                    BankName = a.BankName,
                    Total = a.Total,
                    PreviousMonth = a.PreviousMonth,
                    CurrentMonth = a.CurrentMonth,
                    PendingDeployment = a.PendingDeployment,
                    GEM = a.GEM,
                    EM = a.EM,
                    IWL = a.IWL,
                   
                    MaintenanceId = a.MaintenanceId,
                    BankId =a.BankId,
                    
                    MaintenanceNumber = a.MaintenanceNumber,
                    RollNumber=b.Where(x=>x.BankId==a.Id).Select(s=>s.SoldNumber).Sum()

                });

                return Ok(bankDashboard);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }


        [HttpGet("notification/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<BankDashboardViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult GetNotifiy(int page, int pageSize)
        {
            try
            {
                var notification = _unitOfWork.Banks.GetAll().GroupJoin(_unitOfWork.DeploymentRequests.GetAll(), a => a.Id, b => b.BankId, (a, b) => new BankDashboardViewModel
                {
                    Id = a.Id,
                    BankName = a.BankName,
                    DeploymentRequestId = b.Where(s => s.Verification == "Pending" || s.Verification == "Verified").Select(s => s.Id).SingleOrDefault()


                });



                return Ok(notification);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }
        [HttpGet("stock/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<StockViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult GetStock(int page, int pageSize)
        {
            try
            {


                var totalStock = _unitOfWork.Inventorys.GetAll().Where(x => x.HasSerialNo == true).GroupJoin(_unitOfWork.InventorysAdd.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new StockViewModel
                {
                    Id = a.Id,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    Total = _unitOfWork.InventorysAdd.GetAll().Where(s => s.InventoryId == a.Id).Select(s => s.TotalNumber).Sum()
                }).GroupJoin(_unitOfWork.Configurations.GetAll().Where(x => x.Verification == "Verified" || x.Verification == "Pending"), a => a.Id, b => b.InventoryId, (a, b) => new StockViewModel
                {
                    Id = a.Id,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    Total = a.Total,
                    TotalDeploy = _unitOfWork.Configurations.GetAll().Where(s => s.InventoryId == a.Id).Select(s => s.Id).Count()
                }).GroupJoin(_unitOfWork.PosStocks.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new StockViewModel
                {
                    Id = a.Id,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    Total = a.Total,
                    TotalDeploy = a.TotalDeploy,
                    TotalSold = _unitOfWork.PosStocks.GetAll().Where(s => s.InventoryId == a.Id).Select(s => s.Id).Count(),
                    TotalSoldDeploy = _unitOfWork.PosStocks.GetAll().Where(s => s.InventoryId == a.Id && s.Status == "undeployed").Select(s => s.Id).Count()

                }).GroupJoin(_unitOfWork.Maintenances.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new StockViewModel
                {
                    Id = a.Id,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    Total = a.Total,
                    TotalDeploy = a.TotalDeploy,
                    TotalSold = a.TotalSold,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    TotalMaintenance = _unitOfWork.Maintenances.GetAll().Where(s => s.InventoryId == a.Id).Select(s => s.Id).Count()

                }).GroupJoin(_unitOfWork.NotReceivedPos.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new StockViewModel
                {
                    Id = a.Id,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    Total = a.Total,
                    TotalDeploy = a.TotalDeploy,
                    TotalSold = a.TotalSold,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    TotalMaintenance = a.TotalMaintenance,
                    TotalNotReceived = _unitOfWork.NotReceivedPos.GetAll().Where(s => s.InventoryId == a.Id).Select(s => s.Id).Count()

                }).GroupJoin(_unitOfWork.DamagePos.GetAll(), a => a.Id, b => b.InventoryId, (a, b) => new StockViewModel
                {
                    Id = a.Id,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    Total = a.Total,
                    TotalDeploy = a.TotalDeploy,
                    TotalSoldDeploy = a.TotalSoldDeploy,
                    TotalSold = a.TotalSold,
                    TotalMaintenance = a.TotalMaintenance,
                    TotalNotReceived = a.TotalNotReceived,
                    TotalDamage = _unitOfWork.DamagePos.GetAll().Where(s => s.InventoryId == a.Id).Select(s => s.Id).Count(),
                    TotalStock = a.Total - a.TotalDeploy - a.TotalSoldDeploy - a.TotalNotReceived - a.TotalMaintenance - _unitOfWork.DamagePos.GetAll().Where(s => s.InventoryId == a.Id).Select(s => s.Id).Count()

                });

                return Ok(totalStock);
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }
    }
}
