﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class SerialNoStatus : AuditableEntity
    {
        public Guid Id { get; set; }
        public string Status { get; set; }
       
    }
}
