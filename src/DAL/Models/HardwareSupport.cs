﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class HardwareSupport: AuditableEntity
    {
        public Guid Id { get; set; }        
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string SerialNo { get; set; }
        public string Manufacturer { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }        
        public Guid  BankId { get; set; }
       // public Bank  BankName { get; set; }
    }
}
