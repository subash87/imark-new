﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class OtherStock : AuditableEntity
    {
        public Guid Id { get; set; }
        public Guid DeploymentRequestId { get; set; }
        //public DeploymentRequest DeploymentRequest { get; set; }
        public Guid InventoryAddId { get; set; }
        public InventoryAdd InventoryAdd { get; set; }
        public Guid? OtherPartyId { get; set; }
       // public OtherParty OtherParty { get; set; }
        public DateTime StockDate { get; set; }
        public int SoldOut { get; set; }
        public string Remarks { get; set; }
        public string ReceivedBy { get; set; }
        public string DistributedBy { get; set; }

        public string ContactNo { get; set; }


    }
}
