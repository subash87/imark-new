﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class MaintenanceIssue : AuditableEntity
    {
        public Int32 Id { get; set; }
        public string Issue { get; set; }
        public string Remarks { get; set; }
        public virtual Maintenance Maintenance { get; set; }
        public Guid MaintenanceId { get; set; }
    }
}
