﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Bank: AuditableEntity
    {
        public Guid Id { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string ContactNo1 { get; set; }
        public string Address { get; set; }
        public string Status { get; set; }
        public string ContactPerson1 { get; set; }
        public string ContactPerson2 { get; set; }
        public string ContactNo2 { get; set; }
    }
}
