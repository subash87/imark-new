﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Checker: AuditableEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid StatusId { get; set; }
        public Guid DeploymentRequestId { get; set; }
    }
}
