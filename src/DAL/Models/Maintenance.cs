﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Maintenance : AuditableEntity
    {
        public Guid Id { get; set; }
        public Guid SerialNumberId { get; set; }
        public Guid InventoryId { get; set; }
        public Guid InventoryAddId { get; set; }
        public string SerialNumber { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public Guid ApplicationVersionId { get; set; }
        public string Status { get; set; }
        public string Terminate { get; set; }
        public string Issue { get; set; }
        public string ResolvedBy { get; set; }
        public string Remarks { get; set; }
        public string Verification { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public virtual IEnumerable<MaintenanceIssue> MaintenanceIssue {get;set;}
        public string Merchant { get; set; }
        public string IdMerchant { get; set; }
        public string IdTerminal { get; set; }
        public string Outlet { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
    }
}
