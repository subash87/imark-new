﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class OtherParty : AuditableEntity
    {
        public Guid Id { get; set; }
        public string PartyName { get; set; }
        public string PartyAddress { get; set; }
        public string PhoneNo { get; set; }
     

    }
}
