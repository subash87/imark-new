﻿ using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class Configuration : AuditableEntity
    {
        public Guid Id{ get; set; }
        [ForeignKey("DeploymentRequest")]
        public Guid DeploymentRequestId { get; set; }
        public DeploymentRequest DeploymentRequest { get; set; }
        public string Verification { get; set; }
        [ForeignKey("SerialNumber")]
        public Guid SerialNumberId { get; set; }
        public SerialNumber SerialNumber { get; set; }
        [ForeignKey("ApplicationVersion")]
        public Guid ApplicationVersionId { get; set; }
        public ApplicationVersion ApplicationVersion { get; set; }
        public string SerialKey { get; set; }
        public Guid InventoryId { get; set; }
        public Guid InventoryAddId { get; set; }
        public string Status { get; set; }
        public bool IsSale { get; set; }
        public string PreviousSerial { get; set; }

    }
}
