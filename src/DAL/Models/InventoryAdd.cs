﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class InventoryAdd : AuditableEntity
    {
        public Guid Id { get; set; }
        public int TotalNumber { get; set; }
        public DateTime BuyDate { get; set; }
        public string Remarks { get; set; }
        //public virtual Inventory Inventory { get; set; }
        [ForeignKey("Inventory")]
        public Guid InventoryId { get; set; }
        public Inventory Inventory { get; set; }
        //public ICollection<SerialNumber> SerialNumbers { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
    }
}
