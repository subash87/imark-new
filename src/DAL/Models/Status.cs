﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Status: AuditableEntity
    {
        public Guid Id { get; set; }
        public string StatusName { get; set; }
    }
}
