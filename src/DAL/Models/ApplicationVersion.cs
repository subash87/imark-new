﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class ApplicationVersion: AuditableEntity
    {
        public Guid Id { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public Guid? BankId { get; set; }
        public string ApplicationVer { get; set; }
        public string KernelType { get; set; }
        public string Remarks { get; set; }
    }
}
