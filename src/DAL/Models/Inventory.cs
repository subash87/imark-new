﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class Inventory : AuditableEntity
    {
        public Guid Id { get; set; }
        public string PrimaryItem{ get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public bool HasSerialNo { get; set; }
        public string Connectivity { get; set; }
        public ICollection<InventoryAdd> InventorysAdd { get; set; }

    }
}
