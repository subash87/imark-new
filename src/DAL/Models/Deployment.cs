﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
   public class Deployment : AuditableEntity
    {
        public Guid Id { get; set; }
        public string ReceivedBy { get; set; }
        public string Connectivity { get; set; }
        public string MerchantLocation { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Image { get; set; }
        public string Scanned { get; set; }
        public string Remarks { get; set; }
        public string StatusForDeploy { get; set; }
        [ForeignKey("DeploymentRequest")]
        public Guid DeploymentRequestId { get; set; }
        public DeploymentRequest DeploymentRequest { get; set; }
        public DateTime DeployDate { get; set; }
        public string Verification { get; set; }
        public Guid BankId { get; set; }
        public string BankName { get; set; }
        public Guid InventoryId { get; set; }
        public Guid? InventoryAddId { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public bool Dispatch { get; set; }
    }
}
