﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class PosHistory : AuditableEntity
    {
        public Guid Id { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public Guid BankId { get; set; }
        public string BankName { get; set; }
        public string Merchant { get; set; }
        public string IdMerchant { get; set; }
        public string Outlet { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string IdTerminal { get; set; }
        public string TerminalType { get; set; }
        public string Currency { get; set; }
        public bool TipAdjustment { get; set; }
        public bool ManualTransaction { get; set; }
        public bool PreAuthorization { get; set; }
        public bool Refund { get; set; }
        public string PrimaryNacNumber { get; set; }
        public string SecondaryNacNumber { get; set; }
        public bool Priority { get; set; }
        public string Remarks { get; set; }
        public Guid StatusId { get; set; }
        public string StatusName { get; set; }
        public string RequestName { get; set; }
        public Guid InventoryId { get; set; }
        //*********For Configuration************//
        public Guid SerialNumberId { get; set; }
        public string SerialKey { get; set; }
        public Guid ApplicationVersionId { get; set; }
        public string ApplicationDateVersion { get; set; }
        //************For Deploy***********
        public string ReceivedBy { get; set; }
        public string Connectivity { get; set; }
        public string MerchantLocation { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Image { get; set; }
        public string Scanned { get; set; }
        public string RemarksDeploy { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public DateTime DeployDate { get; set; }
        public string IsReceived { get; set; }

    }
}
