﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class DeploymentRequest : AuditableEntity
    {
        public Guid Id { get; set; }
        [ForeignKey("Bank")]
        public Guid BankId { get; set; }
        public Bank Bank { get; set; }
        public string Merchant { get; set; }
        public string IdMerchant { get; set; }
        public string Outlet { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo1 { get; set; }
        public string ContactNo2 { get; set; }
        public string IdTerminal { get; set; }
        public string TerminalType { get; set; }
        public Guid InventoryId { get; set; }
        public Inventory Inventory { get; set; }
        public string RentalInventoryAdd { get; set; }
        public string Currency { get; set; }
        public bool TipAdjustment { get; set; }
        public bool ManualTransaction { get; set; }
        public bool PreAuthorization { get; set; }
        public bool Refund { get; set; }
        public string PrimaryNacNumber { get; set; }
        public string SecondaryNacNumber { get; set; }
        public bool Priority { get; set; }
        [ForeignKey("Status")]
        public Guid StatusId { get; set; }
        public Status Status { get; set; }
        public string Remarks { get; set; }
        public string RemarksForDeploy { get; set; }
        public string Verification { get; set; }

        [ForeignKey("RequestType")]
        public Guid RequestTypeId { get; set; }
        public RequestType RequestType { get; set; }
        public Deployment Deployment { get; set; }
        public Configuration Configuration { get; set; }



    }
}
