﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class DamagePos : AuditableEntity
    {
        public Guid Id { get; set; }

        public Guid DeploymentRequestId { get; set; }
        public Guid ApplicationVersionId { get; set; }
        public Guid InventoryId { get; set; }
        public Guid InventoryAddId { get; set; }
        public string Status { get; set; }
        public Guid SerialNumberId { get; set; }
        public string Merchant { get; set; }
        public string IdMerchant { get; set; }
        public string IdTerminal { get; set; }
        public string Outlet { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }

    }
}
