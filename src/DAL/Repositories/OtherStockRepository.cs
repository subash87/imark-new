﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class OtherStockRepository : Repository<OtherStock>, IOtherStockRepository
    {
        public OtherStockRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
