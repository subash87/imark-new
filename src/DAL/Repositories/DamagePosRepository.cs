﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class DamagePosRepository : Repository<DamagePos>, IDamagePosRepository
    {
        public DamagePosRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
