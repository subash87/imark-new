﻿using System;
using DAL.Repositories.Interfaces;
using System.Collections.Generic;
using System.Text;
using DAL.Models;

namespace DAL.Repositories
{
    public class HardwareSupportRepository : Repository<HardwareSupport>, IHardwareSupportRepository
    {
        public HardwareSupportRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
