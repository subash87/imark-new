﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Models;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories
{
    public class MaintenanceRepository: Repository<Maintenance>,IMaintenanceRepository
    {
        public MaintenanceRepository(ApplicationDbContext context):base(context)
        {

        }
    }
}
