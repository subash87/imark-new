﻿using DAL.Models;
using DAL.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories.Interfaces
{
   public interface IInventoryRepository :IRepository<Inventory>
    {
        Inventory GetInventoryById(Guid id);
    }
}
