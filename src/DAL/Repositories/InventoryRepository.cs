﻿using DAL.Models;
using DAL.Models.Interfaces;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class InventoryRepository :Repository<Inventory>, IInventoryRepository
    {
        public InventoryRepository(ApplicationDbContext context): base(context)
        {
        }
        public Inventory GetInventoryById(Guid Id)
        {
            return GetGuid(Id);
            
        }

    }
}
