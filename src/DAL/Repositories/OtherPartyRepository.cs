﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;



namespace DAL.Repositories
{
    public class OtherPartyRepository :Repository<OtherParty>, IOtherPartyRepository
    {
        public OtherPartyRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
