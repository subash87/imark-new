﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class ApplicationVersionRepository: Repository<ApplicationVersion>, IApplicationVersionRepository
    {
        public ApplicationVersionRepository(ApplicationDbContext context): base(context)
        {

        }
    }
}
