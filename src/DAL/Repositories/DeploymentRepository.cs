﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class DeploymentRepository : Repository<Deployment>, IDeploymentRepository
    {
        public DeploymentRepository(ApplicationDbContext context): base(context)
        {

        }
    }
}
