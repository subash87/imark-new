﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Models;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories
{
    public class HwSupportRepository:Repository<HwSupport>,IHwSupportRepository
    {
        public HwSupportRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
