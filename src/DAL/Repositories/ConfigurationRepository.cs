﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class ConfigurationRepository: Repository<Configuration>, IConfigurationRepository
    {
        public ConfigurationRepository(ApplicationDbContext context): base(context)
        {

        }
    }
}
