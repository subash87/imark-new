﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class CheckerRepository : Repository<Checker>, ICheckerRepository
    {
        public CheckerRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
