﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class MaintenanceIssueRepository: Repository<MaintenanceIssue>, IMaintenanceIssueRepository
    {
        public MaintenanceIssueRepository(ApplicationDbContext context):base(context)
        {

        }
    }
}
