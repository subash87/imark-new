﻿using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class InventoryAddRepository:Repository<InventoryAdd>, IInventoryAddRepository
    {
        public InventoryAddRepository(ApplicationDbContext context): base(context)
        {
        }
    }
}
