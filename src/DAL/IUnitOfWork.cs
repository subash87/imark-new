﻿
using DAL.Core.Interfaces;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUnitOfWork
    {
        ICustomerRepository Customers { get; }
        IProductRepository Products { get; }
        IOrdersRepository Orders { get; }
        IInventoryRepository Inventorys { get; }
        IInventoryManager InventoryManager { get; }
        IInventoryAddRepository InventorysAdd { get; }
        ISerialNumberRepository SerialNumbers { get; }
        IBankRepository Banks { get; }
        ICheckerRepository Checkers { get; }
        IDeploymentRequestRepository DeploymentRequests { get; }
        IRequestTypeRepository RequestTypes { get; }
        IStatusRepository Statuses { get; }
        IConfigurationRepository Configurations { get; }
        IApplicationVersionRepository ApplicationVersions { get; }
        IDeploymentRepository Deployments { get; }
        IPosHistoryRepository PosHistorys { get; }
        IOtherStockRepository OtherStocks { get; }

        IOtherPartyRepository OtherPartys { get; }

        ISupportRepository Supports { get; }
        IMaintenanceRepository Maintenances { get; }
        IMaintenanceIssueRepository MaintenanceIssues { get; }
        IHardwareSupportRepository HardwareSupports { get; }

        IHwSupportRepository HwSupports { get; }

        IPosStockRepository PosStocks { get; }

        INotReceivedRepository NotReceivedPos { get; }
        IDamagePosRepository DamagePos { get; }


        int SaveChanges();
    }
}
