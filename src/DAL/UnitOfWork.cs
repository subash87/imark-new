﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Core;
using DAL.Core.Interfaces;
using DAL.Repositories;
using DAL.Repositories.Interfaces;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;

        ICustomerRepository _customers;
        IProductRepository _products;
        IOrdersRepository _orders;
        IInventoryRepository _inventorys;
        IInventoryManager _inventoryManager;
        IInventoryAddRepository _inventorysAdd;
        ISerialNumberRepository _serialNumbers;
        IDeploymentRequestRepository _deploymentRequest;
        IBankRepository _bank;
        ICheckerRepository _checker;
        IRequestTypeRepository _requestType;
        IStatusRepository _status;
        IConfigurationRepository _configurations;
        IApplicationVersionRepository _applicationVersions;
        IDeploymentRepository _deploymentRepository;
        IPosHistoryRepository _posHistoryRepository;
        IOtherStockRepository _otherStockRepository;
        IOtherPartyRepository _otherPartyRepository;
        ISupportRepository _supportRepository;
        IMaintenanceRepository _maintenanceRepository;
        IMaintenanceIssueRepository _maintenanceIssueRepository;
        IHardwareSupportRepository _hardwareSupportsRepository;

        IHwSupportRepository _hwSupportsRepository;

        IPosStockRepository _posStocksRepository;

        INotReceivedRepository _notReceivedPosRepository;
        IDamagePosRepository _damagePosRepository;


        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }



        public ICustomerRepository Customers
        {
            get
            {
                if (_customers == null)
                    _customers = new CustomerRepository(_context);

                return _customers;
            }
        }



        public IProductRepository Products
        {
            get
            {
                if (_products == null)
                    _products = new ProductRepository(_context);

                return _products;
            }
        }



        public IOrdersRepository Orders
        {
            get
            {
                if (_orders == null)
                    _orders = new OrdersRepository(_context);

                return _orders;
            }
        }


        public IInventoryRepository Inventorys
        {
            get
            {
                if (_inventorys == null)
                    _inventorys = new InventoryRepository(_context);

                return _inventorys;
            }
        }
        public IInventoryManager InventoryManager
        {
            get
            {
                if (_inventoryManager == null)
                    _inventoryManager = new InventoryManager(_context);
                return _inventoryManager;
            }
        }
        public IInventoryAddRepository InventorysAdd
        {
            get
            {
                if (_inventorysAdd == null)
                    _inventorysAdd = new InventoryAddRepository(_context);
                return _inventorysAdd;
            }
        }
        public ISerialNumberRepository SerialNumbers
        {
            get
            {
                if (_serialNumbers == null)
                    _serialNumbers = new SerialNumberRepository(_context);
                return _serialNumbers;
            }
        }
        public IBankRepository Banks
        {
            get
            {
                if (_bank == null)
                    _bank = new BankRepository(_context);
                return _bank;
            }
        }
        public ICheckerRepository Checkers
        {
            get
            {
                if (_checker == null)
                    _checker = new CheckerRepository(_context);
                return _checker;
            }
        }

        public IDeploymentRequestRepository DeploymentRequests
        {
            get
            {
                if (_deploymentRequest == null)
                    _deploymentRequest = new DeploymentRequestRepository(_context);
                return _deploymentRequest;
            }
        }
        public IRequestTypeRepository RequestTypes
        {
            get
            {
                if (_requestType == null)
                    _requestType = new RequestTypeRepository(_context);
                return _requestType;
            }
        }

        public IStatusRepository Statuses
        {
            get
            {
                if (_status == null)
                    _status = new StatusRepository(_context);
                return _status;
            }
        }
        public IConfigurationRepository Configurations
        {
            get
            {
                if (_configurations == null)
                    _configurations = new ConfigurationRepository(_context);
                return _configurations;
            }
        }
        public IApplicationVersionRepository ApplicationVersions
        {
            get
            {
                if (_applicationVersions == null)
                    _applicationVersions = new ApplicationVersionRepository(_context);
                return _applicationVersions;
            }
        }
        public IDeploymentRepository Deployments
        {
            get
            {
                if (_deploymentRepository == null)
                    _deploymentRepository = new DeploymentRepository(_context);
                return _deploymentRepository;
            }
        }
        public IPosHistoryRepository PosHistorys
        {
            get
            {
                if (_posHistoryRepository == null)
                    _posHistoryRepository = new PosHistoryRepository(_context);
                return _posHistoryRepository;
            }
        }
        public IOtherStockRepository OtherStocks
        {
            get
            {
                if (_otherStockRepository == null)
                    _otherStockRepository = new OtherStockRepository(_context);
                return _otherStockRepository;
            }
        }

        public IOtherPartyRepository OtherPartys
        {
            get
            {
                if (_otherPartyRepository == null)
                    _otherPartyRepository = new OtherPartyRepository(_context);
                return _otherPartyRepository;
            }
        }

        public ISupportRepository Supports
        {
            get
            {
                if (_supportRepository == null)
                    _supportRepository = new SupportRepository(_context);
                return _supportRepository;
            }
        }
        public IMaintenanceRepository Maintenances
        {
            get
            {
                if (_maintenanceRepository == null)
                    _maintenanceRepository = new MaintenanceRepository(_context);
                return _maintenanceRepository;
            }
        }
        public IMaintenanceIssueRepository MaintenanceIssues
        {
            get
            {
                if (_maintenanceIssueRepository == null)
                    _maintenanceIssueRepository = new MaintenanceIssueRepository(_context);
                return _maintenanceIssueRepository;
            }
        }

        public IHardwareSupportRepository HardwareSupports
        {
            get
            {
                if (_hardwareSupportsRepository == null)
                    _hardwareSupportsRepository = new HardwareSupportRepository(_context);
                return _hardwareSupportsRepository;
            }
        }


        public IHwSupportRepository HwSupports
        {
            get
            {
                if (_hwSupportsRepository == null)
                    _hwSupportsRepository = new HwSupportRepository(_context);
                return _hwSupportsRepository;
            }
        }
        public IPosStockRepository PosStocks
        {
            get
            {
                if (_posStocksRepository == null)
                    _posStocksRepository = new PosStockRepository(_context);
                return _posStocksRepository;

            }
        }
        public INotReceivedRepository NotReceivedPos
        {
            get
            {
                if (_notReceivedPosRepository == null)
                    _notReceivedPosRepository = new NotReceivedRepository(_context);
                return _notReceivedPosRepository;

            }
        }
        public IDamagePosRepository DamagePos
        {
            get
            {
                if (_damagePosRepository == null)
                    _damagePosRepository = new DamagePosRepository(_context);
                return _damagePosRepository;

            }
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
