﻿using DAL.Core.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Core
{
    public class InventoryManager : IInventoryManager
    {
        private readonly IUnitOfWork _unitOfWork;
        private ApplicationDbContext _context;

        public InventoryManager(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }

        public InventoryManager(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Inventory>> GetInventoryAsync(int page, int pageSize)
        {
            IEnumerable<Inventory> inventoryQuery = _unitOfWork.Inventorys.GetAll();

            if (page != -1)
                inventoryQuery = inventoryQuery.Skip((page - 1) * pageSize);

            if (pageSize != -1)
                inventoryQuery = inventoryQuery.Take(pageSize);

            var  inventoryList = inventoryQuery.ToList();

            return inventoryList;

        }
    }
}
