﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;

using DAL.Core;

using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;
using DAL.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class NotReceivedPosController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;
        public NotReceivedPosController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            this._unitOfWork = unitOfWork;
            _context = context;

        }

        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<NotReceivedPosViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult Get(int page, int pageSize)
        {
            //IQueryable<NotReceivedPosViewModel> notReceivedPosQuery = (from notReceived in _context.NotReceivedPos
            //                                                                 join depReq in _context.DeploymentRequests
            //                                                                 on notReceived.DeploymentRequestId equals depReq.Id
            //                                                                 join config in _context.Configurations
            //                                                                 on notReceived.ConfigurationId equals config.Id
            //                                                                 join inventoryAdd in _context.Inventorys
            //                                                                 on notReceived.InventoryId equals inventoryAdd.Id
            //                                                                join serialNo in _context.SerialNumbers
            //                                                                on notReceived.SerialNumberId equals serialNo.Id
            //                                                           select new NotReceivedPosViewModel()
            //                                                                 {
            //                                                                     Id = notReceived.Id,
            //                                                                     Status=notReceived.Status,
            //                                                                     BankName = depReq.Bank.BankName,
            //                                                                     Merchant = depReq.Merchant,
            //                                                                     IdMerchant = depReq.IdMerchant,
            //                                                                     IdTerminal = depReq.IdTerminal,
            //                                                                     Outlet = depReq.Outlet,
            //                                                                     Address = depReq.Address,
            //                                                                     District = depReq.District,
            //                                                                     ContactPerson = depReq.ContactPerson,
            //                                                                     ContactNo1 = depReq.ContactNo1,
            //                                                                     InventoryId = depReq.InventoryId,
            //                                                                     SerialKey = serialNo.SerialKey,
            //                                                                     ConfigStatus=config.Status,
            //                                                                     Item= inventoryAdd.PrimaryItem+' '+ inventoryAdd.SecondaryItem+' '+ inventoryAdd.TertiaryItem+' ' + inventoryAdd.DetailItem,
            //                                                                     CreatedDate = depReq.CreatedDate,
            //                                                                     UpdatedDate = depReq.UpdatedDate
                                                                            

            //                                                                 }
            //        );

            //return Ok(notReceivedPosQuery.Where(x=>x.Status=="NotReceived").OrderByDescending(x=>x.CreatedDate));

            IEnumerable<NotReceivedPosViewModel> notReceivedPosQuery = _unitOfWork.NotReceivedPos.GetAll().Join(_unitOfWork.DeploymentRequests.GetAll(), a => a.DeploymentRequestId, b => b.Id, (a, b) => new {  NotReceivedPos= a, DeploymentRequest = b })
                                                               
                                                               .Join(_unitOfWork.ApplicationVersions.GetAll(), e => e.NotReceivedPos.ApplicationVersionId, f => f.Id, (e, f) => new { e.NotReceivedPos, e.DeploymentRequest, Application = f })
                                                              
                                                               .Join(_unitOfWork.Inventorys.GetAll(), g => g.NotReceivedPos.InventoryId, h => h.Id, (g, h) => new { g.NotReceivedPos, g.DeploymentRequest, g.Application, Inventory = h })
                                                                .Join(_unitOfWork.Banks.GetAll(), g => g.DeploymentRequest.BankId, h => h.Id, (g, h) => new { g.NotReceivedPos, g.DeploymentRequest, g.Application,g.Inventory, Bank = h })
                                                               .Join(_unitOfWork.SerialNumbers.GetAll(), g => g.NotReceivedPos.SerialNumberId, h => h.Id, (g, h) => new { g.NotReceivedPos, g.DeploymentRequest, g.Application, g.Inventory,g.Bank, SerialNumber = h })
                                                               .Join(_context.Users, x => x.NotReceivedPos.CreatedBy, y => y.Id, (x, y) => new { x.NotReceivedPos, x.DeploymentRequest, x.Application, x.Inventory, x.Bank, x.SerialNumber, User = y })
                                                               
                                 
                                                               .Select(s => new NotReceivedPosViewModel
                                                               {
                                                                   Id = s.NotReceivedPos.Id,
                                                                   DeploymentRequestId = s.DeploymentRequest.Id,
                                                                   BankName = s.Bank.BankName,
                                                                   BankCode = s.Bank.BankCode,
                                                                   Merchant = s.NotReceivedPos.Merchant,
                                                                   IdMerchant = s.NotReceivedPos.IdMerchant,
                                                                   IdTerminal = s.NotReceivedPos.IdTerminal,
                                                                   Outlet = s.NotReceivedPos.Outlet,
                                                                   Address = s.NotReceivedPos.Address,
                                                                   District = s.NotReceivedPos.District,
                                                                   ContactPerson = s.NotReceivedPos.ContactPerson,
                                                                   ContactNo = s.NotReceivedPos.ContactNo,
                                                                   InventoryId = s.NotReceivedPos.InventoryId,
                                                                   InventoryAddId = s.NotReceivedPos.InventoryAddId,
                                                                   SerialNumberId = s.NotReceivedPos.SerialNumberId,
                                                                    ApplicationVersionId= s.NotReceivedPos.ApplicationVersionId,
                                                                   Status =s.NotReceivedPos.Status,
                                                                 
                                                                   SerialKey = s.SerialNumber.SerialKey,
                                                                   ConfigStatus = s.NotReceivedPos.Status,
                                                                   ApplicationFormat = s.Bank.BankCode + " " + s.Application.ApplicationVer + " " + s.Application.ApplicationDate.Value.ToShortDateString() + " " + s.Application.KernelType,
                                                                   Item = s.Inventory.PrimaryItem + ' ' + s.Inventory.SecondaryItem + ' ' + s.Inventory.TertiaryItem + ' ' + s.Inventory.DetailItem,

                                                                   ReceivedBy = s.NotReceivedPos.ReceivedBy,
                                                                   ReceivedContactNo = s.NotReceivedPos.ReceivedContactNo,
                                                                   DeployedLocation = s.NotReceivedPos.DeployedLocation,
                                                                   CreatedDate = s.NotReceivedPos.CreatedDate,
                                                                
                                                                   CreatedBy = s.User.FullName,

                                                               }).OrderByDescending(x => x.CreatedDate);



            return Ok(notReceivedPosQuery);

        }


        [HttpPut("{id}")]
        public IActionResult Put([FromBody]NotReceivedPosViewModel _notReceivedVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            try
            {
                var _notReceived = _unitOfWork.NotReceivedPos.GetGuid(_notReceivedVM.Id);
                var _posHistoryId = _unitOfWork.PosHistorys.Find(x => x.SerialNumberId == _notReceivedVM.SerialNumberId && x.IsReceived == "NOT RECEIVED").Select(s => s.Id).SingleOrDefault();
                var _configId= _unitOfWork.Configurations.Find(x => x.DeploymentRequestId == _notReceivedVM.DeploymentRequestId && x.PreviousSerial=="notreceived").Select(s => s.Id).SingleOrDefault();
                var _posHistory = _unitOfWork.PosHistorys.GetGuid(_posHistoryId);
                var config=_unitOfWork.Configurations.GetGuid(_configId);
                if (_posHistory != null || config !=null)
                {
                    _posHistory.Scanned = _notReceivedVM.Status;
                    _posHistory.IsReceived = "IN STOCK";
                    config.PreviousSerial = "received";
                    config.Status = _notReceivedVM.Status;
                    _unitOfWork.PosHistorys.Update(_posHistory);
                    _unitOfWork.Configurations.Update(config);
                    _unitOfWork.NotReceivedPos.Remove(_notReceived);
                    _unitOfWork.SaveChanges();
                }
                else
                {
                    _unitOfWork.NotReceivedPos.Remove(_notReceived);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (DbUpdateException)
            {

                throw;

            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }


    }
}
