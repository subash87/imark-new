﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;

using DAL.Core;

using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;
using DAL.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class DamagePosController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;
        public DamagePosController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            this._unitOfWork = unitOfWork;
            _context = context;

        }

        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<DamagePoseViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult Get(int page, int pageSize)
        {
            IQueryable<DamagePoseViewModel> damageQuery = _unitOfWork.DamagePos.GetAll().Join(_unitOfWork.DeploymentRequests.GetAll(), a => a.DeploymentRequestId, b => b.Id, (a, b) => new { DamagePos = a, DeploymentRequest = b })
                                                                .Join(_unitOfWork.ApplicationVersions.GetAll(), c => c.DamagePos.ApplicationVersionId, d => d.Id, (c, d) => new { c.DamagePos, c.DeploymentRequest, Application = d })
                                                                .Join(_unitOfWork.Banks.GetAll(), e => e.Application.BankId, f => f.Id, (e, f) => new { e.DamagePos, e.DeploymentRequest, e.Application, Bank = f })
                                                                
                                                                 .Join(_unitOfWork.Inventorys.GetAll(), g => g.DeploymentRequest.InventoryId, h => h.Id, (g, h) => new { g.DamagePos, g.DeploymentRequest, g.Application,g.Bank,  Inventory = h })
                                                             
                                                                 .Join(_unitOfWork.SerialNumbers.GetAll(), g => g.DamagePos.SerialNumberId, h => h.Id, (g, h) => new { g.DamagePos, g.DeploymentRequest, g.Application, g.Bank, g.Inventory, Serial = h })
                                                                .Join(_context.Users, x => x.DamagePos.CreatedBy, y => y.Id, (x, y) => new { x.DamagePos, x.DeploymentRequest, x.Application, x.Bank, x.Inventory, x.Serial, User = y })

                                                                .Select(s => new DamagePoseViewModel
                                                                {
                                                                    Id = s.DamagePos.Id,
                                                                    Status=s.DamagePos.Status,
                                                                    BankName = s.Bank.BankName,
                                                                    BankCode = s.Bank.BankCode,
                                                                    Merchant = s.DeploymentRequest.Merchant,
                                                                    IdMerchant = s.DeploymentRequest.IdMerchant,
                                                                    IdTerminal = s.DeploymentRequest.IdTerminal,
                                                                    Address = s.DeploymentRequest.Address,
                                                                    ContactPerson = s.DeploymentRequest.ContactPerson,
                                                                    ContactNo = s.DeploymentRequest.ContactNo1,
                                                                    SerialKey = s.Serial.SerialKey,
                                                                    ApplicationFormat = s.Bank.BankCode + " " + s.Application.ApplicationVer + " " + s.Application.ApplicationDate.Value.ToShortDateString() + " " + s.Application.KernelType,
                                                                    Item = s.Inventory.PrimaryItem + ' ' + s.Inventory.SecondaryItem + ' ' + s.Inventory.TertiaryItem + ' ' + s.Inventory.DetailItem,

                                                                    CreatedDate = s.DamagePos.UpdatedDate,

                                                                    CreatedBy = s.User.FullName


                                                                });

            return Ok(damageQuery);

        }

        [HttpPost]
        public IActionResult NotReceivedPos([FromBody]ConfigurationViewModel _configurationVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var configSerial = _unitOfWork.NotReceivedPos.GetAll().Select(x => x.SerialNumberId).SingleOrDefault();
            //var dublicate=_unitOfWork.Configurations.GetAll().Select(x=>x.SerialNumberId).FirstOrDefault();
            int duplicate = _unitOfWork.NotReceivedPos.GetAll().Where(a => a.SerialNumberId == _configurationVM.SerialNumberId).Count();

            if (duplicate > 0)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {
                NotReceivedPos notReceived = new NotReceivedPos()
                {
                    Id = Guid.NewGuid(),
                    DeploymentRequestId = _configurationVM.DeploymentRequestId,
                    SerialNumberId = _configurationVM.SerialNumberId,
                    ApplicationVersionId = _configurationVM.ApplicationVersionId,
                    InventoryId = _configurationVM.InventoryId,
                    Status = "NotReceived"
                };
                try
                {

                    _unitOfWork.NotReceivedPos.Add(notReceived);
                    _unitOfWork.SaveChanges();

                }
                catch (DbUpdateException)
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }



    }
}
