﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;

using DAL.Core;

using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;
using DAL.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using QuickApp.Helpers;
using Microsoft.AspNetCore.Identity;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class InventoryController : Controller
    {
      
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;

        public InventoryController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            this._unitOfWork = unitOfWork;
            this._userManager = userManager;
        }
        
        [HttpGet("pagination/{page:int}/{pageSize:int}/{hasSerial}")]
        [Produces(typeof(List<InventoryViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(int page, int pageSize, string hasSerial)
        {
            //var nonSerial = _unitOfWork.Inventorys.GetAll().Where(x => x.HasSerialNo == false).ToList();
            if (hasSerial =="false") {
                IEnumerable<Inventory> inventoryQuery = _unitOfWork.Inventorys.GetAll().Where(x => x.HasSerialNo == false).ToList();

                if (page != -1)
                    inventoryQuery = inventoryQuery.Skip((page - 1) * pageSize);

                if (pageSize != -1)
                    inventoryQuery = inventoryQuery.Take(pageSize);

                var inventoryList = inventoryQuery.ToList();

                List<InventoryViewModel> usersVM = new List<InventoryViewModel>();

                foreach (var item in inventoryList)
                {
                    var userVM = Mapper.Map<InventoryViewModel>(item);


                    usersVM.Add(userVM);
                }

                return Ok(usersVM);
            }
            else if(hasSerial=="true")
            {
                IEnumerable<Inventory> inventoryQuery = _unitOfWork.Inventorys.GetAll().Where(x => x.HasSerialNo == true).ToList();

                if (page != -1)
                    inventoryQuery = inventoryQuery.Skip((page - 1) * pageSize);

                if (pageSize != -1)
                    inventoryQuery = inventoryQuery.Take(pageSize);

                var inventoryList = inventoryQuery.ToList();

                List<InventoryViewModel> usersVM = new List<InventoryViewModel>();

                foreach (var item in inventoryList)
                {
                    var userVM = Mapper.Map<InventoryViewModel>(item);


                    usersVM.Add(userVM);
                }

                return Ok(usersVM);
            }
            else 
            {
                IEnumerable<Inventory> inventoryQuery = _unitOfWork.Inventorys.GetAll().ToList();

                if (page != -1)
                    inventoryQuery = inventoryQuery.Skip((page - 1) * pageSize);

                if (pageSize != -1)
                    inventoryQuery = inventoryQuery.Take(pageSize);

                var inventoryList = inventoryQuery.ToList();

                List<InventoryViewModel> usersVM = new List<InventoryViewModel>();

                foreach (var item in inventoryList)
                {
                    var userVM = Mapper.Map<InventoryViewModel>(item);


                    usersVM.Add(userVM);
                }

                return Ok(usersVM);
            }
        }

        // GET api/<controller>/5
       
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                var _inventory = _unitOfWork.Inventorys.GetInventoryById(id);
                return Ok(_inventory);
            }
            catch
            {
                throw;
            }
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]InventoryViewModel _inventoryVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Inventory inventory = new Inventory()
            {
                Id = Guid.NewGuid(),
                PrimaryItem = _inventoryVM.PrimaryItem,
                SecondaryItem = _inventoryVM.SecondaryItem,
                TertiaryItem = _inventoryVM.TertiaryItem,
                DetailItem = _inventoryVM.DetailItem,
                HasSerialNo = _inventoryVM.HasSerialNo,
                Connectivity=_inventoryVM.Connectivity
               
            };
            try
            {
                
                _unitOfWork.Inventorys.Add(inventory);
                _unitOfWork.SaveChanges();
                
            }
            catch(DbUpdateException)
            {
                if(ItemAlreadyExists(inventory.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }


        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put( [FromBody]InventoryViewModel _inventoryVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Inventory inventory = new Inventory()
            {
                Id = _inventoryVM.Id,
                PrimaryItem = _inventoryVM.PrimaryItem,
                SecondaryItem = _inventoryVM.SecondaryItem,
                TertiaryItem = _inventoryVM.TertiaryItem,
                DetailItem = _inventoryVM.DetailItem,
                HasSerialNo = _inventoryVM.HasSerialNo,
                Connectivity=_inventoryVM.Connectivity

            };
            try
            {

                _unitOfWork.Inventorys.Update(inventory);
                _unitOfWork.SaveChanges();


            }
            catch (DbUpdateException)
            {
                
                    throw;
                
            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Produces(typeof(InventoryViewModel))]
        public void Delete(Guid id)
        {
            var _invToDelete = _unitOfWork.Inventorys.GetInventoryById(id);
            try
            {
                _unitOfWork.Inventorys.Remove(_invToDelete);
                _unitOfWork.SaveChanges();
            }
            catch(DbUpdateException)
            {
                throw;
            }
        }
        private bool ItemAlreadyExists(Guid Id)
        {
            int count =_unitOfWork.Inventorys.Find(a => a.Id == Id).Count();
            if (count > 0)
            {
                return true;
            }
            else
                return false;
        }
    }
}
