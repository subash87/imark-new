﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;


namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class HwSupportController:Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;

        public HwSupportController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            this._unitOfWork = unitOfWork;
            this._userManager = userManager;
        }
        

        [HttpGet("pagination/{page:int}/{pageSize:int}/{HwSupportId}")]
        [Produces(typeof(List<HwSupportViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(int page, int pageSize, Guid? HwSupportId)
        {
            
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            var user = await _userManager.GetUserAsync(User);
            try
            {
                var support = _unitOfWork.HwSupports.GetAll().GroupJoin(_unitOfWork.HardwareSupports.GetAll(), a => a.HwSupportId, b => b.Id, (a, b) => new HwSupportViewModel

                {
                    Id = a.Id,
                    HwSupportId = b.Select(x => x.Id).SingleOrDefault(),
                    BankName =_unitOfWork.Banks.GetAll().Where(x=>x.Id == a.BankId).Select(s=>s.BankName).FirstOrDefault(),
                    BankId=a.BankId,
                    Phone = a.Phone,
                    Field = a.Field,
                    Status = a.Status,
                    Priority = a.Priority,
                    Issue = a.Issue,
                    ContactPerson = a.ContactPerson,
                    ContactNo = a.ContactNo,
                    ResolvedBy = a.ResolvedBy,
                    Remarks = a.Remarks,
                    VerifiedBy = user.UserName,
                    Verification = a.Verification,
                    VerifiedDate = a.VerifiedDate,
                    SupportDate = a.SupportDate


                }).ToList();

                if (HwSupportId == null)
                {

                    return Ok(support.Where(x => x.Status == false));
                }
                else
                {
                    return Ok(support.Where(a => a.HwSupportId == HwSupportId && a.Verification == "Verified").OrderByDescending(x => x.VerifiedDate).ToList());
                }
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }



        }



        // GET: api/Support/5
        [HttpGet("{id}")]
        public ActionResult Get(Guid id)
        {
            try
            {
                Support support = _unitOfWork.Supports.GetGuid(id);
                return Ok(support);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: api/Support/5 for only required deployment request
        [HttpGet("support/{id}")]
        public ActionResult GetSupport(Guid id)
        {
            try
            {

                IEnumerable<HwSupport> hw_support = _unitOfWork.HwSupports.GetAll().Where(a => a.HwSupportId == id);
                return Ok(hw_support);
            }
            catch (Exception)
            {
                throw;
            }
        }
        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]HwSupportViewModel _supportViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            _supportViewModel.SupportDate = DateTime.UtcNow.AddHours(5).AddMinutes(45);

            HwSupport support = new HwSupport();
            support.SupportDate = DateTime.UtcNow.AddHours(5).AddMinutes(45);
            support = Mapper.Map<HwSupport>(_supportViewModel);
           
            try
            {
                _unitOfWork.HwSupports.Add(support);
                _unitOfWork.SaveChanges();


            }
            catch (DbUpdateException)
            {
                
                if (ItemAlreadyExists(support.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }

        //// PUT: api/Support/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody]SupportViewModel _supportViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            var user = await _userManager.GetUserAsync(User);

            if (_supportViewModel.Verification == "Verified")
            {
                _supportViewModel.VerifiedBy = user.FullName;
                _supportViewModel.VerifiedDate = DateTime.UtcNow.AddHours(5).AddMinutes(45);
            }
            var support = Mapper.Map<Support>(_supportViewModel);
            try
            {
                _unitOfWork.Supports.Update(support);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch
            {
                throw;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            var _supportToDelete = _unitOfWork.Supports.GetGuid(id);
            try
            {
                _unitOfWork.Supports.Remove(_supportToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }
        private bool ItemAlreadyExists(Guid Id)
        {
            int count = _unitOfWork.Configurations.Find(a => a.Id == Id).Count();
            if (count > 0)
            {
                return true;
            }
            else
                return false;
        }

    }
}
