﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;


namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class RequestTypeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public RequestTypeController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<RequestTypeViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(int page, int pageSize)
        {
            IEnumerable<RequestType> requestQuery = _unitOfWork.RequestTypes.GetAll();

            if (page != -1)
                requestQuery = requestQuery.Skip((page - 1) * pageSize);

            if (pageSize != -1)
                requestQuery = requestQuery.Take(pageSize);

            var requestList = requestQuery.ToList();

            List<RequestTypeViewModel> requestsVM = new List<RequestTypeViewModel>();

            foreach (var item in requestList)
            {
                var requestVM = Mapper.Map<RequestTypeViewModel>(item);


                requestsVM.Add(requestVM);
            }

            return Ok(requestsVM);

        }
    }
}
