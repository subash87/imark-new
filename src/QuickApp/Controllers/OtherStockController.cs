﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;

using DAL.Core;

using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;
using DAL.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class OtherStockController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;

        public OtherStockController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;



        }

        [HttpGet("pagination/{page:int}/{pageSize:int}/{deploymentRequestId}")]
        [Produces(typeof(List<OtherStockViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(int page, int pageSize, Guid? deploymentRequestId)
        {
            //if (deploymentRequestId == null)
            //{
            //    OtherStockAddViewModel oStock = new OtherStockAddViewModel();
            //    return Ok(oStock);
            //}
            //else
            //{
                try
                {
                    //DeploymentRequest _request = _unitOfWork.DeploymentRequests.GetGuid(id);
                    var stock = _unitOfWork.OtherStocks.GetAll().GroupJoin(_unitOfWork.DeploymentRequests.GetAll(), a => a.DeploymentRequestId, b => b.Id, (a, b) => new OtherStockAddViewModel

                    {
                        Id = a.Id,
                        DeploymentRequestId = b.Select(x => x.Id).SingleOrDefault(),
                        InventoryAddId = a.InventoryAddId,
                        OtherPartyId = a.OtherPartyId,
                        Merchant = b.Select(x => x.Merchant).FirstOrDefault(),
                        IdMerchant = b.Select(x => x.IdMerchant).FirstOrDefault(),
                        Outlet = b.Select(x => x.Merchant).FirstOrDefault(),
                        IdTerminal = b.Select(x => x.IdTerminal).FirstOrDefault(),
                        SoldOut = a.SoldOut,
                        Remarks = a.Remarks,
                        ReceivedBy=a.ReceivedBy,
                        ContactNo=a.ContactNo,
                        DistributedBy=a.DistributedBy,
                        StockDate=a.StockDate,
                        CreatedBy=a.CreatedBy
                    });
                    var stock1 = stock.GroupJoin(_unitOfWork.OtherPartys.GetAll(), a => a.OtherPartyId, b => b.Id, (a, b) => new OtherStockAddViewModel
                    {
                        Id = a.Id,
                        DeploymentRequestId = a.DeploymentRequestId,
                        InventoryAddId = a.InventoryAddId,
                        OtherPartyId = b.Select(x => x.Id).SingleOrDefault(),
                        PartyName = b.Select(x => x.PartyName).FirstOrDefault(),
                        PartyAddress = b.Select(x => x.PartyAddress).FirstOrDefault(),
                        PhoneNo = b.Select(x => x.PhoneNo).FirstOrDefault(),
                        Merchant = a.Merchant,
                        IdMerchant = a.IdMerchant,
                        Outlet = a.Outlet,
                        IdTerminal = a.IdTerminal,
                        SoldOut = a.SoldOut,
                        Remarks = a.Remarks,
                        ReceivedBy = a.ReceivedBy,
                        ContactNo = a.ContactNo,
                        DistributedBy = a.DistributedBy,
                        StockDate = a.StockDate,
                        CreatedBy = a.CreatedBy
                    });

                    var inventory = _unitOfWork.InventorysAdd.GetAll().GroupJoin(_unitOfWork.Inventorys.GetAll(), a => a.InventoryId, b => b.Id, (a, b) => new InventoryandAddViewModel
                    {
                        Id = a.Id,
                        InventoryId = b.Select(x => x.Id).SingleOrDefault(),
                        PrimaryItem = b.Select(x => x.PrimaryItem).FirstOrDefault(),
                        SecondaryItem = b.Select(x => x.SecondaryItem).FirstOrDefault(),
                        TertiaryItem = b.Select(x => x.TertiaryItem).FirstOrDefault(),
                        DetailItem = b.Select(x => x.DetailItem).FirstOrDefault(),
                        TotalNumber = a.TotalNumber,

                    });
                    var bank = _unitOfWork.DeploymentRequests.GetAll().GroupJoin(_unitOfWork.Banks.GetAll(), a => a.BankId, b => b.Id, (a, b) => new DeploymentAddRequestViewModel
                    {
                        Id = a.Id,
                        BankId = b.Select(s => s.Id).SingleOrDefault(),
                        BankName = b.Select(s => s.BankName).FirstOrDefault(),
                    });

                    var stock2 = stock1.GroupJoin(inventory, a => a.InventoryAddId, b => b.Id, (a, b) => new OtherStockAddViewModel
                    {

                        Id = a.Id,
                        DeploymentRequestId = a.DeploymentRequestId,
                        InventoryAddId = b.Select(s => s.Id).SingleOrDefault(),
                        OtherPartyId = a.OtherPartyId,
                        Merchant = a.Merchant,
                        IdMerchant = a.IdMerchant,
                        Outlet = a.Outlet,
                        IdTerminal = a.IdTerminal,
                        SoldOut = a.SoldOut,
                        Remarks = a.Remarks,
                        ReceivedBy = a.ReceivedBy,
                        ContactNo = a.ContactNo,
                        DistributedBy = a.DistributedBy,
                        PartyName = a.PartyName,
                        PartyAddress = a.PartyAddress,
                        PhoneNo = a.PhoneNo,
                        PrimaryItem = b.Select(s => s.PrimaryItem).FirstOrDefault(),
                        SecondaryItem = b.Select(s => s.SecondaryItem).FirstOrDefault(),
                        TertiaryItem = b.Select(s => s.TertiaryItem).FirstOrDefault(),
                        DetailItem = b.Select(s => s.DetailItem).FirstOrDefault(),
                        TotalNumber = b.Select(s => s.TotalNumber).FirstOrDefault(),
                        Remaining = b.Select(s => s.TotalNumber - _unitOfWork.OtherStocks.GetAll().Where(w => w.InventoryAddId == a.InventoryAddId).Select(w => w.SoldOut).Sum()).FirstOrDefault(),
                        StockDate = a.StockDate,
                        CreatedBy = a.CreatedBy
                    });
                    var stock3 = stock2.GroupJoin(bank, a => a.DeploymentRequestId, b => b.Id, (a, b) => new OtherStockAddViewModel
                    {

                        Id = a.Id,
                        DeploymentRequestId = a.DeploymentRequestId,
                        InventoryAddId = a.InventoryAddId,
                        OtherPartyId = a.OtherPartyId,
                        Merchant = a.Merchant,
                        IdMerchant = a.IdMerchant,
                        Outlet = a.Outlet,
                        IdTerminal = a.IdTerminal,
                        SoldOut = a.SoldOut,
                        PrimaryItem = a.PrimaryItem,
                        SecondaryItem = a.SecondaryItem,
                        TertiaryItem = a.TertiaryItem,
                        DetailItem = a.DetailItem,
                        TotalNumber = a.TotalNumber,
                        BankName = b.Select(s => s.BankName).FirstOrDefault(),
                        Remaining = a.Remaining,
                        Remarks = a.Remarks,
                        ReceivedBy = a.ReceivedBy,
                        ContactNo = a.ContactNo,
                        DistributedBy = a.DistributedBy,
                        PartyName = a.PartyName,
                        PartyAddress = a.PartyAddress,
                        PhoneNo = a.PhoneNo,
                        StockDate = a.StockDate,
                        CreatedBy = _context.Users.Where(x => x.Id == a.CreatedBy).Select(s => s.FullName).FirstOrDefault()
                });

                    if (deploymentRequestId == null)

                        return Ok(stock3);
                    else
                    {
                        return Ok(stock3.Where(a => a.DeploymentRequestId == deploymentRequestId).OrderByDescending(x => x.StockDate).ToList());
                    }
                }
                catch (Exception ex)
                {
                    return new StatusCodeResult(StatusCodes.Status500InternalServerError);
                }
           // }


        }



        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] OtherStockAddViewModel _otherStockVM)
        {
            //_otherStockVM.DeploymentRequestId = new Guid();
           
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int countTotalNumber = _unitOfWork.OtherStocks.GetAll().Where(a => a.InventoryAddId == _otherStockVM.InventoryAddId).Select(a => a.SoldOut).Sum();
            int remain = countTotalNumber + _otherStockVM.SoldOut;
            int total = _unitOfWork.InventorysAdd.GetGuid(_otherStockVM.InventoryAddId).TotalNumber;
            if ( remain > total)
            {


                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {
                var request = _unitOfWork.DeploymentRequests.GetGuid(_otherStockVM.DeploymentRequestId);

                OtherStock otherStock = new OtherStock()
                    {
                        Id = Guid.NewGuid(),
                        DeploymentRequestId = request.Id,
                        InventoryAddId = _otherStockVM.InventoryAddId,
                        OtherPartyId = _otherStockVM.OtherPartyId,
                        SoldOut = _otherStockVM.SoldOut,
                        Remarks = _otherStockVM.Remarks,
                        ReceivedBy=_otherStockVM.ReceivedBy,
                        DistributedBy=_otherStockVM.DistributedBy,
                        ContactNo=_otherStockVM.ContactNo,
                        StockDate= DateTime.UtcNow.AddHours(5).AddMinutes(45)
            };

                    try
                    {
                        _unitOfWork.OtherStocks.Add(otherStock);
                    _unitOfWork.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        throw;
                    }
                    return new StatusCodeResult(StatusCodes.Status201Created);
                
            }
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Update([FromBody]OtherStockAddViewModel _otherStockVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int countTotalNumber = _unitOfWork.OtherStocks.GetAll().Where(a => a.InventoryAddId == _otherStockVM.InventoryAddId).Select(a => a.SoldOut).Sum();
            int remain = countTotalNumber - _otherStockVM.SoldOut;
            int total = _unitOfWork.InventorysAdd.GetGuid(_otherStockVM.InventoryAddId).TotalNumber;
            if (remain >= total)
            {


                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {

                OtherStock otherstock = new OtherStock()
            {
                Id = _otherStockVM.Id,
                DeploymentRequestId = _otherStockVM.DeploymentRequestId,
                InventoryAddId = _otherStockVM.InventoryAddId,
                OtherPartyId=_otherStockVM.OtherPartyId,
                SoldOut = _otherStockVM.SoldOut,
                Remarks = _otherStockVM.Remarks,
                    ReceivedBy = _otherStockVM.ReceivedBy,
                    DistributedBy = _otherStockVM.DistributedBy,
                    ContactNo = _otherStockVM.ContactNo,
                    StockDate = DateTime.UtcNow.AddHours(5).AddMinutes(45)


                };
            try
            {

                _unitOfWork.OtherStocks.Update(otherstock);
                _unitOfWork.SaveChanges();


            }
                catch (DbUpdateException)
                {
                    throw;
                }
                return new StatusCodeResult(StatusCodes.Status201Created);
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Produces(typeof(OtherStockAddViewModel))]
        public void Delete(Guid id)
        {
            var _stockDelete = _unitOfWork.OtherStocks.GetGuid(id);
            try
            {
                _unitOfWork.OtherStocks.Remove(_stockDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }
    }
}
