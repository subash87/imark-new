﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;


namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class BankController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public BankController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        // GET: api/<controller>
        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<BankViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult Get(int page, int pageSize)
        {


            var bankQuery = _unitOfWork.Banks.GetAll().ToList();


            return Ok(bankQuery);

        }

        [HttpGet("partialRentalBank/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<BankViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult GetPartialRentalBank(int page, int pageSize)
        {
            IQueryable<Bank> bankQuery = _unitOfWork.Banks.GetAll().Where(s => s.Status == "Partial Rental" || s.Status=="Both");


            return Ok(bankQuery);

        }
        [HttpGet("rentalBank/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<BankViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult GetRentalBank(int page, int pageSize)
        {
            IQueryable<Bank> bankQuery = _unitOfWork.Banks.GetAll().Where(s => s.Status == "Partial Rental" || s.Status == "Both" || s.Status=="Rental");


            return Ok(bankQuery);

        }

        [HttpPost]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult Post([FromBody] BankViewModel _bankVM)
        {
            try
            {
                _bankVM.Id = Guid.NewGuid();
                var bank = Mapper.Map<Bank>(_bankVM);
                _unitOfWork.Banks.Add(bank);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch
            {
                throw;
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromBody] BankViewModel _bankVM)
        {
            try
            {
                var bank = Mapper.Map<Bank>(_bankVM);
                _unitOfWork.Banks.Update(bank);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch
            {
                throw;
            }
        }
        [HttpDelete("{id}")]
        [Produces(typeof(BankViewModel))]
        public void Delete(Guid id)
        {
            var _bankToDelete = _unitOfWork.Banks.GetGuid(id);
            try
            {
                _unitOfWork.Banks.Remove(_bankToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }


    }

}
