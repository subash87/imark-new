﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickApp.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class MaintenanceController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;

        public MaintenanceController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Authorize(Authorization.Policies.ViewAllRolesPolicy)]
        [Produces(typeof(List<MaintenanceViewModel>))]
        public ActionResult Get(int page, int pageSize)
        {
            try
            {

                IEnumerable<MaintenanceViewModel> maintenanceQuery = _unitOfWork.Maintenances.GetAll().Join(_unitOfWork.DeploymentRequests.GetAll(), a => a.DeploymentRequestId, b => b.Id, (a, b) => new { Maintenance = a, DeploymentRequest = b })
                                                                .Join(_unitOfWork.Banks.GetAll(), c => c.DeploymentRequest.BankId, d => d.Id, (c, d) => new { c.Maintenance, c.DeploymentRequest, Bank = d })
                                                                
                                                                .Join(_unitOfWork.ApplicationVersions.GetAll(), e => e.Maintenance.ApplicationVersionId, f => f.Id, (e, f) => new { e.Maintenance, e.DeploymentRequest, e.Bank, Application = f })
                                                                 .Join(_unitOfWork.Inventorys.GetAll(), g => g.Maintenance.InventoryId, h => h.Id, (g, h) => new { g.Maintenance, g.DeploymentRequest, g.Bank, g.Application, Inventory=h })
                                                                 
                                                                .Join(_context.Users, x => x.Maintenance.CreatedBy, y => y.Id, (x, y) => new { x.Maintenance, x.DeploymentRequest, x.Bank, x.Application, x.Inventory, User = y })
                                                                //.Join(_context.Users, k => k.Deployment.UpdatedBy, m => m.Id, (k, m) => new { k.DeploymentRequest, k.Bank, k.Configuration, k.Application, k.Deployment, k.User, UserUpdated = m })

                                                                .Select(s => new MaintenanceViewModel
                                                                {
                                                                    Id = s.Maintenance.Id,
                                                                    Status=s.Maintenance.Status,
                                                                    Terminate=s.Maintenance.Terminate,
                                                                    Remarks=s.Maintenance.Remarks,
                                                                    ResolvedBy=s.Maintenance.ResolvedBy,
                                                                    BankName = s.Bank.BankName,
                                                                    BankCode = s.Bank.BankCode,
                                                                    Merchant = s.Maintenance.Merchant,
                                                                    IdMerchant = s.Maintenance.IdMerchant,
                                                                    IdTerminal = s.Maintenance.IdTerminal,
                                                                    Outlet = s.Maintenance.Outlet,
                                                                    District = s.Maintenance.District,
                                                                    Address = s.Maintenance.Address,
                                                                    ContactPerson = s.Maintenance.ContactPerson,
                                                                    ContactNo = s.Maintenance.ContactNo,
                                                                    DeploymentRequestId =s.Maintenance.DeploymentRequestId,
                                                                    InventoryAddId=s.Maintenance.InventoryAddId,
                                                                    InventoryId=s.Maintenance.InventoryId,
                                                                    SerialNumberId=s.Maintenance.SerialNumberId,
                                                                    ApplicationVersionId=s.Maintenance.ApplicationVersionId,
                                                                    SerialNumber = s.Maintenance.SerialNumber,
                                                                    ApplicationFormat = s.Bank.BankCode + " " + s.Application.ApplicationVer + " " + s.Application.ApplicationDate.Value.ToShortDateString() + " " + s.Application.KernelType,
                                                                    Item = s.Inventory.PrimaryItem + ' ' + s.Inventory.SecondaryItem + ' ' + s.Inventory.TertiaryItem + ' ' + s.Inventory.DetailItem,
                                                                    CreatedBy = s.User.FullName,
                                                                    CreatedDate=s.Maintenance.CreatedDate

                                                                }).OrderByDescending(x=>x.CreatedDate);



                return Ok(maintenanceQuery);

            }
            catch
            {
                throw;
            }
        }
        [HttpGet("issue/{page:int}/{pageSize:int}/{id}")]
        [Authorize(Authorization.Policies.ViewAllRolesPolicy)]
        [Produces(typeof(List<ItemRowsViewModel>))]
        public ActionResult GetIssue(int page, int pageSize, Guid id)
        {
            try
            {
                IQueryable<ItemRowsViewModel> issueQuery = _unitOfWork.MaintenanceIssues.GetAll().Where(x=>x.MaintenanceId==id).GroupJoin(_unitOfWork.Maintenances.GetAll(), a => a.MaintenanceId, b => b.Id, (a, b) => new ItemRowsViewModel
                {
                    Id = a.Id,
                    MaintenanceId = b.Select(s=>s.Id).SingleOrDefault(),
                    Remarks = a.Remarks,
                   Issue=a.Issue,
                   CreatedBy=a.CreatedBy,
                   CreatedDate=a.CreatedDate,
                   //Status=b.Select(s=>s.Status).FirstOrDefault(),
                   //ResolvedBy=b.Select(s=>s.ResolvedBy).FirstOrDefault(),
                   //Terminate = b.Select(s => s.Terminate).FirstOrDefault(),


                }).OrderByDescending(x=>x.CreatedDate);

                if (page != -1)
                    issueQuery = issueQuery.Skip((page - 1) * pageSize);

                if (pageSize != -1)
                    issueQuery = issueQuery.Take(pageSize);

                var issueList = issueQuery.ToList();

                List<ItemRowsViewModel> issuesVM = new List<ItemRowsViewModel>();

                foreach (var item in issueList)
                {
                    var issueVM = Mapper.Map<ItemRowsViewModel>(item);
                    issueVM.CreatedBy = _context.Users.Where(x => x.Id == item.CreatedBy).Select(s => s.FullName).FirstOrDefault();
                    issueVM.UpdatedBy = _context.Users.Where(x => x.Id == item.UpdatedBy).Select(s => s.FullName).FirstOrDefault();

                    issuesVM.Add(issueVM);
                }

                return Ok(issuesVM);

            }
            catch
            {
                throw;
            }
        }
        //[HttpGet("seriallist/{page:int}/{pageSize:int}")]
        //[Authorize(Authorization.Policies.ViewAllRolesPolicy)]
        //[Produces(typeof(List<MaintenanceViewModel>))]
        //public ActionResult GetSerialList(int page, int pageSize)
        //{
        //    try
        //    {
        //        IQueryable<MaintenanceViewModel> serialNumbers = _unitOfWork.SerialNumbers.GetAll().GroupJoin(_unitOfWork.InventorysAdd.GetAll(), a => a.InventoryAddId, b => b.Id, (a, b) => new MaintenanceViewModel
        //        {
        //            SerialNumberId = a.Id,
        //            SerialKey = a.SerialKey,
        //            InventoryId = b.Select(s => s.InventoryId).FirstOrDefault(),
        //            BuyDate = b.Select(s => s.BuyDate).FirstOrDefault(),

        //        }).GroupJoin(_unitOfWork.Inventorys.GetAll().Where(a => a.HasSerialNo == true), a => a.InventoryId, b => b.Id, (a, b) => new MaintenanceViewModel
        //        {

        //            SerialNumberId = a.SerialNumberId,
        //            SerialKey = a.SerialKey,
        //            InventoryId = a.InventoryId,
        //            BuyDate = a.BuyDate,
        //            InventoryName = b.Select(s => s.PrimaryItem).FirstOrDefault() +" "+ b.Select(s => s.SecondaryItem).FirstOrDefault()
        //                            + " " + b.Select(s => s.TertiaryItem).FirstOrDefault() + " " + b.Select(s => s.DetailItem).FirstOrDefault(),
        //        });
        //        return Ok(serialNumbers);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //[HttpGet("{id}")]
        //[Produces(typeof(SerialNumberViewModel))]
        //public void GetSerial(Guid id)
        //{
        //    var _maintenanceSerialList = _unitOfWork.SerialNumbers.GetGuid(id);
        //    try
        //    {
        //        _unitOfWork.Configurations.Remove(_configToDelete);
        //        _unitOfWork.SaveChanges();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        throw;
        //    }
        //}



        [HttpPost("{serialId}")]
        public ActionResult Post([FromBody]MaintenanceViewModel _maintenanceVM, Guid serialId)
        {
            var posHistoryInUseId = _unitOfWork.PosHistorys.Find(x => x.SerialNumberId == _maintenanceVM.SerialNumberId).Select(s => s.Id).SingleOrDefault();

            var _posHistoryId = _unitOfWork.PosHistorys.Find(x => x.SerialNumberId == _maintenanceVM.SerialNumberId).Select(s => s.Id).LastOrDefault();
            var posHistory = _unitOfWork.PosHistorys.GetGuid(posHistoryInUseId);
            // var x =_maintenanceVM.ItemRows.FirstOrDefault().
            var deploymentReq = _unitOfWork.DeploymentRequests.GetGuid(_maintenanceVM.DeploymentRequestId);
            Maintenance _maintenance = new Maintenance();
            var maintenance = Mapper.Map<Maintenance>(_maintenanceVM);
            maintenance.Id = Guid.NewGuid();
            maintenance.Status = "Under Repair";
            maintenance.SerialNumberId = serialId;
            maintenance.SerialNumber = _unitOfWork.SerialNumbers.GetGuid(serialId).SerialKey;
            maintenance.ApplicationVersionId = _maintenanceVM.ApplicationVersionId;
            maintenance.InventoryAddId = _maintenanceVM.InventoryAddId;
            maintenance.InventoryId = _maintenanceVM.InventoryId;
            maintenance.DeploymentRequestId = _maintenanceVM.DeploymentRequestId;
            maintenance.Merchant = deploymentReq.Merchant;
            maintenance.IdMerchant = deploymentReq.IdMerchant;
            maintenance.IdTerminal = deploymentReq.IdTerminal;
            maintenance.Outlet = deploymentReq.Outlet;
            maintenance.District = deploymentReq.District;
            maintenance.Address = deploymentReq.Address;
            maintenance.ContactPerson = deploymentReq.ContactPerson;
            maintenance.ContactNo = deploymentReq.ContactNo1;
            var duplicate = _unitOfWork.Maintenances.Find(a => a.SerialNumberId == serialId).Select(x=>x.SerialNumberId).SingleOrDefault();
            var notReceivedDublicate= _unitOfWork.NotReceivedPos.Find(a => a.SerialNumberId == serialId).Select(x => x.SerialNumberId).SingleOrDefault();
            if (duplicate == serialId || notReceivedDublicate == serialId )
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {
                try
                {
                    if (posHistory != null)
                    {
                        posHistory.IsReceived = "IN MAINTENANCE";
                        posHistory.Scanned = "NOT IN USE";
                        _unitOfWork.Maintenances.Add(maintenance);
                        _unitOfWork.PosHistorys.Update(posHistory);
                        _unitOfWork.SaveChanges();
                    }
                    else
                    {
                       _unitOfWork.Maintenances.Add(maintenance);
                      
                        _unitOfWork.SaveChanges();
                    }

                }
                catch (Exception)
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }

        [HttpPost("fromDeployment/{deploymentRequestId}")]
        public ActionResult PostMaintenance([FromBody]MaintenanceViewModel _maintenanceVM, Guid deploymentRequestId)
        {

           
             


            Maintenance _maintenance = new Maintenance();
            var maintenance = Mapper.Map<Maintenance>(_maintenanceVM);
            var depReq = _unitOfWork.DeploymentRequests.GetGuid(deploymentRequestId);
            depReq.RequestTypeId = _unitOfWork.RequestTypes.Find(a => a.RequestName == "Delete").Select(a => a.Id).FirstOrDefault();
            depReq.StatusId = _unitOfWork.Statuses.Find(a => a.StatusName == "RetrieveVerified").Select(a => a.Id).FirstOrDefault();
           
            maintenance.Id = Guid.NewGuid();
            maintenance.Status = "Under Repair";
            maintenance.InventoryId = _maintenanceVM.InventoryId;
            maintenance.DeploymentRequestId = _maintenanceVM.DeploymentRequestId;
            var configId = _unitOfWork.Configurations.Find(x => x.DeploymentRequestId == deploymentRequestId).Select(s => s.Id).SingleOrDefault();
            var config = _unitOfWork.Configurations.GetGuid(configId);
            config.Verification = null;
            var serialNumberId = config.SerialNumberId;
            var serialNumber = config.SerialKey;
            var applicationId = config.ApplicationVersionId;
            var inventoryAddId = config.InventoryAddId;
            //var _posHistoryId = _unitOfWork.PosHistorys.Find(x => x.SerialNumberId == serialNumberId).Select(s => s.Id).LastOrDefault();
            //var posHistory = _unitOfWork.PosHistorys.GetGuid(_posHistoryId);
            maintenance.SerialNumberId = serialNumberId;
            maintenance.ApplicationVersionId = applicationId;
            maintenance.InventoryAddId = inventoryAddId;
            maintenance.SerialNumber = serialNumber;
            maintenance.DeploymentRequestId = _maintenanceVM.DeploymentRequestId;
            maintenance.Merchant = _maintenanceVM.Merchant;
            maintenance.IdMerchant = _maintenanceVM.IdMerchant;
            maintenance.IdTerminal = _maintenanceVM.IdTerminal;
            maintenance.Outlet = _maintenanceVM.Outlet;
            maintenance.District = _maintenanceVM.District;
            maintenance.Address = _maintenanceVM.Address;
            maintenance.ContactPerson = _maintenanceVM.ContactPerson;
            maintenance.ContactNo = _maintenanceVM.ContactNo;
            maintenance.InventoryId = _maintenanceVM.InventoryId;
            maintenance.Status = _maintenanceVM.Status;

            var duplicate = _unitOfWork.Maintenances.Find(a => a.SerialNumberId == serialNumberId).Select(x => x.SerialNumberId).SingleOrDefault();
            var notReceivedDublicate = _unitOfWork.NotReceivedPos.Find(a => a.SerialNumberId == serialNumberId).Select(x => x.SerialNumberId).SingleOrDefault();
            if (duplicate == serialNumberId || notReceivedDublicate == serialNumberId)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {
                try
                {
                    //if (posHistory != null)
                    //{
                    //    posHistory.IsReceived = "IN MAINTENANCE";
                    //    _unitOfWork.Maintenances.Add(maintenance);
                    //    _unitOfWork.PosHistorys.Update(posHistory);
                    //    _unitOfWork.DeploymentRequests.Update(depReq);
                    //    _unitOfWork.Configurations.Remove(config);
                    //    _unitOfWork.SaveChanges();

                    //}
                    //else
                    //{
                    _unitOfWork.Maintenances.Add(maintenance);
                    _unitOfWork.DeploymentRequests.Update(depReq);
                        _unitOfWork.Configurations.Remove(config);
                        _unitOfWork.SaveChanges();

                    //}
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }
        [HttpPost("postMaintenanceFromNotReceive/{deploymentRequestId}")]
        public ActionResult AddMaintenance([FromBody]MaintenanceViewModel _maintenanceVM, Guid deploymentRequestId)
        {

            var _posHistoryId = _unitOfWork.PosHistorys.Find(x => x.SerialNumberId == _maintenanceVM.SerialNumberId).Select(s => s.Id).LastOrDefault();
            var posHistory = _unitOfWork.PosHistorys.GetGuid(_posHistoryId);
            // var x =_maintenanceVM.ItemRows.FirstOrDefault().
            var _configId = _unitOfWork.Configurations.Find(x => x.DeploymentRequestId == _maintenanceVM.DeploymentRequestId && x.PreviousSerial == "notreceived").Select(s => s.Id).SingleOrDefault();
            var config = _unitOfWork.Configurations.GetGuid(_configId);
            Maintenance _maintenance = new Maintenance();
            var maintenance = Mapper.Map<Maintenance>(_maintenanceVM);

            var serialNumber = _unitOfWork.SerialNumbers.GetGuid(_maintenanceVM.SerialNumberId).SerialKey;
            var notReceivedId = _unitOfWork.NotReceivedPos.Find(x => x.SerialNumberId == _maintenanceVM.SerialNumberId).Select(s => s.Id).SingleOrDefault();
            var notReceive = _unitOfWork.NotReceivedPos.GetGuid(notReceivedId);
            maintenance.Id = Guid.NewGuid();
            maintenance.Status = _maintenanceVM.Status;
            maintenance.InventoryId = _maintenanceVM.InventoryId;
            maintenance.DeploymentRequestId = deploymentRequestId;
            maintenance.InventoryAddId = _maintenanceVM.InventoryAddId;
            maintenance.InventoryId = _maintenanceVM.InventoryId;
            maintenance.SerialNumberId = _maintenanceVM.SerialNumberId;
            maintenance.ApplicationVersionId = _maintenanceVM.ApplicationVersionId;
            maintenance.SerialNumber = serialNumber;
            maintenance.Merchant = _maintenanceVM.Merchant;
            maintenance.IdMerchant = _maintenanceVM.IdMerchant;
            maintenance.IdTerminal = _maintenanceVM.IdTerminal;
            maintenance.Outlet = _maintenanceVM.Outlet;
            maintenance.District = _maintenanceVM.District;
            maintenance.Address = _maintenanceVM.Address;
            maintenance.ContactPerson = _maintenanceVM.ContactPerson;
            maintenance.ContactNo = _maintenanceVM.ContactNo;

            var duplicate = _unitOfWork.Maintenances.Find(a => a.SerialNumberId == _maintenanceVM.SerialNumberId).Select(x => x.SerialNumberId).Count();
           
            if (duplicate > 0)
            {
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }
            else
            {
                try
                {
                    if (posHistory != null || config != null)
                    {
                        posHistory.IsReceived = "IN MAINTENANCE";
                        posHistory.Scanned = "NOT IN USE";
                        config.PreviousSerial = "received";
                        config.Status = "IN MAINTENANCE";
                        _unitOfWork.PosHistorys.Update(posHistory);
                        _unitOfWork.Configurations.Update(config);
                        _unitOfWork.Maintenances.Add(maintenance);
                        _unitOfWork.NotReceivedPos.Remove(notReceive);
                        _unitOfWork.SaveChanges();

                    }
                    else
                    {
                        _unitOfWork.Maintenances.Add(maintenance);
                        _unitOfWork.NotReceivedPos.Remove(notReceive);
                        _unitOfWork.SaveChanges();

                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromBody]MaintenanceViewModel _maintenanceVM)
        {
            //Maintenance _maintenance = new Maintenance();
            //_maintenance.Id = _maintenanceVM.Id;


            var maintenance = Mapper.Map<Maintenance>(_maintenanceVM);
            maintenance.Id = _maintenanceVM.Id;
            maintenance.ApplicationVersionId = _maintenanceVM.ApplicationVersionId;
            maintenance.DeploymentRequestId = _maintenanceVM.DeploymentRequestId;
            maintenance.InventoryId = _maintenanceVM.InventoryId;
            maintenance.SerialNumberId = _maintenanceVM.SerialNumberId;
            maintenance.SerialNumber = _maintenanceVM.SerialNumber;
            maintenance.Status = _maintenanceVM.Status;
            
            //_unitOfWork.Maintenances.Update(maintenance);
            //_unitOfWork.SaveChanges();
            try
            {
                List<MaintenanceIssue> issues = new List<MaintenanceIssue>();
                _unitOfWork.Maintenances.Update(maintenance);
                _unitOfWork.SaveChanges();
                if (_maintenanceVM.ItemRow.ItemRows.Count() > 0)
                {
                    foreach (ItemRowsViewModel item in _maintenanceVM.ItemRow.ItemRows)
                    {
                        item.MaintenanceId = _maintenanceVM.Id;
                        var itemRow = Mapper.Map<MaintenanceIssue>(item);
                       
                        _unitOfWork.MaintenanceIssues.Add(itemRow);
                        
                        _unitOfWork.SaveChanges();

                    }

                }

            }

            catch (Exception)
            {
                throw;
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }
        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Produces(typeof(MaintenanceViewModel))]
        public void Delete(Guid id)
        {
            var _maintenanceToDelete = _unitOfWork.Maintenances.GetGuid(id);
            var _posHistoryId = _unitOfWork.PosHistorys.Find(x => x.SerialNumberId == _maintenanceToDelete.SerialNumberId && x.IsReceived == "IN MAINTENANCE" && x.Scanned == "NOT IN USE").Select(s => s.Id).SingleOrDefault();
            var posHistory = _unitOfWork.PosHistorys.GetGuid(_posHistoryId);
            try
            {
                if (posHistory != null)
                {
                    posHistory.IsReceived = "DAMAGE";
                    posHistory.Scanned = "NOT IN USE";
                    _unitOfWork.PosHistorys.Update(posHistory);
                    _unitOfWork.Maintenances.Remove(_maintenanceToDelete);
                    _unitOfWork.SaveChanges();
                }
                else
                {
                    _unitOfWork.Maintenances.Remove(_maintenanceToDelete);
                    _unitOfWork.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}