﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;


namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class OtherPartyController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public OtherPartyController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        // GET: api/<controller>
        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<OtherPartyViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult Get(int page, int pageSize)
        {
            IQueryable<OtherParty> partyQuery = _unitOfWork.OtherPartys.GetAll();

            if (page != -1)
                partyQuery = partyQuery.Skip((page - 1) * pageSize);

            if (pageSize != -1)
                partyQuery = partyQuery.Take(pageSize);

            var partyList = partyQuery;

            List<OtherPartyViewModel> partysVM = new List<OtherPartyViewModel>();

            foreach (var item in partyList)
            {
                var partyVM = Mapper.Map<OtherPartyViewModel>(item);


                partysVM.Add(partyVM);
            }

            return Ok(partysVM);

        }
    }
}
