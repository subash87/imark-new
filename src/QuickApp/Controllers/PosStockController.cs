﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;

using DAL.Core;

using DAL.Models;
using DAL.Repositories.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;
using DAL.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class PosStockController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;

        public PosStockController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;


        }

        [HttpGet("pagination/{page:int}/{pageSize:int}/{id}")]
        [Produces(typeof(List<PosStockViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult Get(int page, int pageSize, Guid id)
        {
            IEnumerable<PosStock> posStockQuery = _unitOfWork.PosStocks.GetAll().Where(x => x.InventoryId == id);

            if (page != -1)
                posStockQuery = posStockQuery.Skip((page - 1) * pageSize);

            if (pageSize != -1)
                posStockQuery = posStockQuery.Take(pageSize);

            var posStockList = posStockQuery.ToList();

            List<PosStockViewModel> stocksVM = new List<PosStockViewModel>();

            foreach (var item in posStockList)
            {
                string bankName = _unitOfWork.Banks.GetAll().Where(b => b.Id == item.BankId).Select(a => a.BankName).FirstOrDefault();
                string serialNumber = _unitOfWork.SerialNumbers.GetAll().Where(b => b.SerialKey == item.SerialNumber).Select(a => a.SerialKey).FirstOrDefault();

                var stockVM = Mapper.Map<PosStockViewModel>(item);
                stockVM.BankName = bankName;
                stockVM.SerialNumber = serialNumber;
                stockVM.CreatedBy = _context.Users.Where(x => x.Id == item.CreatedBy).Select(s => s.FullName).FirstOrDefault();
                stocksVM.Add(stockVM);
            }
            stocksVM = stocksVM.OrderByDescending(x => x.StockDate).ToList();
            return Ok(stocksVM);

        }

        [HttpGet("soldHistory/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<PosStockViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public IActionResult GetSoldHistory(int page, int pageSize)
        {
            IEnumerable<PosStockViewModel> posSoldQuery = _unitOfWork.PosStocks.GetAll().Join(_unitOfWork.Inventorys.GetAll(), a => a.InventoryId, b => b.Id, (a, b) => new { PosStock = a, Inventory = b })
                                                              //.Join(_unitOfWork.Inventorys.GetAll(), x => x.PosHistory.InventoryId, y => y.Id, (x, y) => new { x.PosHistory, x.Bank, Inventory = y })
                                                               .Join(_context.Users, x => x.PosStock.CreatedBy, y => y.Id, (x, y) => new { x.PosStock, x.Inventory, User = y })

                                                               .Select(s => new PosStockViewModel
                                                               {
                                                                   Id = s.PosStock.Id,
                                                                   BankName = s.PosStock.BankName,
                                                                  
                                                                  SerialNumber = s.PosStock.SerialNumber,
                                                                   Location = s.PosStock.Location,
                                                                  Remarks=s.PosStock.Remarks,
                                                                   TerminalType = s.Inventory.PrimaryItem + " " + s.Inventory.SecondaryItem + " " + s.Inventory.TertiaryItem + " " + s.Inventory.DetailItem,
                                                                  CreatedBy = s.User.FullName,
                                                                   StockDate=s.PosStock.StockDate
                                                               }).OrderByDescending(x => x.StockDate);
            return Ok(posSoldQuery);
        }



        // POST api/<controller>
        [HttpPost("{id}/{isSale}")]
        public IActionResult put([FromBody]PosStockViewModel _posStockVM, Guid id, bool isSale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var inventory = _unitOfWork.Inventorys.GetGuid(id);
            var _serial = _unitOfWork.SerialNumbers.GetGuid(_posStockVM.SerialNumberId);
            var inventoryAddId = _unitOfWork.SerialNumbers.Find(a => a.InventoryAddId == _serial.InventoryAddId).Select(a => a.InventoryAddId).FirstOrDefault();

            //var inventoryAddId = _unitOfWork.SerialNumbers.Find(x => x.Id == _serial.Id).Select(s => s.InventoryAddId).SingleOrDefault();
            //_serial.InventoryAddId = inventoryAddId;
            _serial.IsSold = isSale;

            PosStock posStock = new PosStock()
            {
                Id = Guid.NewGuid(),
                BankId = _posStockVM.BankId,
                InventoryId = id,
                InventoryAddId= inventoryAddId,
                BankName = _unitOfWork.Banks.GetAll().Where(x => x.Id == _posStockVM.BankId).Select(x => x.BankName).FirstOrDefault(),
                SerialNumberId = _posStockVM.SerialNumberId,
                SerialNumber = _unitOfWork.SerialNumbers.GetAll().Where(x => x.Id == _posStockVM.SerialNumberId).Select(x => x.SerialKey).FirstOrDefault(),
                StockDate = DateTime.UtcNow.AddHours(5).AddMinutes(45),
                Status = "UnDeployed",
                Remarks = _posStockVM.Remarks,
                Location = _posStockVM.Location,
                PrimaryItem = _unitOfWork.Inventorys.GetAll().Where(x => x.Id == id).Select(x => x.PrimaryItem).FirstOrDefault(),
                SecondaryItem = _unitOfWork.Inventorys.GetAll().Where(x => x.Id == id).Select(x => x.SecondaryItem).FirstOrDefault(),
                TertiaryItem = _unitOfWork.Inventorys.GetAll().Where(x => x.Id == id).Select(x => x.TertiaryItem).FirstOrDefault(),
                DetailItem = _unitOfWork.Inventorys.GetAll().Where(x => x.Id == id).Select(x => x.DetailItem).FirstOrDefault(),
            };
            try
            {

                _unitOfWork.PosStocks.Add(posStock);
                _unitOfWork.SerialNumbers.Update(_serial);
                _unitOfWork.SaveChanges();

            }
            catch (DbUpdateException)
            {
                throw;
            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }


        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody]PosStockViewModel _posStockVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PosStock posStock = new PosStock()
            {
                Id = _posStockVM.Id,
                BankId = _posStockVM.BankId,
                BankName = _unitOfWork.Banks.GetAll().Where(x => x.Id == _posStockVM.BankId).Select(x => x.BankName).FirstOrDefault(),
                SerialNumberId = _posStockVM.SerialNumberId,
                SerialNumber = _unitOfWork.SerialNumbers.GetAll().Where(x => x.Id == _posStockVM.SerialNumberId).Select(x => x.SerialKey).FirstOrDefault(),
                StockDate = _posStockVM.StockDate,
                Status = _posStockVM.Status,
                Remarks = _posStockVM.Remarks,
                Location = _posStockVM.Location
            };
            try
            {

                _unitOfWork.PosStocks.Update(posStock);
                _unitOfWork.SaveChanges();


            }
            catch (DbUpdateException)
            {

                throw;

            }
            return new StatusCodeResult(StatusCodes.Status200OK);
        }

        //[HttpPost("newPartialDeployment")]
        //public IActionResult Post([FromBody]DeploymentRequestViewModel _depRequestVM)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (_depRequestVM.Verification == null)
        //    {
        //        _depRequestVM.StatusId = _unitOfWork.Statuses.GetAll().Where(a => a.StatusName == "PartialRental").Select(a => a.Id).FirstOrDefault();
        //        _depRequestVM.Verification = "Pending";
        //        _depRequestVM.RequestTypeId = _unitOfWork.RequestTypes.GetAll().Where(a => a.RequestName == "Reconfigure").Select(a => a.Id).FirstOrDefault();
        //    }



        //    DeploymentRequest depRequest = new DeploymentRequest()
        //    {
        //        Id = Guid.NewGuid(),
        //        BankId = _depRequestVM.BankId,
        //        InventoryAddId = _depRequestVM.InventoryAddId,
        //        SerialNumberId = _depRequestVM.SerialNumberId,
        //        SerialNumber = _unitOfWork.SerialNumbers.GetAll().Where(x => x.Id == _depRequestVM.SerialNumberId).Select(x => x.SerialKey).FirstOrDefault(),
        //        LocationForPartialRental = _depRequestVM.LocationForPartialRental,
        //        RemarksForPartialRental = _depRequestVM.RemarksForPartialRental,
        //        RequestTypeId = _depRequestVM.RequestTypeId,
        //        StatusId = _depRequestVM.StatusId,
        //        Verification = _depRequestVM.Verification,
        //        SoldDate = DateTime.Now
        //    };
        //    try
        //    {

        //        _unitOfWork.DeploymentRequests.Add(depRequest);

        //        _unitOfWork.SaveChanges();

        //    }
        //    catch (DbUpdateException)
        //    {
               
        //            throw;
                
        //    }
        //    return new StatusCodeResult(StatusCodes.Status201Created);
        //}

        //[HttpPost("newPartialDeploy")]
        //public IActionResult AddConfig([FromBody]DeploymentRequestViewModel _deployVM)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }


        //    Deployment deployRequest = new Deployment()
        //    {
        //        Id = Guid.NewGuid(),
        //        InventoryAddId = _configVM.InventoryAddId,
        //        SerialNumberId = _configVM.SerialNumberId,
        //       DeploymentRequestId=_configVM.DeploymentRequestId
        //    };
        //    try
        //    {

        //        _unitOfWork.Configurations.Add(configRequest);

        //        _unitOfWork.SaveChanges();

        //    }
        //    catch (DbUpdateException)
        //    {

        //        throw;

        //    }
        //    return new StatusCodeResult(StatusCodes.Status201Created);
        //}


        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Produces(typeof(PosStockViewModel))]
        public void Delete(Guid id)
        {
            var _stockToDelete = _unitOfWork.PosStocks.GetGuid(id);
            var serialNumberId = _stockToDelete.SerialNumberId;
            var serialNumber = _unitOfWork.SerialNumbers.GetGuid(serialNumberId);
            serialNumber.IsSold = false;
            try
            {
                _unitOfWork.SerialNumbers.Update(serialNumber);
                _unitOfWork.PosStocks.Remove(_stockToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }
       
    }
}
