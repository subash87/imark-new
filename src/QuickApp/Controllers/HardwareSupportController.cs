﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;


namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class HardwareSupportController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public HardwareSupportController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        // GET: api/<controller>
        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<HardwareSupportViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult Get(int page, int pageSize)
        {
            IQueryable<HardwareSupportViewModel> HardwareSupportQuery = _unitOfWork.HardwareSupports.GetAll().Join(_unitOfWork.Banks.GetAll(), a => a.BankId, b => b.Id, (a, b) => new { HardwareSupport = a, Bank = b })
                                                                
                                                                .Select(s => new HardwareSupportViewModel
                                                                {
                                                                    Id = s.HardwareSupport.Id,
                                                                    PrimaryItem=s.HardwareSupport.PrimaryItem,
                                                                    BankName=s.Bank.BankName,
                                                                    Manufacturer=s.HardwareSupport.Manufacturer,
                                                                    ContactPerson=s.HardwareSupport.ContactPerson,
                                                                    ContactNo=s.HardwareSupport.ContactNo

                                                                });
            return Ok(HardwareSupportQuery);

        }

        [HttpPost]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public ActionResult Post([FromBody] HardwareSupportViewModel _hardwareSupportVM)
        {
            try
            {
                _hardwareSupportVM.Id = Guid.NewGuid();
                var hwSupport = Mapper.Map<HardwareSupport>(_hardwareSupportVM);
                _unitOfWork.HardwareSupports.Add(hwSupport);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch
            {
                throw;
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromBody] HardwareSupportViewModel _hwSupportVM)
        {
            try
            {
                var hwSupport = Mapper.Map<HardwareSupport>(_hwSupportVM);
               
                _unitOfWork.HardwareSupports.Update(hwSupport);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch
            {
                throw;
            }
        }
        [HttpDelete("{id}")]
        [Produces(typeof(HardwareSupportViewModel))]
        public void Delete(Guid id)
        {
            var _hwSupportToDelete = _unitOfWork.HardwareSupports.GetGuid(id);
            try
            {
                _unitOfWork.HardwareSupports.Remove(_hwSupportToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }


    }

}
