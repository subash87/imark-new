﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;


namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class CheckerController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CheckerController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<CheckerViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(int page, int pageSize)
        {
            IEnumerable<Checker> checkerQuery = _unitOfWork.Checkers.GetAll();

            if (page != -1)
                checkerQuery = checkerQuery.Skip((page - 1) * pageSize);

            if (pageSize != -1)
                checkerQuery = checkerQuery.Take(pageSize);

            var checkerList = checkerQuery.ToList();

            List<CheckerViewModel> checkersVM = new List<CheckerViewModel>();

            foreach (var item in checkerList)
            {
                var checkerVM = Mapper.Map<CheckerViewModel>(item);


                checkersVM.Add(checkerVM);
            }

            return Ok(checkersVM);

        }
    }
}
