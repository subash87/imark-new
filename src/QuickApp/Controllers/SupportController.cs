﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class SupportController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;

        public SupportController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        // GET: api/Support
        //[HttpGet("Pagination/{page:int}/{pageSize:int}")]
        //[Produces(typeof(List<SupportViewModel>))]
        //[Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        //public ActionResult Get(int page, int pageSize)
        //{
        //    IEnumerable<Support> supportQuery = _unitOfWork.Supports.GetAll();
        //    if (page != -1)
        //        supportQuery = supportQuery.Skip((page - 1) * pageSize);
        //    if (pageSize != -1)
        //        supportQuery = supportQuery.Take(pageSize);
        //    var supportList = supportQuery.ToList();
        //    List<SupportViewModel> supportsVM = new List<SupportViewModel>();
        //    foreach(var item in supportList)
        //    {
        //        SupportViewModel supportVM = Mapper.Map<SupportViewModel>(item);
        //        supportsVM.Add(supportVM);
        //    }
        //    return Ok(supportsVM);
        //}

        [HttpGet("pagination/{page:int}/{pageSize:int}/{deploymentRequestId}")]
        [Produces(typeof(List<SupportViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(int page, int pageSize, Guid? deploymentRequestId)
        {
            //if (deploymentRequestId == null)
            //{
            //    SupportViewModel oSupport = new SupportViewModel();
            //    return Ok(oSupport);
            //}
            //else
            //{

            try
                {
                //var support = _unitOfWork.Supports.GetAll().GroupJoin(_unitOfWork.DeploymentRequests.GetAll(), a => a.DeploymentRequestId, b => b.Id, (a, b) => new SupportViewModel

                //{
                //    Id = a.Id,
                //    DeploymentRequestId = b.Select(x => x.Id).SingleOrDefault(),
                //    BankName = b.Select(x => x.Bank.BankName).FirstOrDefault(),
                //    Merchant = b.Select(x => x.Merchant).FirstOrDefault(),
                //    IdMerchant = b.Select(x => x.IdMerchant).FirstOrDefault(),
                //    Outlet = b.Select(x => x.Merchant).FirstOrDefault(),
                //    IdTerminal = b.Select(x => x.IdTerminal).FirstOrDefault(),
                //    Item = b.Select(x => x.InventoryAdd.PrimaryItem).FirstOrDefault()+" "+ b.Select(x => x.InventoryAdd.SecondaryItem).FirstOrDefault()+" "+ b.Select(x => x.InventoryAdd.TertiaryItem).FirstOrDefault()+" "+ b.Select(x => x.InventoryAdd.DetailItem).FirstOrDefault(),

                //    SerialKey = b.Select(x => x.Configuration.SerialNumber.SerialKey).FirstOrDefault(),
                //    Phone=a.Phone,
                //    Field=a.Field,
                //    Status=a.Status,
                //    Priority=a.Priority,
                //    Issue=a.Issue,
                //    ContactPerson=a.ContactPerson,
                //    ContactNo=a.ContactNo,
                //    ResolvedBy=a.ResolvedBy,
                //    Remarks=a.Remarks,
                //    VerifiedBy= _context.Users.Where(x => x.Id == a.CreatedBy).Select(s => s.FullName).FirstOrDefault(),
                //    Verification =a.Verification,
                //    VerifiedDate = a.VerifiedDate,
                //    SupportDate=a.SupportDate


                //}).GroupJoin(_unitOfWork.Configurations.GetAll(), a => a.DeploymentRequestId, b => b.DeploymentRequestId, (a, b) => new SupportViewModel

                //{
                //    Id = a.Id,
                //    DeploymentRequestId = a.DeploymentRequestId,
                //    BankName = a.BankName,
                //    Merchant = a.Merchant,
                //    IdMerchant = a.IdMerchant,
                //    Outlet = a.Outlet,
                //    IdTerminal = a.IdTerminal,
                //    Item = a.Item,

                //    SerialKey =a.SerialKey,
                //    Phone = a.Phone,
                //    Field = a.Field,
                //    Status = a.Status,
                //    Priority = a.Priority,
                //    Issue = a.Issue,
                //    ContactPerson = a.ContactPerson,
                //    ContactNo = a.ContactNo,
                //    ResolvedBy = a.ResolvedBy,
                //    Remarks = a.Remarks,
                //    VerifiedBy = a.VerifiedBy,
                //    Verification = a.Verification,
                //    VerifiedDate = a.VerifiedDate,
                //    SupportDate = a.SupportDate,
                //    ApplicationVersionId=b.Select(s=>s.ApplicationVersion.Id).SingleOrDefault(),
                //    BankId= b.Select(s => s.ApplicationVersion.BankId).SingleOrDefault(),
                //    ApplicationFormat = _unitOfWork.Banks.Find(x => x.Id == a.BankId).Select(s => s.BankCode).FirstOrDefault()+" "+ b.Select(s => s.ApplicationVersion.ApplicationVer).FirstOrDefault()+" "+
                //                        b.Select(s => s.ApplicationVersion.ApplicationDate.Value.ToShortDateString()).FirstOrDefault()+" "+ b.Select(s => s.ApplicationVersion.KernelType).FirstOrDefault()


                //}).ToList();
                IQueryable<SupportViewModel> support = _unitOfWork.Supports.GetAll().Join(_unitOfWork.DeploymentRequests.GetAll(), a => a.DeploymentRequestId, b => b.Id, (a, b) => new { Support = a, DeploymentRequest = b })
                                                          .Join(_unitOfWork.Configurations.GetAll(), c => c.DeploymentRequest.Id, d => d.DeploymentRequestId, (c, d) => new { c.Support, c.DeploymentRequest, Configuration = d })
                                                          .Join(_unitOfWork.ApplicationVersions.GetAll(), e => e.Configuration.ApplicationVersionId, f => f.Id, (e, f) => new { e.Support, e.DeploymentRequest, e.Configuration, Application = f })
                                                          .Join(_unitOfWork.Banks.GetAll(), e => e.Application.BankId, f => f.Id, (g, h) => new { g.Support, g.DeploymentRequest, g.Configuration, g.Application, Bank = h })
                                                           .Join(_context.Users, x => x.Support.UpdatedBy, y => y.Id, (x, y) => new { x.Support, x.DeploymentRequest, x.Configuration, x.Application, x.Bank, User=y})
                                                           .Join(_unitOfWork.Deployments.GetAll(), x => x.DeploymentRequest.Id, y => y.DeploymentRequestId, (x, y) => new { x.Support, x.DeploymentRequest, x.Configuration, x.Application, x.Bank,x.User, Deployment = y })
                                                          .Select(s => new SupportViewModel
                                                          {
                                                              Id = s.Support.Id,
                                                              DeploymentRequestId = s.DeploymentRequest.Id,
                                                              BankName = s.Bank.BankName,
                                                              BankCode = s.Bank.BankCode,
                                                              Merchant = s.DeploymentRequest.Merchant,
                                                              IdMerchant = s.DeploymentRequest.IdMerchant,
                                                              IdTerminal = s.DeploymentRequest.IdTerminal,
                                                              Outlet = s.DeploymentRequest.Outlet,
                                                              Address=s.DeploymentRequest.Address,
                                                              MerchantLocation=s.Deployment.MerchantLocation,
                                                              ReceivedBy=s.Deployment.ReceivedBy,
                                                              ReceivedContactNo=s.Deployment.Longitude,
                                                              ContactPerson = s.Support.ContactPerson,
                                                              ContactNo = s.Support.ContactNo,
                                                              Phone = s.Support.Phone,
                                                              Field = s.Support.Field,
                                                              Status = s.Support.Status,
                                                              Priority = s.Support.Priority,
                                                              Issue = s.Support.Issue,
                                                              ResolvedBy = s.Support.ResolvedBy,
                                                              Remarks = s.Support.Remarks,
                                                              VerifiedBy = s.User.FullName,
                                                              Verification = s.Support.Verification,
                                                              VerifiedDate = s.Support.VerifiedDate,
                                                              SupportDate = s.Support.SupportDate,
                                                              SerialKey = s.Configuration.SerialKey,
                                                              ApplicationFormat = s.Bank.BankCode + " " + s.Application.ApplicationVer + " " + s.Application.ApplicationDate.Value.ToShortDateString() + " " + s.Application.KernelType,
                                                              Item = s.DeploymentRequest.Inventory.PrimaryItem + ' ' + s.DeploymentRequest.Inventory.SecondaryItem + ' ' + s.DeploymentRequest.Inventory.TertiaryItem + ' ' + s.DeploymentRequest.Inventory.DetailItem                                                            
                                                          }).OrderByDescending(x=>x.SupportDate);



                if (deploymentRequestId == null) { 

                        return Ok(support);
                        }
                    else
                    {
                        return Ok(support.Where(a => a.DeploymentRequestId == deploymentRequestId && a.Status==true ).OrderByDescending(x => x.SupportDate).ToList());
                    }
                }
                catch (Exception ex)
                {
                    return new StatusCodeResult(StatusCodes.Status500InternalServerError);
                }
            


        }



        // GET: api/Support/5
        [HttpGet("{id}")]
        public ActionResult Get(Guid id)
        {
            try
            {
                Support support = _unitOfWork.Supports.GetGuid(id);
                return Ok(support);
            }
            catch(Exception)
            {
                throw;
            }
        }

        // GET: api/Support/5 for only required deployment request
        [HttpGet("support/{id}")]
        public ActionResult GetSupport(Guid id)
        {
            try
            {

                IEnumerable<Support> support = _unitOfWork.Supports.GetAll().Where(a=>a.DeploymentRequestId == id);
                return Ok(support);
            }
            catch (Exception)
            {
                throw;
            }
        }
        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]SupportViewModel _supportViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



                _supportViewModel.SupportDate = DateTime.UtcNow.AddHours(5).AddMinutes(45);
            
            Support support = new Support();
            support.SupportDate= DateTime.UtcNow.AddHours(5).AddMinutes(45);
            support = Mapper.Map<Support>(_supportViewModel);
            try
            {

                _unitOfWork.Supports.Add(support);
                _unitOfWork.SaveChanges();

            }
            catch (DbUpdateException)
            {
                if (ItemAlreadyExists(support.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }

        //// PUT: api/Support/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody]SupportViewModel _supportViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            if (_supportViewModel.Verification == "Verified")
            {
                
                _supportViewModel.VerifiedDate = DateTime.UtcNow.AddHours(5).AddMinutes(45);
            }
            var support = Mapper.Map<Support>(_supportViewModel);
            try
            {
                _unitOfWork.Supports.Update(support);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch
            {
                throw;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            var _supportToDelete = _unitOfWork.Supports.GetGuid(id);
            try
            {
                _unitOfWork.Supports.Remove(_supportToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }
        private bool ItemAlreadyExists(Guid Id)
        {
            int count = _unitOfWork.Configurations.Find(a => a.Id == Id).Count();
            if (count > 0)
            {
                return true;
            }
            else
                return false;
        }
    }
}
