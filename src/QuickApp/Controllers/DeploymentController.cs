﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class DeploymentController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly ApplicationDbContext _context;
        public DeploymentController(IUnitOfWork unitOfWork, ApplicationDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;

        }

        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<DeploymentViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(int page, int pageSize)
        {
            IQueryable<Deployment> deploymentQuery = _unitOfWork.Deployments.GetAll();

            if (page != -1)
                deploymentQuery = deploymentQuery.Skip((page - 1) * pageSize);

            if (pageSize != -1)
                deploymentQuery = deploymentQuery.Take(pageSize);

            var deploymentList = deploymentQuery.ToList();

            List<DeploymentViewModel> usersVM = new List<DeploymentViewModel>();

            foreach (var item in deploymentList)
            {
                var userVM = Mapper.Map<DeploymentViewModel>(item);


                usersVM.Add(userVM);
            }

            return Ok(usersVM);

        }

        

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                DeploymentRequest _request = _unitOfWork.DeploymentRequests.GetGuid(id);
                string StatusName = _unitOfWork.Statuses.GetGuid(_request.StatusId).StatusName;
                string RequestName = _unitOfWork.RequestTypes.GetGuid(_request.RequestTypeId).RequestName;
                Deployment _deployment = _unitOfWork.Deployments.GetAll().Where(a => a.DeploymentRequestId == id).FirstOrDefault();
                if (_deployment == null)
                {
                    DeploymentViewModel _deploy = new DeploymentViewModel();
                    _deploy.DeploymentRequestId = id;
                    _deploy.StatusName = StatusName;
                    _deploy.RequestName = RequestName;
                    return Ok(_deploy);
                }
                else
                {
                    DeploymentViewModel userVm = Mapper.Map<DeploymentViewModel>(_deployment);
                    userVm.StatusName = StatusName;
                    userVm.RequestName = RequestName;
                    return Ok(userVm);
                }

            }
            catch
            {
                throw;
            }
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody]DeploymentViewModel _deploymentVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //_deploymentVM.CreatedBy = user.FullName;
           
           

            Deployment configuration = new Deployment()
            {
                Id = Guid.NewGuid(),
                Connectivity = _deploymentVM.Connectivity,
                Verification = _deploymentVM.Verification,
                DeploymentRequestId = _deploymentVM.DeploymentRequestId,
                Image =_deploymentVM.Image,
                Latitude= _deploymentVM.Latitude,
                Longitude= _deploymentVM.Longitude,
                MerchantLocation =_deploymentVM.MerchantLocation,
                ReceivedBy = _deploymentVM.ReceivedBy,
                DeployDate=_deploymentVM.DeployDate,
                Remarks = _deploymentVM.Remarks,
                Scanned = _deploymentVM.Scanned,
               CreatedBy = _deploymentVM.CreatedBy



            };
            try
            {
                

                    _unitOfWork.Deployments.Add(configuration);

                    _unitOfWork.SaveChanges();
       
            }
            catch (DbUpdateException)
            {
                if (ItemAlreadyExists(configuration.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }
            return new StatusCodeResult(StatusCodes.Status201Created);
        }


        // PUT api/<controller>/5
        [HttpPut("{id}/{dispatch}")]
        public IActionResult Put([FromBody]DeploymentViewModel _deploymentVM, bool dispatch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var configId = _unitOfWork.Configurations.Find(x => x.DeploymentRequestId == _deploymentVM.DeploymentRequestId).Select(s=>s.Id).LastOrDefault();
            var config = _unitOfWork.Configurations.GetGuid(configId);
            //var serialNoId = _unitOfWork.Configurations.Find(x => x.SerialNumberId == config.SerialNumberId).Select(s => s.SerialNumberId).SingleOrDefault();
            //var serial = _unitOfWork.SerialNumbers.GetGuid(serialNoId);
            //var inventoryAddId = _unitOfWork.InventorysAdd.Find(x => x.Id == serial.InventoryAddId).Select(s => s.Id).SingleOrDefault();
            //PosStock _posStock = new PosStock();
            //var _posId = _unitOfWork.PosStocks.Find(x=>x.DeploymentId== _deploymentVM.DeploymentRequestId).Select(s=>s.Id).SingleOrDefault();
            //var _pos = _unitOfWork.PosStocks.GetGuid(_posId);
            var request = _unitOfWork.DeploymentRequests.GetGuid(_deploymentVM.DeploymentRequestId);
           
            if (_deploymentVM.Verification == null || _deploymentVM.Verification == "Pending"  )
            {
               
                _deploymentVM.Verification = "Pending";
            
            }
            if (_deploymentVM.Verification == "Pending" && _deploymentVM.Connectivity != null)
            {
                request.StatusId = _unitOfWork.Statuses.GetAll().Where(a => a.StatusName == "Deployed").Select(a => a.Id).FirstOrDefault();
            }
            //if (_deploymentVM.Verification == "Pending" && _pos !=null)
            //{
            //    _pos.Status = "PartialRentalDeployedVerified";
            //}
            if(_deploymentVM.Verification == "Verified")
            {
                request.StatusId = _unitOfWork.Statuses.GetAll().Where(a => a.StatusName == "DeployedVerified").Select(a => a.Id).FirstOrDefault();
                //var history = _unitOfWork.PosHistorys.GetAll().Where(a => a.DeploymentRequestId == _deploymentVM.DeploymentRequestId);
                _deploymentVM.CreatedDate=DateTime.UtcNow.AddHours(5).AddMinutes(45);
                //_deploymentVM.InventoryAddId = inventoryAddId;
                //_deploymentVM.CreatedBy = _context.Users.Where(x=>x.Id==_deploymentVM.CreatedBy).Select(s => s.Id).SingleOrDefault();
                //var current_User = _userManager.GetUserAsync(HttpContext.User).Result;
                //_deploymentVM.UpdatedBy = current_User.FullName;
            }

            //var serialNoId = _unitOfWork.Configurations.GetAll().Where(x => x.DeploymentRequestId == _deploymentVM.DeploymentRequestId).Select(s => s.SerialNumberId).SingleOrDefault();
            //SerialNumber _serialNo = new SerialNumber();
            //_serialNo.Id = _unitOfWork.SerialNumbers.Find(x => x.Id == serialNoId).Select(s => s.Id).SingleOrDefault();
            //_serialNo.InventoryAddId = _unitOfWork.SerialNumbers.Find(x => x.Id == serialNoId).Select(s=>s.InventoryAddId).SingleOrDefault();
            //_serialNo.IsDeployed = true;
            var deployment = Mapper.Map<Deployment>(_deploymentVM);
            deployment.Id = _deploymentVM.Id;


                try
                {
                if (dispatch == true)
                {
                    request.RequestTypeId = _unitOfWork.RequestTypes.Find(a => a.RequestName == "Delete").Select(a => a.Id).FirstOrDefault();
                    _unitOfWork.DeploymentRequests.Update(request);
                    _unitOfWork.Configurations.Remove(config);
                   

                    _unitOfWork.SaveChanges();
                }
                //else if(_pos!=null)
                //{
                //    _unitOfWork.DeploymentRequests.Update(request);
                //    _unitOfWork.Deployments.Update(deployment);
                //    _unitOfWork.PosStocks.Update(_pos);
                //    //_unitOfWork.SerialNumbers.Update(_serialNo);
                //    _unitOfWork.SaveChanges();
                //}
                else
                {
                    _unitOfWork.DeploymentRequests.Update(request);
                    _unitOfWork.Deployments.Update(deployment);

                    _unitOfWork.SaveChanges();
                }

                }
                catch (DbUpdateException)
                {

                    throw;

                }
      
            return new StatusCodeResult(StatusCodes.Status200OK);
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Produces(typeof(DeploymentViewModel))]
        public void Delete(Guid id)
        {
            var _deploymentToDelete = _unitOfWork.Deployments.GetGuid(id);
            try
            {
                _unitOfWork.Deployments.Remove(_deploymentToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw;
            }
        }
        private bool ItemAlreadyExists(Guid Id)
        {
            int count = _unitOfWork.Deployments.Find(a => a.Id == Id).Count();
            if (count > 0)
            {
                return true;
            }
            else
                return false;
        }
    }
}
