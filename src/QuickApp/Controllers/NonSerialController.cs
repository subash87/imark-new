﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuickApp.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickApp.Controllers
{
    [Route("api/[controller]")]
    public class NonSerialController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public NonSerialController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        // GET: api/<controller>
        [HttpGet("pagination/{page:int}/{pageSize:int}")]
        [Produces(typeof(List<InventoryandAddViewModel>))]
        [Authorize(Authorization.Policies.ViewAllUsersPolicy)]
        public async Task<ActionResult> Get(int page, int pageSize)
        {
            try
            {
                var inventorys = _unitOfWork.InventorysAdd.GetAll().Where(x => x.Inventory.HasSerialNo == false).ToList();
               
                var InventoryInfo = inventorys.GroupJoin(_unitOfWork.Inventorys.GetAll(), a => a.InventoryId, b => b.Id, (a, b) => new InventoryandAddViewModel
                {
                  
                    Id = a.Id,
                    InventoryId = a.InventoryId,
                    PrimaryItem = b.Select(s => s.PrimaryItem).FirstOrDefault(),
                    SecondaryItem = b.Select(s => s.SecondaryItem).FirstOrDefault(),
                    TertiaryItem = b.Select(s => s.TertiaryItem).FirstOrDefault(),
                    DetailItem = b.Select(s => s.DetailItem).FirstOrDefault(),
                    HasSerialNo = b.Select(s => s.HasSerialNo).FirstOrDefault(),


                    BuyDate = a.BuyDate,
                    TotalNumber = a.TotalNumber,

                    

                });
                var Inventory = InventoryInfo.GroupJoin(_unitOfWork.OtherStocks.GetAll(), a => a.Id, b => b.InventoryAddId, (a, b) => new InventoryandAddViewModel
                {

                    Id = a.Id,
                    PrimaryItem = a.PrimaryItem,
                    SecondaryItem = a.SecondaryItem,
                    TertiaryItem = a.TertiaryItem,
                    DetailItem = a.DetailItem,
                    HasSerialNo = a.HasSerialNo,
                    TotalNumber = a.TotalNumber,
                    BuyDate=a.BuyDate,
                    RemainingNumber = (a.TotalNumber - _unitOfWork.OtherStocks.GetAll().Where(x => x.InventoryAddId == a.Id).Select(x => x.SoldOut).Sum()),

                });

                return Ok(Inventory.Where(x => x.RemainingNumber > 0).OrderByDescending(x=>x.BuyDate).ToList());

                //return Ok(Inventory);

            }

            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }

       
      
       
    }
}
