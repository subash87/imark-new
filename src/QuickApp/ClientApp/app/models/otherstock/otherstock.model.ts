﻿

export class OtherStock {

    constructor(id?: string, deploymentRequestId?: string, partyName?: string, partyAddress?: string,
        phoneNo?: string, otherPartyId?: string, totalNumber?: number, soldOut?: number, remarks?: string, bankName?: string, merchant?: string, idMerchant?: string, outlet?: string, idTerminal?: string,
        inventoryAddId?: string, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string, remaining?: number, stockDate?:Date, receivedBy?:string,contactNo?:string,distributedBy?:string) {

        this.id = id;
        this.deploymentRequestId = deploymentRequestId;
        this.partyName = partyName;
        this.partyAddress = partyAddress;
        this.phoneNo = phoneNo;
        this.soldOut = soldOut;
        this.remarks = remarks;
        this.bankName = bankName;
        this.merchant = merchant;
        this.idMerchant = idMerchant;
        this.outlet = outlet;
        this.idTerminal = idTerminal;
        this.inventoryAddId = inventoryAddId;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.remaining = remaining;
        this.otherPartyId = otherPartyId;
        this.totalNumber = totalNumber;
        this.stockDate = stockDate;
        this.distributedBy = distributedBy;
        this.contactNo = contactNo;
        this.receivedBy = receivedBy;
    }


    public id: string;
    public deploymentRequestId: string;
    public partyName: string;
    public partyAddress: string;
    public phoneNo: string;
    public soldOut: number;
    public totalNumber: number;
    public remarks: string;
    public bankName: string;
    public merchant: string;
    public idMerchant: string;
    public outlet: string;
    public idTerminal: string;
    public inventoryAddId: string;
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public detailItem: string;
    public remaining: number;
    public otherPartyId: string;
    public stockDate: Date;
    public receivedBy: string;
    public distributedBy: string;
    public contactNo: string;

}