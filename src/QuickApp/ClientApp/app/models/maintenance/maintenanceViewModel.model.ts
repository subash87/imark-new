﻿export class MaintenanceViewModel {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, issue?: string, remarks?: string, maintenanceId?: string, createdBy?: string, createdDate?:Date) {
        this.id = id;
        this.issue = issue;
        this.remarks = remarks;
        this.maintenanceId = maintenanceId;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
    }


    public id: string;
    public issue: string;
    public remarks: string;
    public maintenanceId: string;
    public createdBy: string;
    public createdDate: Date;
}