﻿export class Bank {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, bankName?: string, contactNo1?: string, address?: string, contactPerson1?: string, contactPerson2?: string, contactNo2?: string, total?: number, previousMonth?: number, currentMonth?: number, data?: Array<any>, label?: string, pendingDeployment?: number, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string, totalDeploy?: number, totalSold?: number, totalStock?: number, status?: string, bankCode?: string,
        em?: number, gem?: number, iwl?: number, maintenanceNumber?: number, totalGem?: number, totalEm?:number, totalIwl?:number, rollNumber?:number) {
        this.id = id;
        this.bankName = bankName;
        this.contactNo1 = contactNo1;
        this.address = address;
        this.contactPerson1 = contactPerson1;
        this.contactPerson2 = contactPerson2;
        this.contactNo2 = contactNo2;
        this.total = total;
        this.previousMonth = previousMonth;
        this.currentMonth = currentMonth;
        this.data = [previousMonth, currentMonth, total];
        this.label = label;
        this.pendingDeployment = pendingDeployment;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.totalDeploy = totalDeploy;
        this.totalSold = totalSold;
        this.totalStock = totalStock;
        this.status = status;
        this.bankCode = bankCode;
        this.em = em;
        this.gem = gem;
        this.iwl = iwl;
        this.totalGem = totalGem;
        this.totalEm = totalEm;
        this.totalIwl = totalIwl;
        this.maintenanceNumber = maintenanceNumber;
        this.rollNumber = rollNumber;
    }


    public id: string;
    public bankName: string;
    public contactNo1: string;
    public address: string;
    public contactPerson1: string;
    public contactPerson2: string;
    public contactNo2: string;
    public total: number;
    public previousMonth: number;
    public currentMonth: number;
    public data: Array<any>;
    public label: string;
    public pendingDeployment: number;
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public detailItem: string;
    public totalDeploy: number;
    public totalSold: number;
    public totalStock: number;
    public status: string;
    public bankCode: string;
    public em: number;
    public gem: number;
    public iwl: number;
    public maintenanceNumber: number;
    public totalGem: number;
    public totalEm: number;
    public totalIwl: number;
    public rollNumber: number;
}