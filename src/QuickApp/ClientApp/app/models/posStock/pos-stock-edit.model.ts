﻿

export class PosStockEdit {

    constructor(id?: string, serialKey?: string, serialNumber?: string, serialNumberId?: string, inventoryAddId?: string, inventoryId?: string, bankId?: string, bankName?: string, location?: string, remarks?: string, stockDate?: Date, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string, soldOut?: number, soldNumber?: number, remainingNumber?: number, createdBy?: string, configureNumber?: number, maintenanceNumber?: number, terminalType?: string, item?: string) {
        this.id = id;
        this.bankName = bankName;
        this.location = location;
        this.remarks = remarks;
        this.stockDate = stockDate;
        this.serialKey = serialKey;
        this.serialNumber = serialNumber;
        this.serialNumberId = serialNumberId,
            this.inventoryAddId = inventoryAddId,
            this.bankId = bankId,
            this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.soldOut = soldOut;
        this.soldNumber = soldNumber;
        this.remainingNumber = remainingNumber;
        this.createdBy = createdBy;
        this.configureNumber = configureNumber;
        this.maintenanceNumber = maintenanceNumber;
        this.inventoryId = inventoryId;
        this.terminalType = terminalType;
        this.item = item;
    }

    public id: string;
    public bankName: string;
    public location: string;
    public remarks: string;
    public stockDate: Date;
    public serialKey: string;
    public serialNumber: string;
    public serialNumberId: string;
    public bankId: string;
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public detailItem: string;
    public soldOut: number;
    public soldNumber: number;
    public remainingNumber: number;
    public inventoryAddId: string;
    public createdBy: string;
    public configureNumber: number;
    public maintenanceNumber: number;
    public inventoryId: string;
    public terminalType: string;
    public item: string;
}