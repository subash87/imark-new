﻿

export class Status {

    constructor(id?: string, statusName?: string) {
        this.id = id;
        this.statusName = statusName;

    }


    public id: string;
    public statusName: string;

}