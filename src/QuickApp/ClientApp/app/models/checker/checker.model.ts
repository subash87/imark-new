﻿export class Checker {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, name?: string, statusId?: string, deploymentRequestId?: string) {
        this.id = id;
        this.name = name;
        this.statusId = statusId;
        this.deploymentRequestId = deploymentRequestId;
    }


    public id: string;
    public name: string;
    public statusId: string;
    public deploymentRequestId: string;
}