﻿

export class OtherParty {

    constructor(id?: string, partyName?: string, partyAddress?: string, phoneNo?: string) {
        this.id = id;
        this.partyName = partyName;
        this.partyAddress = partyAddress;
        this.phoneNo = phoneNo;

    }


    public id: string;
    public partyName: string;
    public partyAddress: string;
    public phoneNo: string;

}