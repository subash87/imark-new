﻿export class HwSupport {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, hwSupportId?: string, phone?: boolean, field?: boolean, status?: boolean,
        priority?: boolean, issue?: string, contactPerson?: string, contactNo?: string, resolvedBy?: string,
        remarks?: string, supportDate?: Date, verification?: string, verifiedBy?: string, verifiedDate?: Date, bankId?: string, bankName?: string) {
        this.id = id;
        this.hwSupportId;
        this.phone;
        this.field;
        this.status;
        this.priority;
        this.issue;
        this.contactPerson;
        this.contactNo;
        this.resolvedBy;
        this.remarks;
        this.supportDate;
        this.verification;
        this.verifiedBy;
        this.verifiedDate;
        this.bankId;
        this.bankName;
    }


    public id: string;
    public hwSupportId: string;
    public phone: boolean;
    public field: boolean;
    public status: boolean;
    public priority: boolean;
    public issue: string;
    public contactPerson: string;
    public contactNo: string;
    public resolvedBy: string;
    public remarks: string;
    public supportDate: Date;
    public verification: string;
    public verifiedBy: string;
    public verifiedDate: Date;
    public bankId: string;
    public bankName: string;
  
}