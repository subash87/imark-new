﻿

export class InventoryEdit {
    constructor(id?: string, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string, hasSerialNo?: boolean, connectivity?: string,
        remainingNumber?: number, stock?: number, deployed?: number, unDeployed?: number, total?: number, bankStatus?: string, item?: string, available?: number, configureNumber?: number, pendingDeploy?: number,
        deployNumber?: number, soldNumber?: number, maintenanceNumber?: number, notReceivedNumber?: number, damageNumber?: number, deploymentRequestId?: string, pendingDeploymentRequest?: number) {
        this.id = id;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.hasSerialNo = hasSerialNo;
        this.connectivity = connectivity;
        this.remainingNumber = remainingNumber;
        this.stock = stock;
        this.deployed = deployed;
        this.unDeployed = unDeployed;
        this.total = total;
        this.bankStatus = bankStatus;
        this.item = item;
        this.available = available;
        this.configureNumber = configureNumber;
        this.pendingDeploy = pendingDeploy;
        this.deployNumber = deployNumber;
        this.soldNumber = soldNumber;
        this.maintenanceNumber = maintenanceNumber;
        this.notReceivedNumber = notReceivedNumber;
        this.damageNumber = damageNumber;
        this.deploymentRequestId = deploymentRequestId;
        this.pendingDeploymentRequest = pendingDeploymentRequest;
    }




    public id: string;
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public detailItem: string;
    public hasSerialNo: boolean;
    public connectivity: string;
    public remainingNumber: number;
    public stock: number;
    public available: number;
    public deployed: number;
    public unDeployed: number;
    public total: number;
    public bankStatus: string;
    public item: string;
    public configureNumber: number;
    public pendingDeploy: number;
    public deployNumber: number;
    public maintenanceNumber: number;
    public notReceivedNumber: number;
    public soldNumber: number;
    public damageNumber: number;
    public deploymentRequestId: string;
    public pendingDeploymentRequest: number;


}