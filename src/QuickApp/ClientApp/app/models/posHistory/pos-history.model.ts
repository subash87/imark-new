﻿import { Data } from "@angular/router/src/config";



export class PosHistory {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type

    constructor(id?: string, deploymentRequestId?: string, bankId?: string, bankName?: string, merchant?: string, idMerchant?: string,
        outlet?: string, district?: string, address?: string, contactPerson?: string, contactNo?: string, idTerminal?: string,
        terminalType?: string, currency?: string, tipAdjustment?: boolean, manualTransaction?: boolean, preAuthorization?: boolean,
        primaryNacNumber?: string, secondaryNacNumber?: string, priority?: boolean, statusId?: number, statusName?: string,
        remarks?: string, requestName?: string, refund?: boolean, applicationVersionId?: string, applicationVersion?: string,
        applicationDate?: Date, serialNumber?: string, serialKey?: string, image?: string, latitude?: string, longitude?: string,
        merchantLocation?: string, receivedBy?: string, remarksDeploy?: string, scanned?: string, createdBy?: string, applicationDateVersion?: string,
        createdDate?: Date, connectivity?: string, deployDate?: Date, primaryItem?: string, secondaryItem?: string, tertiaryItem?: string, detailItem?: string, isReceived?: string, verifiedBy?: string, verifiedDate?: Date, item?: string, applicationFormat?: string,
        receivedContactNo?: string, serialNumberId?: string, previousSerial?: string, configStatus?:string, isSold?: string) {
        this.id = id;
        this.bankId = bankId;
        this.bankName = bankName;
        this.merchant = merchant;
        this.idMerchant = idMerchant;
        this.outlet = outlet;
        this.district = district;
        this.address = address;
        this.contactPerson = contactPerson;
        this.contactNo = contactNo;
        this.idTerminal = idTerminal;
        this.terminalType = terminalType;
        this.currency = currency;
        this.tipAdjustment = tipAdjustment;
        this.manualTransaction = manualTransaction;
        this.preAuthorization = preAuthorization;
        this.primaryNacNumber = primaryNacNumber;
        this.secondaryNacNumber = secondaryNacNumber;
        this.priority = priority;
        this.statusId = statusId;
        this.statusName = statusName;
        this.remarks = remarks;
        this.requestName = requestName;
        this.refund = refund;
        this.applicationVersionId = applicationVersionId;
        this.applicationVersion = applicationVersion;
        this.applicationDate = applicationDate;
        this.serialNumberId = serialNumberId;
        this.serialNumber = serialNumber;
        this.serialKey = serialKey;
        this.deployDate = deployDate;
        this.image = image;
        this.primaryItem = primaryItem;
        this.secondaryItem = secondaryItem;
        this.tertiaryItem = tertiaryItem;
        this.detailItem = detailItem;
        this.isReceived = isReceived;
        this.verifiedBy = verifiedBy;
        this.verifiedDate = verifiedDate;
        this.item = item;
        this.applicationFormat = applicationFormat;
        this.applicationDateVersion = applicationDateVersion;
        this.receivedContactNo = receivedContactNo;
        this.latitude = latitude;
        this.longitude = longitude;
        this.merchantLocation = merchantLocation;
        this.receivedBy = receivedBy;
        this.remarksDeploy = remarksDeploy;
        this.scanned = scanned;
        this.previousSerial = previousSerial;
        this.configStatus = configStatus;
        this.isSold = isSold;
        this.createdBy
        this.createdDate
        this.connectivity
    }

    public id: string;
    public deploymentRequestId: string
    public bankId: string;
    public bankName: string;
    public merchant: string;
    public idMerchant: string;
    public outlet: string;
    public district: string;
    public address: string;
    public contactPerson: string;
    public contactNo: string;
    public idTerminal: string;
    public terminalType: string;
    public currency: string;
    public tipAdjustment: boolean;
    public manualTransaction: boolean;
    public preAuthorization: boolean;
    public primaryNacNumber: string;
    public secondaryNacNumber: string;
    public priority: boolean;
    public statusId: number;
    public statusName: string;
    public remarks: string;
    public requestName: string;
    public refund: boolean;
    public applicationVersionId: string;
    public applicationVersion: string
    public applicationDate: Date;
    public serialNumberId: string;
    public serialNumber: string;
    public serialKey: string;
    public image: string;
    public latitude: string;
    public longitude: string;
    public merchantLocation: string;
    public receivedBy: string;
    public remarksDeploy: string;
    public scanned: string;
    public createdBy: string;
    public createdDate: Date;
    public deployDate: Date;
    public connectivity: string;
    public primaryItem: string;
    public secondaryItem: string;
    public tertiaryItem: string;
    public detailItem: string;
    public isReceived: string;
    public verifiedBy: string;
    public verifiedDate: Date;
    public item: string;
    public applicationFormat: string;
    public applicationDateVersion: string;
    public receivedContactNo: string;
    public previousSerial: string;
    public configStatus: string;
    public isSold: string;


}