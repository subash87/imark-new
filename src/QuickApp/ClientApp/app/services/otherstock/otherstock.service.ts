﻿
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { OtherStockEndpoint } from './otherstock-endpoint.service';
import { AuthService } from '../auth.service';
import { OtherStock } from '../../models/otherstock/otherstock.model';
import { OtherStockEdit } from '../../models/otherstock/otherstock-edit.model';
import { OtherParty } from '../../models/otherparty/otherparty.model';
import { OtherPartyEdit } from '../../models/otherparty/otherparty-edit.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';




export type OtherStockChangedOperation = "add" | "delete" | "modify";
export type OtherStockChangedEventArg = { deploy: OtherStock[] | string[], operation: OtherStockChangedOperation };



@Injectable()
export class OtherStockService {

    public static readonly otherStockAddedOperation: OtherStockChangedOperation = "add";
    public static readonly otherStockDeletedOperation: OtherStockChangedOperation = "delete";
    public static readonly otherStockModifiedOperation: OtherStockChangedOperation = "modify";

    private _otherStockChanged = new Subject<OtherStockChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private OtherStockEndpoint: OtherStockEndpoint) {

    }


    getOtherStock(otherStockId?: string) {
        return this.OtherStockEndpoint.getOtherStockEndpoint<OtherStock>(otherStockId);
    }


    getOtherStockInfo(page?: number, pageSize?: number, deploymentRequestId?: string) {
        return this.OtherStockEndpoint.getOtherStocksEndpoint<OtherStock[]>(page, pageSize, deploymentRequestId);
    }
    getOtherPartyInfo(page?: number, pageSize?: number) {
        return this.OtherStockEndpoint.getOtherPartysEndpoint<OtherParty[]>(page, pageSize);
    }

    updateOtherStock(otherStock: OtherStockEdit) {
        if (otherStock.id) {
            return this.OtherStockEndpoint.getUpdateOtherStockEndpoint(otherStock, otherStock.id);
        }
        else {

            return this.OtherStockEndpoint.getOtherStockByTotalEndpoint<OtherStock>(otherStock.id)

                .mergeMap(foundUser => {
                    otherStock.id = foundUser.id;
                    return this.OtherStockEndpoint.getUpdateOtherStockEndpoint(otherStock, otherStock.id)
                });
        }
    }


    newOtherStock(otherStock: OtherStockEdit) {
        return this.OtherStockEndpoint.getNewOtherStockEndpoint<OtherStock>(otherStock );
    }




    deleteOtherStock(otherStockOrotherStockId: string | OtherStockEdit): Observable<OtherStock> {

        if (typeof otherStockOrotherStockId === 'string' || otherStockOrotherStockId instanceof String) {
            return this.OtherStockEndpoint.getDeleteOtherStockEndpoint<OtherStock>(<string>otherStockOrotherStockId);
        }
        else {

            if (otherStockOrotherStockId.id) {
                return this.deleteOtherStock(otherStockOrotherStockId.id);
            }
            else {
                return this.OtherStockEndpoint.getOtherStockByTotalEndpoint<OtherStock>(otherStockOrotherStockId.id)
                    .mergeMap(otherStock => this.deleteOtherStock(otherStockOrotherStockId.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}