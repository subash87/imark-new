﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class OtherStockEndpoint extends EndpointFactory {

    private readonly _otherStockUrl: string = "/api/otherstock";
    private readonly _otherStocDepkUrl: string = "/api/otherstock/id";
    private readonly _otherStockPageUrl: string = "/api/otherstock/pagination";
    private readonly _otherPartyPageUrl: string = "/api/otherparty/pagination";
    private readonly _currentUserUrl: string = "/api/account/users/me";



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }




    getOtherStockEndpoint<T>(otherStockId?: string): Observable<T> {
        let endpointUrl = otherStockId ? `${this._otherStockUrl}/${otherStockId}` : this._otherStockUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getOtherStockEndpoint(otherStockId));
            });
    }



    getOtherStockByTotalEndpoint<T>(otherStockName: string): Observable<T> {
        let endpointUrl = `${this._otherStockUrl}/${otherStockName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getOtherStockByTotalEndpoint(otherStockName));
            });
    }



    getOtherStocksEndpoint<T>(page?: number, pageSize?: number, deploymentRequestId?: string): Observable<T> {
        let endpointUrl = `${this._otherStockPageUrl}/${-1}/${-1}/${deploymentRequestId}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getOtherStocksEndpoint(page, pageSize));
            });
    }
    getOtherPartysEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._otherPartyPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getOtherPartysEndpoint(page, pageSize));
            });
    }


    getNewOtherStockEndpoint<T>(userObject: any): Observable<T> {


        return this.http.post<T>(this._otherStockUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewOtherStockEndpoint(userObject));
            });
    }
       


    getUpdateOtherStockEndpoint<T>(userObject: any, otherStockId?: string): Observable<T> {
        let endpointUrl = otherStockId ? `${this._otherStockUrl}/${otherStockId}` : this._otherStockUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateOtherStockEndpoint(userObject, otherStockId));
            });
    }






    getDeleteOtherStockEndpoint<T>(otherStockId: string): Observable<T> {
        let endpointUrl = `${this._otherStockUrl}/${otherStockId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteOtherStockEndpoint(otherStockId));
            });
    }




}