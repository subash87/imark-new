﻿
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { InventoryEndpoint } from './inventory-endpoint.service';
import { AuthService } from './auth.service';
import { Inventory } from '../models/inventory.model';
import { InventoryEdit } from '../models/inventory-edit.model';
import { InventoryAdd } from '../models/inventory-add.model';
import { Permission, PermissionNames, PermissionValues } from '../models/permission.model';




export type InventoryChangedOperation = "add" | "delete" | "modify";
export type InventoryChangedEventArg = { inventory: Inventory[] | string[], operation: InventoryChangedOperation };



@Injectable()
export class InventoryService {

    public static readonly inventoryAddedOperation: InventoryChangedOperation = "add";
    public static readonly inventoryDeletedOperation: InventoryChangedOperation = "delete";
    public static readonly inventoryModifiedOperation: InventoryChangedOperation = "modify";

    private _inventoryChanged = new Subject<InventoryChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private InventoryEndpoint: InventoryEndpoint) {

    }


    getInventory(inventoryId?: string) {
        return this.InventoryEndpoint.getInventoryEndpoint<Inventory>(inventoryId);
    }

    //getUserAndRoles(userId?: string) {

    //    return Observable.forkJoin(
    //        this.accountEndpoint.getUserEndpoint<User>(userId),
    //        this.accountEndpoint.getRolesEndpoint<Role[]>());
    //}

    getInventoryInfo(page?: number, pageSize?: number, hasSerial?: boolean) {
        return this.InventoryEndpoint.getInventoriesEndpoint<Inventory[]>(page, pageSize, hasSerial);
    }

    //getUsersAndRoles(page?: number, pageSize?: number) {

    //    return Observable.forkJoin(
    //        this.accountEndpoint.getUsersEndpoint<User[]>(page, pageSize),
    //        this.accountEndpoint.getRolesEndpoint<Role[]>());
    //}


    updateInventory(inventory: InventoryEdit) {
        if (inventory.id) {
            return this.InventoryEndpoint.getUpdateInventoryEndpoint(inventory, inventory.id);
        }
        else {

            return this.InventoryEndpoint.getInventoryByPrimaryItemEndpoint<Inventory>(inventory.id)

                .mergeMap(foundUser => {
                    inventory.id = foundUser.id;
                    return this.InventoryEndpoint.getUpdateInventoryEndpoint(inventory, inventory.id)
                });
        }
    }


    newInventory(inventory: InventoryEdit) {
        return this.InventoryEndpoint.getNewInventoryEndpoint<Inventory>(inventory);
    }

    addInventory(inventory: InventoryAdd) {
        return this.InventoryEndpoint.getAddInventoryEndpoint<InventoryAdd>(inventory);
    }



    deleteInventory(inventoryOrInventoryId: string | InventoryEdit): Observable<Inventory> {

        if (typeof inventoryOrInventoryId === 'string' || inventoryOrInventoryId instanceof String) {
            return this.InventoryEndpoint.getDeleteInventoryEndpoint<Inventory>(<string>inventoryOrInventoryId);
                //.do(data => this.onInventoryCountChanged([data], AccountService.posDeletedOperation));
        }
        else {

            if (inventoryOrInventoryId.id) {
                return this.deleteInventory(inventoryOrInventoryId.id);
            }
            else {
                return this.InventoryEndpoint.getInventoryByPrimaryItemEndpoint<Inventory>(inventoryOrInventoryId.primaryItem)
                    .mergeMap(inventory => this.deleteInventory(inventory.id));
            }
        }
    }


   
    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}