﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class RequestTypeEndpoint extends EndpointFactory {

    private readonly _requestUrl: string = "/api/requestType";
    private readonly _requestPageUrl: string = "/api/requestType/pagination";

    private readonly _currentUserUrl: string = "/api/account/users/me";



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }


    getRequestsEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._requestPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getRequestsEndpoint(page, pageSize));
            });
    }
}