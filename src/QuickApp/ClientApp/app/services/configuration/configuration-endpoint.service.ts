﻿import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';

import { ConfigurationService } from '../configuration.service';


@Injectable()
export class ConfigurationEndpoint extends EndpointFactory {

    private readonly _configurationUrl: string = "/api/configuration";
    private readonly _configurationStatusUrl: string = "/api/configuration/addConfigStatus";
    private readonly _serialNotReceivedUrl: string = "/api/configuration/notReceivedPos";
    private readonly _notReceivedSerialPageUrl: string = "/api/notReceivedPos/pagination";
    private readonly _notReceivedUrl: string = "/api/notReceivedPos";
    private readonly _serialConfigurationUrl: string = "/api/maintenance/serialList";
    private readonly _configurationPageUrl: string = "/api/configuration/pagination";
    private readonly _damagePosPageUrl: string = "/api/damagePos/pagination";
    private readonly _damagePosUrl: string = "/api/damagePos";
  

    private readonly _currentUserUrl: string = "/api/account/users/me";
    get currentUserUrl() { return this.configurations.baseUrl + this._currentUserUrl; }



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }

    getConfigurationEndpoint<T>(configurationId?: string): Observable<T> {
        let endpointUrl = configurationId ? `${this._configurationUrl}/${configurationId}` : this._configurationUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getConfigurationEndpoint(configurationId));
            });
    }



    getConfigurationByPrimaryItemEndpoint<T>(primaryName: string): Observable<T> {
        let endpointUrl = `${this._configurationUrl}/${primaryName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getConfigurationByPrimaryItemEndpoint(primaryName));
            });
    }
    getNotReceivedByPrimaryItemEndpoint<T>(primaryName: string): Observable<T> {
        let endpointUrl = `${this._configurationUrl}/${primaryName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNotReceivedByPrimaryItemEndpoint(primaryName));
            });
    }
   

    getConfigurationsEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._configurationPageUrl}/${-1}/${-1}`;
        //let endpointUrl = page && pageSize ? `${this._inventoryPageUrl}/${page}/${pageSize}` : this._inventoryPageUrl;
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getConfigurationsEndpoint(page, pageSize));
            });
    }
    getSerialConfigurationsEndpoint<T>(page?: number, pageSize?: number, serialId?:string): Observable<T> {
        let endpointUrl = `${this._serialConfigurationUrl}/${-1}/${-1}/{serialId}`;
    
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSerialConfigurationsEndpoint(page, pageSize, serialId));
            });
    }

    getNotReceivedSerialPageEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._notReceivedSerialPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSerialConfigurationsEndpoint(page, pageSize));
            });
    }

    getDamagePosPageEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._damagePosPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDamagePosPageEndpoint(page, pageSize));
            });
    }

    getNewConfigurationEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._configurationUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewConfigurationEndpoint(userObject));
            });
    }

 
    getNotReceivedSerialEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._serialNotReceivedUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNotReceivedSerialEndpoint(userObject));
            });
    }
    getNewDamagePosEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._damagePosUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewDamagePosEndpoint(userObject));
            });
    }


    getUpdateConfigurationEndpoint<T>(userObject: any, configurationId?: string): Observable<T> {
        let endpointUrl = configurationId ? `${this._configurationUrl}/${configurationId}` : this._configurationUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateConfigurationEndpoint(userObject, configurationId));
            });
    }
    getUpdateConfigurationStatusEndpoint<T>(userObject: any, configurationId?: string): Observable<T> {
        let endpointUrl = configurationId ? `${this._configurationStatusUrl}/${configurationId}` : this._configurationStatusUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateConfigurationStatusEndpoint(userObject, configurationId));
            });
    }
    getUpdateNotReceivedEndpoint<T>(userObject: any, notReceivedId?: string): Observable<T> {
        let endpointUrl = notReceivedId ? `${this._notReceivedUrl}/${notReceivedId}` : this._notReceivedUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateNotReceivedEndpoint(userObject, notReceivedId));
            });
    }
   


    getDeleteConfigurationEndpoint<T>(configurationId: string): Observable<T> {
        let endpointUrl = `${this._configurationUrl}/${configurationId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteConfigurationEndpoint(configurationId));
            });
    }

   



}