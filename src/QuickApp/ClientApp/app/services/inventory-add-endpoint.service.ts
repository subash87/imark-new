﻿
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';


@Injectable()
export class InventoryAddEndpoint extends EndpointFactory {

 
    private readonly _inventoryAddUrl: string = "/api/inventoryAdd";
    private readonly _terminalTypePageUrl: string = "/api/inventoryAdd/terminalType";
    private readonly _inventoryStockPageUrl: string = "/api/inventoryAdd/pagination";
    private readonly _inventoryAddPosStockPageUrl: string = "/api/inventoryAdd/posStock";
    private readonly _inventoryPage1Url: string = "/api/nonserial/pagination";
    private readonly _posStockHistoryPageUrl: string = "/api/posStock/soldHistory";
   
    private readonly _currentUserUrl: string = "/api/account/users/me";
    


    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }




    getInventoryAddEndpoint<T>(inventoryAddId?: string): Observable<T> {
        let endpointUrl = inventoryAddId ? `${this._inventoryAddUrl}/${inventoryAddId}` : this._inventoryAddUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getInventoryAddEndpoint(inventoryAddId));
            });
    }



    getInventoryByTotalEndpoint<T>(inventoryAddName: string): Observable<T> {
        let endpointUrl = `${this._inventoryAddUrl}/${inventoryAddName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getInventoryByTotalEndpoint(inventoryAddName));
            });
    }



    //getAddInventoriesEndpoint<T>(page?: number, pageSize?: number, deploymentRequestId?: string): Observable<T> {
    //    let endpointUrl = `${this._inventoryAddPageUrl}/${-1}/${-1}/${deploymentRequestId}`;

    //    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
    //        .catch(error => {
    //            return this.handleError(error, () => this.getAddInventoriesEndpoint(page, pageSize));
    //        });
    //}
    getAddInventoriesListEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._inventoryStockPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getAddInventoriesListEndpoint(page, pageSize));
            });
    }
    getInventoriesListEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._terminalTypePageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getInventoriesListEndpoint(page, pageSize));
            });
    }
    getPosStockEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._inventoryAddPosStockPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosStockEndpoint(page, pageSize));
            });
    }
    getPosStockHistoryEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._posStockHistoryPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosStockHistoryEndpoint(page, pageSize));
            });
    }

    getAddInventories1Endpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._inventoryPage1Url}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getAddInventories1Endpoint(page, pageSize));
            });
    }

    getNewAddInventoryEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._inventoryAddUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewAddInventoryEndpoint(userObject));
            });
    }
    getAddInventoryEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._inventoryAddUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewAddInventoryEndpoint(userObject));
            });
    }

    getUpdateAddInventoryEndpoint<T>(userObject: any, addInventoryId?: string): Observable<T> {
        let endpointUrl = addInventoryId ? `${this._inventoryAddUrl}/${addInventoryId}` : this._inventoryAddUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateAddInventoryEndpoint(userObject, addInventoryId));
            });
    }

   


  

    getDeleteInventoryAddEndpoint<T>(inventoryAddId: string): Observable<T> {
        let endpointUrl = `${this._inventoryAddUrl}/${inventoryAddId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteInventoryAddEndpoint(inventoryAddId));
            });
    }




    //getInventoryEndpoint<T>(inventoryId: string): Observable<T> {
    //    let endpointUrl = `${this._inventoryUrl}/${inventoryId}`;

    //    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
    //        .catch(error => {
    //            return this.handleError(error, () => this.getInventoryEndpoint(inventoryId));
    //        });
    //}


    //getInventoryEndpoint<T>(inventoryName: string): Observable<T> {
    //    let endpointUrl = `${this._inventoryUrl}/${inventoryName}`;

    //    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
    //        .catch(error => {
    //            return this.handleError(error, () => this.getInventoryEndpoint(inventoryName));
    //        });
    //}



    //getInventoryEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
    //    let endpointUrl = '${this._inventoryPageUrl}/${-1}/${-1}';

    //    return this.http.get<T>(endpointUrl, this.getRequestHeaders())
    //        .catch(error => {
    //            return this.handleError(error, () => this.getInventoryEndpoint(page, pageSize));
    //        });
    //}


}