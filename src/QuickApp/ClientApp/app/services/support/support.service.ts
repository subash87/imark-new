﻿
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { SupportEndpoint } from './support-endpoint.service';
import { AuthService } from '../auth.service';
import { Support } from '../../models/support/support.model';
import { SupportEdit } from '../../models/support/support-edit.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';




export type SupportChangedOperation = "add" | "delete" | "modify";
export type SupportChangedEventArg = { support: Support[] | string[], operation: SupportChangedOperation };



@Injectable()
export class SupportService {

    public static readonly supportAddedOperation: SupportChangedOperation = "add";
    public static readonly supportDeletedOperation: SupportChangedOperation = "delete";
    public static readonly supportModifiedOperation: SupportChangedOperation = "modify";

    private _supportChanged = new Subject<SupportChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private SupportEndpoint: SupportEndpoint) {

    }


    getSupport(supportId?: string) {
        return this.SupportEndpoint.getSupportEndpoint<Support>(supportId);
    }


    getSupportInfo(page?: number, pageSize?: number, id?:string) {
        return this.SupportEndpoint.getSupportsEndpoint<Support[]>(page, pageSize, id);
    }


    updateSupport(support: SupportEdit) {
        if (support.id) {
            return this.SupportEndpoint.getUpdateSupportEndpoint(support, support.id);
        }
        else {

            return this.SupportEndpoint.getSupportByTotalEndpoint<Support>(support.id)

                .mergeMap(foundUser => {
                    support.id = foundUser.id;
                    return this.SupportEndpoint.getUpdateSupportEndpoint(support, support.id)
                });
        }
    }


    newSupport(support: SupportEdit) {
        return this.SupportEndpoint.getNewSupportEndpoint<Support>(support);
    }




    deleteSupport(supportOrSupportId: string | SupportEdit): Observable<Support> {

        if (typeof supportOrSupportId === 'string' || supportOrSupportId instanceof String) {
            return this.SupportEndpoint.getDeleteSupportEndpoint<Support>(<string>supportOrSupportId);
        }
        else {

            if (supportOrSupportId.id) {
                return this.deleteSupport(supportOrSupportId.id);
            }
            else {
                return this.SupportEndpoint.getSupportByTotalEndpoint<Support>(supportOrSupportId.id)
                    .mergeMap(support => this.deleteSupport(support.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}