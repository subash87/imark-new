﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class SupportEndpoint extends EndpointFactory {

    private readonly _supportUrl: string = "/api/support";
    private readonly _supportPageUrl: string = "/api/support/pagination";

    private readonly _currentUserUrl: string = "/api/account/users/me";



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }




    getSupportEndpoint<T>(supportId?: string): Observable<T> {
        let endpointUrl = supportId ? `${this._supportUrl}/${supportId}` : this._supportUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSupportEndpoint(supportId));
            });
    }



    getSupportByTotalEndpoint<T>(supportName: string): Observable<T> {
        let endpointUrl = `${this._supportUrl}/${supportName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSupportByTotalEndpoint(supportName));
            });
    }



    getSupportsEndpoint<T>(page?: number, pageSize?: number, id?: string): Observable<T> {
        let endpointUrl = `${this._supportPageUrl}/${-1}/${-1}/${id}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSupportsEndpoint(page, pageSize, id));
            });
    }



    getNewSupportEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._supportUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewSupportEndpoint(userObject));
            });
    }


    getUpdateSupportEndpoint<T>(userObject: any, supportId?: string): Observable<T> {
        let endpointUrl = supportId ? `${this._supportUrl}/${supportId}` : this._supportUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateSupportEndpoint(userObject, supportId));
            });
    }






    getDeleteSupportEndpoint<T>(supportId: string): Observable<T> {
        let endpointUrl = `${this._supportUrl}/${supportId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteSupportEndpoint(supportId));
            });
    }




}