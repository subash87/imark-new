﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class DashboardEndPoint extends EndpointFactory {

    private readonly _dashboardUrl: string = "/api/dashboard";
    private readonly _dashboardPageUrl: string = "/api/dashboard/pagination";
    private readonly _dashboardStockPageUrl: string = "/api/dashboard/stock";
    private readonly _currentUserUrl: string = "/api/account/users/me";



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }


    getBankDeployEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._dashboardPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getBankDeployEndpoint(page, pageSize));
            });
    }

    getStockDashboardEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._dashboardStockPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getStockDashboardEndpoint(page, pageSize));
            });
    }
}