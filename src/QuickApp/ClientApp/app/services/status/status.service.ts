﻿


import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { StatusEndpoint } from './status-endpoint.service';
import { AuthService } from '../auth.service';
import { Status } from '../../models/status.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';





@Injectable()
export class StatusService {

    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private StatusEndpoint: StatusEndpoint) {

    }

    getStatusInfo(page?: number, pageSize?: number) {
        return this.StatusEndpoint.getStatusesEndpoint<Status[]>(page, pageSize);
    }




    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}