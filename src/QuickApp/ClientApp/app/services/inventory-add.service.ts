﻿

import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { InventoryAddEndpoint } from './inventory-add-endpoint.service';
import { AuthService } from './auth.service';
import { Inventory } from '../models/inventory.model';
import { InventoryAddEdit } from '../models/inventory-add-edit.model';
import { InventoryAdd } from '../models/inventory-add.model';
import { Permission, PermissionNames, PermissionValues } from '../models/permission.model';
import { PosStock } from '../models/posStock/pos-stock.model';




export type InventoryAddChangedOperation = "add" | "delete" | "modify";
export type InventoryAddChangedEventArg = { inventory: InventoryAdd[] | string[], operation: InventoryAddChangedOperation };



@Injectable()
export class InventoryAddService {

    public static readonly inventoryAddedOperation: InventoryAddChangedOperation = "add";
    public static readonly inventoryDeletedOperation: InventoryAddChangedOperation = "delete";
    public static readonly inventoryModifiedOperation: InventoryAddChangedOperation = "modify";

    private _inventoryChanged = new Subject<InventoryAddChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private InventoryAddEndpoint: InventoryAddEndpoint) {

    }


    getInventoryAdd(inventoryId?: string) {
        return this.InventoryAddEndpoint.getInventoryAddEndpoint<InventoryAdd>(inventoryId);
    }

    getAddInventoryList(page?: number, pageSize?: number) {
        return this.InventoryAddEndpoint.getAddInventoriesListEndpoint<InventoryAdd[]>(page, pageSize);
    }
    getInventoryList(page?: number, pageSize?: number) {
        return this.InventoryAddEndpoint.getInventoriesListEndpoint<Inventory[]>(page, pageSize);
    }
    getPosStockInfo(page?: number, pageSize?: number, ) {
        return this.InventoryAddEndpoint.getPosStockEndpoint<Inventory[]>(page, pageSize);
    }
    getPosStockHistoryInfo(page?: number, pageSize?: number, ) {
        return this.InventoryAddEndpoint.getPosStockHistoryEndpoint<PosStock[]>(page, pageSize);
    }

    getAddInventory1Info(page?: number, pageSize?: number) {
        return this.InventoryAddEndpoint.getAddInventories1Endpoint<InventoryAdd[]>(page, pageSize);
    }



    updateAddInventory(inventory: InventoryAddEdit) {
        if (inventory.id) {
            return this.InventoryAddEndpoint.getUpdateAddInventoryEndpoint(inventory, inventory.id);
        }
        else {

            return this.InventoryAddEndpoint.getInventoryByTotalEndpoint<InventoryAdd>(inventory.id)

                .mergeMap(foundUser => {
                    inventory.id = foundUser.id;
                    return this.InventoryAddEndpoint.getUpdateAddInventoryEndpoint(inventory, inventory.id)
                });
        }
    }


    newAddInventory(inventory: InventoryAddEdit) {
        return this.InventoryAddEndpoint.getNewAddInventoryEndpoint<InventoryAdd>(inventory);
    }

    addAddInventory(inventory: InventoryAddEdit) {
        return this.InventoryAddEndpoint.getAddInventoryEndpoint<InventoryAdd>(inventory);
    }



    deleteAddInventory(inventoryOrInventoryId: string | InventoryAddEdit): Observable<InventoryAdd> {

        if (typeof inventoryOrInventoryId === 'string' || inventoryOrInventoryId instanceof String) {
            return this.InventoryAddEndpoint.getDeleteInventoryAddEndpoint<InventoryAdd>(<string>inventoryOrInventoryId);
                //.do(data => this.onInventoryAddCountChanged(data.inventoryId));
        }
        else {

            if (inventoryOrInventoryId.id) {
                return this.deleteAddInventory(inventoryOrInventoryId.id);
            }
            else {
                return this.InventoryAddEndpoint.getInventoryByTotalEndpoint<InventoryAdd>(inventoryOrInventoryId.id)
                    .mergeMap(inventory => this.deleteAddInventory(inventory.id));
            }
        }
    }


    private onInventoryAddChanged(inventory: InventoryAdd[] | string[], op: InventoryAddChangedOperation) {
        this._inventoryChanged.next({ inventory: inventory, operation: op });
    }


    onInventoryAddCountChanged(roles: InventoryAdd[] | string[]) {
        return this.onInventoryAddChanged(roles, InventoryAddService.inventoryModifiedOperation);
    }


    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}