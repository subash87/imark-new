﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';


@Injectable()
export class SerialEndpoint extends EndpointFactory {

    private readonly _serialUrl: string = "/api/serialNumber";
    //private readonly _serialAddUrl: string = "/api/serialAdd";
    private readonly _serialPageUrl: string = "/api/serialNumber/pagination";
    private readonly _serialStockUrl: string = "/api/serialNumber/posStock";
    //private readonly _inventoryDelete: string = "/api/inventory/delete";
    private readonly _currentUserUrl: string = "/api/account/users/me";
    //get posUrl() { return this._posUrl + this.posUrl; }
    get currentUserUrl() { return this.configurations.baseUrl + this._currentUserUrl; }



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }

    getSerialPageEndpoint<T>(page?: number, pageSize?: number, id?: string): Observable<T> {
        let endpointUrl = `${this._serialPageUrl}/${-1}/${-1}/${id}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSerialPageEndpoint(page, pageSize, id));
            });
    }

    getSerialBySerialKeyEndpoint<T>(serialKey: string): Observable<T> {
        let endpointUrl = `${this._serialUrl}/${serialKey}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSerialBySerialKeyEndpoint(serialKey));
            });
    }

    getSerialsEndpoint<T>(inventoryAddId?: string, config?: string): Observable<T> {
        let endpointUrl = `${this._serialUrl}/${inventoryAddId}/${config}`;
        ////let endpointUrl = page && pageSize ? `${this._inventoryPageUrl}/${page}/${pageSize}` : this._inventoryPageUrl;
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSerialsEndpoint(inventoryAddId, config));
            });
    }
    getPosStockEndpoint<T>(inventoryAddId?: string): Observable<T> {
        let endpointUrl = `${this._serialStockUrl}/${inventoryAddId}`;
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosStockEndpoint(inventoryAddId));
            });
    }

    getSerialsStockEndpoint<T>(inventoryAddId?: string): Observable<T> {
        let endpointUrl = `${this._serialUrl}/${inventoryAddId}`;
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSerialsStockEndpoint(inventoryAddId));
            });
    }

      getUpdateSerialEndpoint<T>(userObject: any, serialId?: string): Observable<T> {
          let endpointUrl = serialId ? `${this._serialUrl}/${serialId}` : this._serialUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateSerialEndpoint(userObject, serialId));
            });
    }


  
    getSerialEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._serialUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getSerialEndpoint(userObject));
            });
    }


   



    getDeleteSerialEndpoint<T>(serialId: string): Observable<T> {
        let endpointUrl = `${this._serialUrl}/${serialId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteSerialEndpoint(serialId));
            });
    }





}