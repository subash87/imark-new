﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class BankEndpoint extends EndpointFactory {

    private readonly _bankUrl: string = "/api/bank";
    private readonly _bankPageUrl: string = "/api/bank/pagination";
    private readonly _bankPartialRentalPageUrl: string = "/api/bank/partialRentalBank";
    private readonly _bankRentalPageUrl: string = "/api/bank/rentalBank"; 
    private readonly _currentUserUrl: string = "/api/account/users/me";
   

    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }


    getBankEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._bankPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getBankEndpoint(page, pageSize));
            });
    }
    getPartialRentalBankEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._bankPartialRentalPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPartialRentalBankEndpoint(page, pageSize));
            });
    }
    getRentalBankEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._bankRentalPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getRentalBankEndpoint(page, pageSize));
            });
    }

    getBankById<T>(primaryName: string): Observable<T> {
        let endpointUrl = `${this._bankUrl}/${primaryName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getBankById(primaryName));
            });
    }
        
    getNewBankEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._bankUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewBankEndpoint(userObject));
            });
    }
    


    getUpdateBankEndpoint<T>(userObject: any, bankId?: string): Observable<T> {
        let endpointUrl = bankId ? `${this._bankUrl}/${bankId}` : this._bankUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateBankEndpoint(userObject, bankId));
            });
    }





    getDeleteBankEndpoint<T>(bankId: string): Observable<T> {
        let endpointUrl = `${this._bankUrl}/${bankId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteBankEndpoint(bankId));
            });
    }
}