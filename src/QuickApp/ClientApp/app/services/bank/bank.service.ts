﻿
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { BankEndpoint } from './bank-endpoint.service';
import { AuthService } from  '../auth.service';
import { Bank } from '../../models/bank/bank.model';
import { BankEdit } from '../../models/bank/bank-edit.model';

import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';


@Injectable()
export class BankService {

    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private BankEndpoint: BankEndpoint){

    }

    getBankInfo(page?: number, pageSize?: number) {
        return this.BankEndpoint.getBankEndpoint<Bank[]>(page, pageSize);
    }
    getPartialRentalBankInfo(page?: number, pageSize?: number) {
        return this.BankEndpoint.getPartialRentalBankEndpoint<Bank[]>(page, pageSize);
    }
    getRentalBankInfo(page?: number, pageSize?: number) {
        return this.BankEndpoint.getRentalBankEndpoint<Bank[]>(page, pageSize);
    }

    getBank(bankId?: string) {
        return this.BankEndpoint.getBankById<Bank>(bankId);
    }

    updateBank(bank: BankEdit) {
        if (bank.id) {
            return this.BankEndpoint.getUpdateBankEndpoint(bank, bank.id);
        }
        else {

            return this.BankEndpoint.getBankById<Bank>(bank.id)

                .mergeMap(foundUser => {
                    bank.id = foundUser.id;
                    return this.BankEndpoint.getUpdateBankEndpoint(bank, bank.id)
                });
        }
    }

    newBank(bank: BankEdit) {
        return this.BankEndpoint.getNewBankEndpoint<Bank>(bank);
    }

    deleteBank(bankOrBankId: string | BankEdit): Observable<Bank> {

        if (typeof bankOrBankId === 'string' || bankOrBankId instanceof String) {
            return this.BankEndpoint.getDeleteBankEndpoint<Bank>(<string>bankOrBankId);
            //.do(data => this.onInventoryCountChanged([data], AccountService.posDeletedOperation));
        }
        else {

            if (bankOrBankId.id) {
                return this.deleteBank(bankOrBankId.id);
            }
            else {
                return this.BankEndpoint.getBankById<Bank>(bankOrBankId.bankName)
                    .mergeMap(bank => this.deleteBank(bank.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}