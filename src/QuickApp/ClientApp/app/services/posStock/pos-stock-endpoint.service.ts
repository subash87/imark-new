﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class PosStockEndpoint extends EndpointFactory {

    private readonly _posStockUrl: string = "/api/posStock";
    private readonly _posStockPageUrl: string = "/api/posStock/pagination";
    private readonly _partialRentalUrl: string = "/api/posStock/reconfigurePartialRental";
    private readonly _currentUserUrl: string = "/api/account/users/me";
    get currentUserUrl() { return this.configurations.baseUrl + this._currentUserUrl; }



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }



    getPosStockByStockEndpoint<T>(posStock: string): Observable<T> {
        let endpointUrl = `${this._posStockUrl}/${posStock}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosStockByStockEndpoint(posStock));
            });
    }

    getPosStocksEndpoint<T>(posStockId?: string): Observable<T> {
        let endpointUrl = `${this._posStockUrl}/${posStockId}`;
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosStocksEndpoint(posStockId));
            });
    }
    getStockEndpoint<T>(page?: number, pageSize?: number, serialId?:string): Observable<T> {
        let endpointUrl = `${this._posStockPageUrl}/${-1}/${-1}/${serialId}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getStockEndpoint(page, pageSize, serialId));
            });
    }

    getUpdatePosStockEndpoint<T>(userObject: any, posStockId?: string): Observable<T> {
        let endpointUrl = posStockId ? `${this._posStockUrl}/${posStockId}` : this._posStockUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdatePosStockEndpoint(userObject, posStockId));
            });
    }



    getPosStockEndpoint<T>(userObject: any, inventoryId?: string, isSale?: boolean): Observable<T> {
        let endpointUrl = inventoryId ? `${this._posStockUrl}/${inventoryId}/${isSale}` : this._posStockUrl;

        return this.http.post<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getPosStockEndpoint(userObject, inventoryId, isSale));
            });

       
    }






    getDeletePosStockEndpoint<T>(posStockId: string): Observable<T> {
        let endpointUrl = `${this._posStockUrl}/${posStockId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeletePosStockEndpoint(posStockId));
            });
    }





}