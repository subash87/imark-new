﻿
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class HardwareSupportEndpoint extends EndpointFactory {

    private readonly _hardwareSupportUrl: string = "/api/hardwareSupport";
    private readonly _hardwareSupportAddUrl: string = "/api/hardwareSupportAdd";
    private readonly _hardwareSupportPageUrl: string = "/api/hardwareSupport/pagination";
    //private readonly _inventoryDelete: string = "/api/inventory/delete";
    private readonly _currentUserUrl: string = "/api/account/users/me";
    get currentUserUrl() { return this.configurations.baseUrl + this._currentUserUrl; }



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }

    getHardwareSupportEndpoint<T>(hardwareSupportId?: string): Observable<T> {
        let endpointUrl = hardwareSupportId ? `${this._hardwareSupportUrl}/${hardwareSupportId}` : this._hardwareSupportUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getHardwareSupportEndpoint(hardwareSupportId));
            });
    }



    getHardwareSupportByPrimaryItemEndpoint<T>(primaryName: string): Observable<T> {
        let endpointUrl = `${this._hardwareSupportUrl}/${primaryName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getHardwareSupportByPrimaryItemEndpoint(primaryName));
            });
    }

    getHardwareSupportsEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._hardwareSupportPageUrl}/${-1}/${-1}`;
        //let endpointUrl = page && pageSize ? `${this._inventoryPageUrl}/${page}/${pageSize}` : this._inventoryPageUrl;
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getHardwareSupportsEndpoint(page, pageSize));
            });
    }



    getNewHardwareSupportEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._hardwareSupportUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewHardwareSupportEndpoint(userObject));
            });
    }
    getAddHardwareSupportEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._hardwareSupportAddUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getAddHardwareSupportEndpoint(userObject));
            });
    }


    getUpdateHardwareSupportEndpoint<T>(userObject: any, hardwareSupportId?: string): Observable<T> {
        let endpointUrl = hardwareSupportId ? `${this._hardwareSupportUrl}/${hardwareSupportId}` : this._hardwareSupportUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateHardwareSupportEndpoint(userObject, hardwareSupportId));
            });
    }





    getDeleteHardwareSupportEndpoint<T>(hardwareSupportId: string): Observable<T> {
        let endpointUrl = `${this._hardwareSupportUrl}/${hardwareSupportId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteHardwareSupportEndpoint(hardwareSupportId));
            });
    }





}