﻿
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from './endpoint-factory.service';
import { ConfigurationService } from './configuration.service';


@Injectable()
export class InventoryEndpoint extends EndpointFactory {

    private readonly _inventoryUrl: string = "/api/inventory";
    private readonly _inventoryAddUrl: string = "/api/inventoryAdd";
    private readonly _inventoryPageUrl: string = "/api/inventory/pagination";
    //private readonly _inventoryDelete: string = "/api/inventory/delete";
    private readonly _currentUserUrl: string = "/api/account/users/me";
    get currentUserUrl() { return this.configurations.baseUrl + this._currentUserUrl; }



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }

    getInventoryEndpoint<T>(inventoryId?: string): Observable<T> {
        let endpointUrl = inventoryId ? `${this._inventoryUrl}/${inventoryId}` : this._inventoryUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getInventoryEndpoint(inventoryId));
            });
    }


   
    getInventoryByPrimaryItemEndpoint<T>(primaryName: string): Observable<T> {
        let endpointUrl = `${this._inventoryUrl}/${primaryName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getInventoryByPrimaryItemEndpoint(primaryName));
            });
    }

    getInventoriesEndpoint<T>(page?: number, pageSize?: number, hasSerial?: boolean): Observable<T> {
        let endpointUrl = `${this._inventoryPageUrl}/${-1}/${-1}/${hasSerial}`;
        //let endpointUrl = page && pageSize ? `${this._inventoryPageUrl}/${page}/${pageSize}` : this._inventoryPageUrl;
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getInventoriesEndpoint(page, pageSize, hasSerial));
            });
    }


  
    getNewInventoryEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._inventoryUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewInventoryEndpoint(userObject));
            });
    }
    getAddInventoryEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._inventoryAddUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getAddInventoryEndpoint(userObject));
            });
    }

  
    getUpdateInventoryEndpoint<T>(userObject: any, inventoryId?: string): Observable<T> {
        let endpointUrl = inventoryId ? `${this._inventoryUrl}/${inventoryId}` : this._inventoryUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateInventoryEndpoint(userObject, inventoryId));
            });
    }

  

   
   
    getDeleteInventoryEndpoint<T>(inventoryId: string): Observable<T> {
        let endpointUrl = `${this._inventoryUrl}/${inventoryId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteInventoryEndpoint(inventoryId));
            });
    }



   

}