﻿


import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { MaintenanceEndpoint } from './maintenance-endpoint.service';
import { AuthService } from '../auth.service';
import { Maintenance } from '../../models/maintenance/maintenance.model';
import { MaintenanceEdit } from '../../models/maintenance/maintenance-edit.model';
import { MaintenanceViewModel } from '../../models/maintenance/maintenanceViewModel.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';




export type MaintenanceChangedOperation = "add" | "delete" | "modify";
export type MaintenanceChangedEventArg = { maintenance: Maintenance[] | string[], operation: MaintenanceChangedOperation };



@Injectable()
export class MaintenanceService {

    public static readonly maintenanceAddedOperation: MaintenanceChangedOperation = "add";
    public static readonly maintenanceDeletedOperation: MaintenanceChangedOperation = "delete";
    public static readonly maintenanceModifiedOperation: MaintenanceChangedOperation = "modify";

    private _maintenanceChanged = new Subject<MaintenanceChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private MaintenanceEndpoint: MaintenanceEndpoint) {

    }


    getMaintenance(maintenanceId?: string) {
        return this.MaintenanceEndpoint.getMaintenanceEndpoint<Maintenance>(maintenanceId);
    }


    getMaintenanceInfo(page?: number, pageSize?: number) {
        return this.MaintenanceEndpoint.getMaintenancesEndpoint<Maintenance[]>(page, pageSize);
    }

    getMaintenanceIssueInfo(page?: number, pageSize?: number, id?:string) {
        return this.MaintenanceEndpoint.getMaintenanceIssuesEndpoint<MaintenanceViewModel[]>(page, pageSize, id);
    }

    getSerialListInfo(page?: number, pageSize?: number) {
        return this.MaintenanceEndpoint.getSerialListEndpoint<Maintenance[]>(page, pageSize);
    }

    updateMaintenance(maintenance: MaintenanceEdit) {
        if (maintenance.id) {
            return this.MaintenanceEndpoint.getUpdateMaintenanceEndpoint(maintenance, maintenance.id);
        }
        else {

            return this.MaintenanceEndpoint.getMaintenanceByTotalEndpoint<Maintenance>(maintenance.id)

                .mergeMap(foundUser => {
                    maintenance.id = foundUser.id;
                    return this.MaintenanceEndpoint.getUpdateMaintenanceEndpoint(maintenance, maintenance.id)
                });
        }
    }
    
    addMaintenanceFromNotReceived(maintenance: MaintenanceEdit, deploymentRequestId?: string) {
        return this.MaintenanceEndpoint.getAddNewMaintenacePosEndpoint<Maintenance>(maintenance, deploymentRequestId);
    }

    newMaintenance(maintenance: MaintenanceEdit, serialId?: string) {
        return this.MaintenanceEndpoint.getNewMaintenanceEndpoint<Maintenance>(maintenance, serialId);
    }
    newMaintenanceFromDeployment(maintenance: MaintenanceEdit, serialId?: string) {
        return this.MaintenanceEndpoint.getNewMaintenanceFromDeploymentEndpoint<Maintenance>(maintenance, serialId);
    }




    deleteMaintenance(maintenanceOrMaintenanceId: string | MaintenanceEdit): Observable<Maintenance> {

        if (typeof maintenanceOrMaintenanceId === 'string' || maintenanceOrMaintenanceId instanceof String) {
            return this.MaintenanceEndpoint.getDeleteMaintenanceEndpoint<Maintenance>(<string>maintenanceOrMaintenanceId);
            //.do(data => this.onInventoryAddCountChanged(data.inventoryId));
        }
        else {

            if (maintenanceOrMaintenanceId.id) {
                return this.deleteMaintenance(maintenanceOrMaintenanceId.id);
            }
            else {
                return this.MaintenanceEndpoint.getMaintenanceByTotalEndpoint<Maintenance>(maintenanceOrMaintenanceId.id)
                    .mergeMap(maintenance => this.deleteMaintenance(maintenance.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}