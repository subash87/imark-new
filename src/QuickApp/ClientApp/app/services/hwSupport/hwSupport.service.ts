﻿
import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { HwSupportEndpoint } from './hwSupport-endpoint.service';
import { AuthService } from '../auth.service';
import { HwSupport } from '../../models/hwSupport/hwSupport.model';
import { SupportEdit } from '../../models/support/support-edit.model';
import { HwSupportEdit } from '../../models/hwSupport/hwSupport-edit.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';




export type SupportChangedOperation = "add" | "delete" | "modify";
export type SupportChangedEventArg = { support: HwSupport[] | string[], operation: SupportChangedOperation };



@Injectable()
export class HwSupportService {

    public static readonly supportAddedOperation: SupportChangedOperation = "add";
    public static readonly supportDeletedOperation: SupportChangedOperation = "delete";
    public static readonly supportModifiedOperation: SupportChangedOperation = "modify";

    private _supportChanged = new Subject<SupportChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private HwSupportEndpoint: HwSupportEndpoint) {

    }


    getSupport(supportId?: string) {
        return this.HwSupportEndpoint.getSupportEndpoint<HwSupport>(supportId);
    }


    getSupportInfo(page?: number, pageSize?: number, id?:string) {
        return this.HwSupportEndpoint.getSupportsEndpoint<HwSupport[]>(page, pageSize, id);
    }


    updateSupport(support: HwSupportEdit) {
        if (support.id) {
            return this.HwSupportEndpoint.getUpdateSupportEndpoint(support, support.id);
        }
        else {

            return this.HwSupportEndpoint.getSupportByTotalEndpoint<HwSupport>(support.id)

                .mergeMap(foundUser => {
                    support.id = foundUser.id;
                    return this.HwSupportEndpoint.getUpdateSupportEndpoint(support, support.id)
                });
        }
    }

    //-----Save Data from HwSupport PopUp
    newSupport(support: HwSupportEdit) {
        return this.HwSupportEndpoint.getNewSupportEndpoint<HwSupport>(support);
    }




    deleteSupport(supportOrSupportId: string | HwSupportEdit): Observable<HwSupport> {

        if (typeof supportOrSupportId === 'string' || supportOrSupportId instanceof String) {
            return this.HwSupportEndpoint.getDeleteSupportEndpoint<HwSupport>(<string>supportOrSupportId);
        }
        else {

            if (supportOrSupportId.id) {
                return this.deleteSupport(supportOrSupportId.id);
            }
            else {
                return this.HwSupportEndpoint.getSupportByTotalEndpoint<HwSupport>(supportOrSupportId.id)
                    .mergeMap(support => this.deleteSupport(support.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}