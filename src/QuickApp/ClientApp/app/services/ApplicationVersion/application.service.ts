import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { ApplicationEndpoint } from './application-endpoint.service';
import { AuthService } from '../auth.service';
import { Application } from '../../models/applicationVersion/application.model';
import { ApplicationEdit } from '../../models/applicationVersion/application-edit.model';
import { Permission, PermissionNames, PermissionValues } from '../../models/permission.model';




export type ApplicationChangedOperation = "add" | "delete" | "modify";
export type ApplicationChangedEventArg = { application: Application[] | string[], operation: ApplicationChangedOperation };



@Injectable()
export class ApplicationService {

    public static readonly applicationAddedOperation: ApplicationChangedOperation = "add";
    public static readonly applicationDeletedOperation: ApplicationChangedOperation = "delete";
    public static readonly applicationModifiedOperation: ApplicationChangedOperation = "modify";

    private _applicationChanged = new Subject<ApplicationChangedEventArg>();


    constructor(private router: Router, private http: HttpClient, private authService: AuthService,
        private ApplicationEndpoint: ApplicationEndpoint) {

    }


    getApplication(applicationId?: string) {
        return this.ApplicationEndpoint.getApplicationEndpoint<Application>(applicationId);
    }

    //getUserAndRoles(userId?: string) {

    //    return Observable.forkJoin(
    //        this.accountEndpoint.getUserEndpoint<User>(userId),
    //        this.accountEndpoint.getRolesEndpoint<Role[]>());
    //}

    getApplicationInfo(page?: number, pageSize?: number) {
        return this.ApplicationEndpoint.getApplicationsEndpoint<Application[]>(page, pageSize);
    }
    getApplicationWithBankInfo(id?:string, page?: number, pageSize?: number) {
        return this.ApplicationEndpoint.getApplicationsWithBankEndpoint<Application[]>(id, page, pageSize);
    }

    //getUsersAndRoles(page?: number, pageSize?: number) {

    //    return Observable.forkJoin(
    //        this.accountEndpoint.getUsersEndpoint<User[]>(page, pageSize),
    //        this.accountEndpoint.getRolesEndpoint<Role[]>());
    //}


    updateApplication(application: ApplicationEdit) {
        if (application.id) {
            return this.ApplicationEndpoint.getUpdateApplicationEndpoint(application, application.id);
        }
        else {

            return this.ApplicationEndpoint.getApplicationByBankIdEndpoint<Application>(application.id)

                .mergeMap(foundUser => {
                    application.id = foundUser.id;
                    return this.ApplicationEndpoint.getUpdateApplicationEndpoint(application, application.id)
                });
        }
    }


    newApplication(application: ApplicationEdit) {
        return this.ApplicationEndpoint.getNewApplicationEndpoint<Application>(application);
    }



    deleteApplication(applicationOrApplicationId: string | ApplicationEdit): Observable<Application> {

        if (typeof applicationOrApplicationId === 'string' || applicationOrApplicationId instanceof String) {
            return this.ApplicationEndpoint.getDeleteApplicationEndpoint<Application>(<string>applicationOrApplicationId);
            //.do(data => this.onApplicationCountChanged([data], AccountService.posDeletedOperation));
        }
        else {

            if (applicationOrApplicationId.id) {
                return this.deleteApplication(applicationOrApplicationId.id);
            }
            else {
                return this.ApplicationEndpoint.getApplicationByBankIdEndpoint<Application>(applicationOrApplicationId.kernelType)
                    .mergeMap(application => this.deleteApplication(application.id));
            }
        }
    }



    userHasPermission(permissionValue: PermissionValues): boolean {
        return this.permissions.some(p => p == permissionValue);
    }



    get permissions(): PermissionValues[] {
        return this.authService.userPermissions;
    }

    get currentUser() {
        return this.authService.currentUser;
    }
}