

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class ApplicationEndpoint extends EndpointFactory {

    private readonly _applicationUrl: string = "/api/applicationVersion";
    private readonly _applicationAddUrl: string = "/api/applicationAdd";
    private readonly _applicationPageUrl: string = "/api/applicationVersion/pagination";
    private readonly _applicationPageWithBankUrl: string = "/api/applicationVersion";
    private readonly _currentUserUrl: string = "/api/account/users/me";
    get currentUserUrl() { return this.configurations.baseUrl + this._currentUserUrl; }



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }

    getApplicationEndpoint<T>(applicationId?: string): Observable<T> {
        let endpointUrl = applicationId ? `${this._applicationUrl}/${applicationId}` : this._applicationUrl;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getApplicationEndpoint(applicationId));
            });
    }



    getApplicationByBankIdEndpoint<T>(primaryName: string): Observable<T> {
        let endpointUrl = `${this._applicationUrl}/${primaryName}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getApplicationByBankIdEndpoint(primaryName));
            });
    }

    getApplicationsEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._applicationPageUrl}/${-1}/${-1}`;
        //let endpointUrl = page && pageSize ? `${this._applicationPageUrl}/${page}/${pageSize}` : this._applicationPageUrl;
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getApplicationsEndpoint(page, pageSize));
            });
    }
    getApplicationsWithBankEndpoint<T>(id?:string, page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._applicationPageWithBankUrl}/${id}/${-1}/${-1}`;
       
        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getApplicationsWithBankEndpoint(id, page, pageSize));
            });
    }



    getNewApplicationEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._applicationUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getNewApplicationEndpoint(userObject));
            });
    }
    getAddApplicationEndpoint<T>(userObject: any): Observable<T> {

        return this.http.post<T>(this._applicationAddUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getAddApplicationEndpoint(userObject));
            });
    }


    getUpdateApplicationEndpoint<T>(userObject: any, applicationId?: string): Observable<T> {
        let endpointUrl = applicationId ? `${this._applicationUrl}/${applicationId}` : this._applicationUrl;

        return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getUpdateApplicationEndpoint(userObject, applicationId));
            });
    }





    getDeleteApplicationEndpoint<T>(applicationId: string): Observable<T> {
        let endpointUrl = `${this._applicationUrl}/${applicationId}`;

        return this.http.delete<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getDeleteApplicationEndpoint(applicationId));
            });
    }





}