﻿

import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { EndpointFactory } from '../endpoint-factory.service';
import { ConfigurationService } from '../configuration.service';


@Injectable()
export class CheckerEndpoint extends EndpointFactory {

    private readonly _checkerUrl: string = "/api/checker";
    private readonly _checkerPageUrl: string = "/api/checker/pagination";

    private readonly _currentUserUrl: string = "/api/account/users/me";



    constructor(http: HttpClient, configurations: ConfigurationService, injector: Injector) {

        super(http, configurations, injector);
    }


    getCheckersEndpoint<T>(page?: number, pageSize?: number): Observable<T> {
        let endpointUrl = `${this._checkerPageUrl}/${-1}/${-1}`;

        return this.http.get<T>(endpointUrl, this.getRequestHeaders())
            .catch(error => {
                return this.handleError(error, () => this.getCheckersEndpoint(page, pageSize));
            });
    }
}