﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { ConfigurationsService } from "../../services/configuration/configuration.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { DamagePos } from '../../models/configuration/configuration.model';
import { DamagePosEdit } from '../../models/configuration/configuration-edit.model';

import { DeployEditComponent } from '../deploy/deploy-edit.component';
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import { ConfigurationService } from '../../services/configuration.service';

import * as moment from 'moment';
@Component({
    selector: 'damage-pos-list',
    templateUrl: './damage-pos.component.html',
    styleUrls: ['./damage-pos.component.css']
})
export class DamagePosListComponent implements OnInit {
    columns: any[] = [];
    columnDefs: any[] = [];
    private gridApi;
    private gridColumnApi;
    private gridOptions;
    rows: DamagePos[] = [];
    rowsCache: DamagePos[] = [];

    editedPos: DamagePosEdit;
    sourcePos: DamagePosEdit;
    loadingIndicator: boolean;


    allPos: DamagePos[] = [];
    @Input()
    isGeneralEditor = false;

    @ViewChild('deployEditor')
    deployEditor: DeployEditComponent;


    constructor(private alertService: AlertService, private router: Router, private translationService: AppTranslationService,
        private damagePosService: ConfigurationsService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);
        this.columnDefs = [
            { headerName: 'Bank Name', field: 'bankName', width: 180, suppressSizeToFit: true },
            { headerName: 'Merchant Name', field: 'merchant', width: 200, suppressSizeToFit: true },
            { headerName: 'Merchant ID', field: 'idMerchant', width: 130, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 130, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: 'item', width: 250, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true },
            { headerName: 'Address', field: 'address', width: 130, suppressSizeToFit: true },
            { headerName: 'Contact Person', field: 'contactPerson', width: 130, suppressSizeToFit: true },
            { headerName: 'Contact No', field: 'contactNo1', width: 130, suppressSizeToFit: true },
            { headerName: 'Created By', field: 'createdBy', width: 180, suppressSizeToFit: true },
            {
                headerName: 'Created Date', field: 'createdDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 150, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },



        ];



        this.loadData();
        //this.addNewDeploymentToList();

    }
    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }
    onGridReady(params) {
        this.gridApi = params.api;

        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.damagePosService.damagePosPage().subscribe(data => {
            params.api.setRowData(data);
        });

    }





    loadData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.damagePosService.damagePosPage().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(damages: DamagePos[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        damages.forEach((damage, index, damages) => {
            (<any>damage).index = index + 1;
        });
        this.rowsCache = [...damages];
        this.rows = damages;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.bankName, r.merchant, r.idMerchant, r.outlet, r.idTerminal, r.address, r.contactPerson, r.item, r.serialKey, r.createdBy));
    }


    //onDeployModalHidden() {
    //    this.editingDeployName = null;
    //    this.deployEditor.resetForm(true);
    //}r.


    //loadDeploy(id) {
    //    this.alertService.startLoadingMessage();
    //    this.loadingIndicator = true;
    //    this.deployService.getDeploy(id).subscribe(results => this.onDeployLoadSuccessful(results), error => this.onDeployLoadFailed(error));
    //}
    //onDeployLoadSuccessful(deploy: Deploy) {
    //    this.alertService.stopLoadingMessage();
    //    this.loadingIndicator = false;
    //    this.editDeploy(deploy);
    //}
    //onDeployLoadFailed(error: any) {
    //    this.alertService.stopLoadingMessage();
    //    this.loadingIndicator = false;

    //    this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
    //        MessageSeverity.error, error);
    //}
    //editDeploy(deploy: DeployEdit) {
    //    this.editingDeployName = { name: deploy.receivedBy };
    //    this.sourceDeploy = deploy;
    //    this.editedDeploy = this.deployEditor.editDeploy(deploy, this.allDeploy);
    //    this.deployModal.show();
    //}





    get canViewRoles() {
        return this.damagePosService.userHasPermission(Permission.viewRolesPermission)

    }

    get canManageUsers() {
        return this.damagePosService.userHasPermission(Permission.manageUsersPermission);
    }
    get canSeeSerial() {
        return this.damagePosService.userHasPermission(Permission.manageUsersPermission);
    }

}
