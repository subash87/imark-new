﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { ConfigurationsService } from "../../services/configuration/configuration.service";
import { PosHistoryService } from "../../services/posHistory/pos-history.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { NotReceivedPos } from '../../models/configuration/configuration.model';
import { NotReceivedPosEdit, ConfigurationEdit } from '../../models/configuration/configuration-edit.model';

import { DeployEditComponent } from '../deploy/deploy-edit.component';
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import { ConfigurationService } from '../../services/configuration.service';
import { AddStatusNotReceivedPosComponent } from "./not-received-add-status.component";
import { AddStatusMaintenancePosComponent } from "./maintenance-add-status.component";
import * as moment from 'moment';
import { PosHistoryEdit } from '../../models/posHistory/pos-edit.model';
import { PosHistory } from '../../models/posHistory/pos-history.model';
import { MaintenanceEdit } from '../../models/maintenance/maintenance-edit.model';
@Component({
    selector: 'not-received-pos-list',
    templateUrl: './not-received-pos.component.html',
    styleUrls: ['./not-received-pos.component.css']
})
export class NotReceivedPosListComponent implements OnInit {
    columns: any[] = [];
    columnDefs: any[] = [];
    private gridApi;
    private gridColumnApi;
    private gridOptions;
    rows: NotReceivedPos[] = [];
    rowsCache: NotReceivedPos[] = [];

    editedPos: NotReceivedPosEdit;
    sourcePos: NotReceivedPosEdit;
    editedStatus: NotReceivedPosEdit;
    editedMaintenance: MaintenanceEdit;
    private maintenanceEdit: MaintenanceEdit = new MaintenanceEdit();
    private notReceived: NotReceivedPosEdit = new NotReceivedPosEdit();
    loadingIndicator: boolean;
    @ViewChild('statusModal')
    statusModal: ModalDirective;

    @ViewChild('maintenanceStatusModal')
    maintenanceStatusModal: ModalDirective;

    allPos: NotReceivedPos[] = [];
    @Input()
    isGeneralEditor = false;

    @ViewChild('statusEditor')
    statusEditor: AddStatusNotReceivedPosComponent;


    @ViewChild('maintenanceStatusEditor')
    maintenanceStatusEditor: AddStatusMaintenancePosComponent;

    constructor(private alertService: AlertService, private router: Router, private translationService: AppTranslationService,
        private notReceivedService: ConfigurationsService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);
        this.columnDefs = [
            { headerName: 'Bank Name', field: 'bankName', width: 180, suppressSizeToFit: true },
            { headerName: 'Merchant Name', field: 'merchant', width: 200, suppressSizeToFit: true },
            { headerName: 'Merchant ID', field: 'idMerchant', width: 130, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 130, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: 'item', width: 250, suppressSizeToFit: true },
            { headerName: 'Application Version', field: 'applicationFormat', width: 180, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true },
            { headerName: 'Status', field: 'status', width: 180, suppressSizeToFit: true },
            //{ headerName: 'Remarks', field: 'configStatus', width: 230, suppressSizeToFit: true },
            { headerName: 'District', field: 'district', width: 130, suppressSizeToFit: true },
            { headerName: 'Address', field: 'address', width: 130, suppressSizeToFit: true },
            { headerName: 'Deployed Location', field: 'deployedLocation', width: 180, suppressSizeToFit: true },
            { headerName: 'Contact Person', field: 'contactPerson', width: 180, suppressSizeToFit: true},
            { headerName: 'Contact No', field: 'contactNo', width: 130, suppressSizeToFit: true },
            { headerName: 'Received By', field: 'receivedBy', width: 180, suppressSizeToFit: true },
            { headerName: 'Received Contact No', field: 'receivedContactNo', width: 180, suppressSizeToFit: true },
            { headerName: 'Created By', field: 'createdBy', width: 180, suppressSizeToFit: true },
            {
                headerName: 'Created Date', field: 'createdDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 150, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },
            {
                headerName: "Actions",
                suppressMenu: true,
                suppressSorting: true,
                width: 250,
                template:
                    `
              </button>
                <button type="button" data-action-type="Stock" class="btn btn-link btn-xs" style="background-color:maroon;color:white;text-decoration:none">
               Send to Stock
             </button>
               <button type="button" data-action-type="Maintenance" class="btn btn-link btn-xs"style="background-color:maroon;color:white;text-decoration:none">
               Send to Maintenance
             </button>`
            }

        ];


        this.addNewNotReceivedToList();

        //this.addNewDeploymentToList();

    }

    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;

            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {

                case "Stock":
                    return this.onStock(data);
                case "Maintenance":
                    return this.onMaintenance(data);

            }
        }
    }

    public onStock(row: NotReceivedPosEdit) {

        this.addStock(row);

    }
    public onMaintenance(row: NotReceivedPosEdit) {
        this.newMaintenance(row);

    }
    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }
    onGridReady(params) {
        this.gridApi = params.api;

        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.notReceivedService.notReceivedSerialPage().subscribe(data => {
            params.api.setRowData(data);
        });

    }



    ngAfterViewInit() {



        this.statusEditor.changesSavedCallback = () => {
            this.addNewNotReceivedToList();
            this.statusModal.hide();
            this.loadData();
        }
        this.statusEditor.changesCancelledCallback = () => {
            this.editedPos = null;
            this.sourcePos = null;
            this.statusModal.hide();
        }
        this.maintenanceStatusEditor.changesSavedCallback = () => {
            this.addNewNotReceivedToList();
            this.maintenanceStatusModal.hide();
            this.loadData();
        }
        this.maintenanceStatusEditor.changesCancelledCallback = () => {
            //this.editedMaintenance = null;
            this.maintenanceStatusModal.hide();
        }

    }


    addNewNotReceivedToList() {
        if (this.sourcePos) {
            Object.assign(this.sourcePos, this.editedPos);

            let sourceIndex = this.rowsCache.indexOf(this.sourcePos, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourcePos, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedPos = null;
            this.sourcePos = null;
        }
        else {
            let notReceived = new NotReceivedPos();
            Object.assign(notReceived, this.editedPos);
            this.editedPos = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>NotReceivedPos).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, notReceived);
            this.rows.splice(0, 0, notReceived);
        }
        this.loadData();
    }


    loadData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.notReceivedService.notReceivedSerialPage().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(notReceived: NotReceivedPos[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        notReceived.forEach((notreceive, index, notReceived) => {
            (<any>notreceive).index = index + 1;
        });
        this.rowsCache = [...notReceived];
        this.rows = notReceived;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.bankName, r.merchant, r.idMerchant, r.outlet, r.idTerminal, r.configStatus, r.address, r.district, r.contactPerson, r.item, r.serialKey));
    }





    addStock(row: NotReceivedPos) {
        this.alertService.showDialog('Are you sure you want to Send to stock ?', DialogType.confirm, () => this.addStockHelper(row));
    }


    addStockHelper(row: NotReceivedPos) {

        this.editedPos = this.statusEditor.addPosHistoryStatus(row, this.allPos);
        this.statusModal.show();
        this.addNewNotReceivedToList();
        this.loadData();
        //this.maintenanceService.deleteMaintenance(row)
        //    .subscribe(results => {
        //        this.alertService.stopLoadingMessage();
        //        this.loadingIndicator = false;

        //        this.rowsCache = this.rowsCache.filter(item => item !== row)
        //        this.rows = this.rows.filter(item => item !== row)
        //    },
        //        error => {
        //            this.alertService.stopLoadingMessage();
        //            this.loadingIndicator = false;

        //            this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
        //                MessageSeverity.error, error);
        //        });
    }

    onStatusModalHidden() {
        this.statusEditor.resetForm(true);
    } 

    newMaintenance(row: NotReceivedPos) {

        this.alertService.showDialog('Are you sure you want to send to Maintenance ?', DialogType.confirm, () => this.retrieveHelper2(row));
    }

    //}
    retrieveHelper2(row: NotReceivedPos) {

        this.maintenanceEdit.deploymentRequestId = row.deploymentRequestId;
        this.maintenanceEdit.applicationVersionId = row.applicationVersionId;
        this.maintenanceEdit.inventoryId = row.inventoryId;
        this.maintenanceEdit.inventoryAddId = row.inventoryAddId;
        this.maintenanceEdit.serialNumberId = row.serialNumberId;
        this.maintenanceEdit.address = row.address;
        this.maintenanceEdit.contactNo = row.contactNo;
        this.maintenanceEdit.contactPerson = row.contactPerson;
        this.maintenanceEdit.district = row.district;
        this.maintenanceEdit.idMerchant = row.idMerchant;
        this.maintenanceEdit.merchant = row.merchant;
        this.maintenanceEdit.idTerminal = row.idTerminal;
        this.maintenanceEdit.outlet = row.outlet;
        this.editedMaintenance = this.maintenanceStatusEditor.addMaintenanceStatus(this.maintenanceEdit, this.maintenanceEdit.deploymentRequestId);
        this.maintenanceStatusModal.show();
        this.addNewNotReceivedToList();
        this.loadData();
        //this.maintenanceEdit.inventoryId = row;
        //this.posHistoryService.newMaintenance(this.maintenanceEdit, this.maintenanceEdit.deploymentRequestId)
        //    .subscribe(results => {
        //        this.alertService.stopLoadingMessage();
        //        this.loadingIndicator = false;
        //        this.alertService.showMessage("Success", `Serial Number+""+ has been sent to maintenance`, MessageSeverity.success);
        //        this.rowsCache = this.rowsCache.filter(item => item !== row)
        //        this.rows = this.rows.filter(item => item !== row)
        //    },
        //        error => {
        //            this.alertService.stopLoadingMessage();
        //            this.loadingIndicator = false;

        //            this.alertService.showStickyMessage("Request Error", `An error occured whilst re-configuring the parameter.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
        //                MessageSeverity.error, error);
        //        });
    }
    onMaintenanceStatusModalHidden() {
        this.maintenanceStatusEditor.resetForm(true);
    } 
    get canViewRoles() {
        return this.notReceivedService.userHasPermission(Permission.viewRolesPermission)

    }

    get canManageUsers() {
        return this.notReceivedService.userHasPermission(Permission.manageUsersPermission);
    }
    get canSeeSerial() {
        return this.notReceivedService.userHasPermission(Permission.manageUsersPermission);
    }

}
