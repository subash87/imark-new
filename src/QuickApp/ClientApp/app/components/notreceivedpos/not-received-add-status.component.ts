﻿import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { PosHistoryService } from "../../services/posHistory/pos-history.service";
import { Utilities } from '../../services/utilities';
import { PosHistory } from '../../models/posHistory/pos-history.model';
import { PosHistoryEdit } from '../../models/posHistory/pos-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { NotReceivedPos } from '../../models/configuration/configuration.model';
import { NotReceivedPosEdit } from '../../models/configuration/configuration-edit.model';
import { ConfigurationsService } from '../../services/configuration/configuration.service';



@Component({
    selector: 'not-received-add-status',
    templateUrl: './not-received-add-status.component.html',
    styleUrls: ['./not-received-pos.component.css']
})
export class AddStatusNotReceivedPosComponent implements OnInit {

    private isEditMode = false;
    private isNewReceived = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingDeployName: string;
    private uniqueId: string = Utilities.uniqueId();
    private notReceived: NotReceivedPos = new NotReceivedPos();
    private allNotReceived: NotReceivedPos[] = [];
    private notReceivedEdit: NotReceivedPosEdit = new NotReceivedPosEdit();
   

    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;
    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;




    @ViewChild('status')
    private status;



    constructor(private alertService: AlertService, private configService: ConfigurationsService) {
    }

    ngOnInit() {


    }





    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.notReceivedEdit = new NotReceivedPosEdit();
            Object.assign(this.notReceivedEdit, this.notReceived);
        }
        else {
            if (!this.notReceivedEdit)
                this.notReceivedEdit = new NotReceivedPosEdit();
            this.notReceivedEdit.status = null;
            this.isEditingSelf = this.configService.updateNotReceived(this.notReceived) ? this.notReceivedEdit.id == this.notReceived.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }
    private saveStatus() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        this.configService.updateNotReceived(this.notReceivedEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));

    }


    private saveSuccessHelper(notReceived?: NotReceivedPos) {

        Object.assign(this.notReceivedEdit, notReceived);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        //this.showValidationErrors = false;
        Object.assign(this.notReceived, this.notReceivedEdit);
        this.notReceivedEdit = new NotReceivedPosEdit();
        this.resetForm();



        this.alertService.showMessage("Success", `Changes to user \"${this.notReceived.status}\" was saved successfully`, MessageSeverity.success);

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }

    



    private cancel() {
        if (this.isGeneralEditor)
            this.notReceivedEdit = this.notReceived = new NotReceivedPosEdit();
        else
            this.notReceivedEdit = new NotReceivedPosEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.notReceivedEdit = this.notReceived = new NotReceivedPosEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    //newDamage(allDeploy: Deploy[]) {
    //    this.isGeneralEditor = true;
    //    this.isNewDeploy = true;

    //    this.allDeploy = [...allDeploy];
    //    this.editingDeployName = null;
    //    this.deploy = this.deployEdit = new DeployEdit();
    //    //this.posEdit.isEnabled = true;
    //    this.edit();

    //    return this.deployEdit;
    //}

    addPosHistoryStatus(notReceived: NotReceivedPos, allPos: NotReceivedPos[]) {
        //if (deploy) {
        this.isGeneralEditor = true;
        //this.isNewDeploy = false;


        this.notReceived = new NotReceivedPos();
        this.notReceivedEdit = new NotReceivedPosEdit();
        Object.assign(this.notReceived, notReceived);
        Object.assign(this.notReceivedEdit, notReceived);

        this.edit();

        return this.notReceivedEdit;
        //}
        //else {
        //    return this.newDeploy(allDeploy);
        //}
    }





    get canViewAllRoles() {
        return this.configService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.configService.userHasPermission(Permission.assignRolesPermission);
    }
}
