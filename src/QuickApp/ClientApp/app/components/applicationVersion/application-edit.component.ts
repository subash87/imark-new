﻿

import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { ApplicationService } from "../../services/applicationVersion/application.service";
import { Utilities } from '../../services/utilities';
import { Application } from '../../models/applicationVersion/application.model';
import { ApplicationEdit } from '../../models/applicationVersion/application-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { BankService } from "../../services/bank/bank.service";
import { Bank } from '../../models/bank/bank.model';

@Component({
    selector: 'application-edit',
    templateUrl: './application-edit.component.html',
    styleUrls: ['./application.component.css']
})
export class ApplicationEditComponent implements OnInit {
    maxDate = new Date();
    rows: Bank[] = [];
    private isEditMode = false;
    private isBankCode = false;
    private isNewApplication = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingApplicationName: string;
    private uniqueId: string = Utilities.uniqueId();
    private application: Application = new Application();
    private allApplication: Application[] = [];
    private applicationEdit: ApplicationEdit;
    private bank: Bank = new Bank();

    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('bankId')
    private bankId;

    @ViewChild('applicationDate')
    private applicationDate;

    @ViewChild('applicationVer')
    private applicationVer;

    @ViewChild('kernelType')
    private kernelType;

    @ViewChild('remarks')
    private remarks;

    constructor(private alertService: AlertService, private applicationService: ApplicationService, private bankService: BankService) {
    }

    ngOnInit() {
        if (!this.isGeneralEditor) {
            this.loadCurrentApplicationData();
        }
        this.loadCurrentBankData();
    }



    private loadCurrentApplicationData() {
        this.alertService.startLoadingMessage();

        if (this.canViewAllRoles) {
            this.applicationService.getApplication().subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
        }

    }


    private onCurrentUserDataLoadSuccessful(application: Application) {
        this.alertService.stopLoadingMessage();
        this.application = application;

    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.application = new Application();
    }


    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }



    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.applicationEdit = new ApplicationEdit();
            Object.assign(this.applicationEdit, this.application);
        }
        else {
            if (!this.applicationEdit)
                this.applicationEdit = new ApplicationEdit();

            this.isEditingSelf = this.applicationService.updateApplication(this.application) ? this.applicationEdit.id == this.application.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }
    private loadCurrentBankData() {
        this.bankService.getRentalBankInfo().subscribe(results => this.onBankDataLoadSuccessful(results), error => this.onBankDataLoadFailed(error));
    }


    private onBankDataLoadSuccessful(banks: Bank[]) {
        banks.forEach((bank, index, banks) => {
            if (bank.bankCode != null) {
                this.isBankCode = true;
                
            }
            else {
                bank.bankCode="------"
             
            }

        });
        this.rows = banks;
    }

    private onBankDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.bank = new Bank();
    }

    private saveApplication() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        if (this.isNewApplication) {
            this.applicationService.newApplication(this.applicationEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            this.applicationService.updateApplication(this.applicationEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(application?: Application) {
        this.testIsApplicationChanged(this.application, this.applicationEdit);

        if (application)
            Object.assign(this.applicationEdit, application);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.application, this.applicationEdit);
        this.applicationEdit = new ApplicationEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewApplication)
                this.alertService.showMessage("Success", `User \"${this.application.bankCode}\" was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Changes to user \"${this.application.bankCode}\" was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private testIsApplicationChanged(application: Application, editedApplication: Application) {

        //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
        //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);

        //let modifiedRoles = rolesAdded.concat(rolesRemoved);

        //if (modifiedRoles.length)
        //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    }



    private cancel() {
        if (this.isGeneralEditor)
            this.applicationEdit = this.application = new ApplicationEdit();
        else
            this.applicationEdit = new ApplicationEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.applicationEdit = this.application = new ApplicationEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newApplication(allApplication: Application[]) {
        this.isGeneralEditor = true;
        this.isNewApplication = true;

        this.allApplication = [...allApplication];
        this.editingApplicationName = null;
        this.application = this.applicationEdit = new ApplicationEdit();
        //this.posEdit.isEnabled = true;
        this.edit();

        return this.applicationEdit;
    }

    editApplication(application: Application, allApplication: Application[]) {
        if (application) {
            this.isGeneralEditor = true;
            this.isNewApplication = false;
            this.editingApplicationName = application.bankId;
            this.application = new Application();
            this.applicationEdit = new ApplicationEdit();
            Object.assign(this.application, application);
            Object.assign(this.applicationEdit, application);
            this.edit();

            return this.applicationEdit;
        }
        else {
            return this.newApplication(allApplication);
        }
    }





    get canViewAllRoles() {
        return this.applicationService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.applicationService.userHasPermission(Permission.assignRolesPermission);
    }
}
