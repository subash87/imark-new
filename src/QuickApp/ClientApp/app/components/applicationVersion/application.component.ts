﻿/// <reference path="../../services/ApplicationVersion/application.service.ts" />

import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { ApplicationService } from "../../services/applicationVersion/application.service";
import { Utilities } from "../../services/utilities";
import { Permission } from '../../models/permission.model';
import { Application } from '../../models/applicationVersion/application.model';
import { ApplicationEdit } from '../../models/applicationVersion/application-edit.model';
import { ApplicationEditComponent } from "./application-edit.component";

@Component({
    selector: 'application-management',
    templateUrl: './application.component.html',
    styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit, AfterViewInit {
    columns: any[] = [];
    rows: Application[] = [];
    rowsCache: Application[] = [];
    editedApplication: ApplicationEdit;
    sourceApplication: ApplicationEdit;
    editingApplicationName: {};
    loadingIndicator: boolean;

    allApplication: Application[] = [];

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('bankIdTemplate')
    bankIdTemplate: TemplateRef<any>;

    @ViewChild('applicationDateTemplate')
    applicationDateTemplate: TemplateRef<any>;

    @ViewChild('applicationVersionTemplate')
    applicationVersionTemplate: TemplateRef<any>;

    @ViewChild('kernelTypeTemplate')
    kernelTypeTemplate: TemplateRef<any>;

    @ViewChild('remarksTemplate')
    remarksTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('applicationEditor')
    applicationEditor: ApplicationEditComponent;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private applicationService: ApplicationService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        this.columns = [
            { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'bankCode', name: "Bank Code", width: 50, cellTemplate: this.bankIdTemplate },
            { prop: 'applicationDate', name: gT('application.management.ApplicationDate'), width: 90, cellTemplate: this.applicationDateTemplate },
            { prop: 'kernelType', name: "O.S/Kernel Verison", width: 120, cellTemplate: this.kernelTypeTemplate },
            { prop: 'applicationVer', name: "Application Version", width: 140, cellTemplate: this.applicationVersionTemplate },
            { prop: 'remarks', name: "Remarks", width: 140, cellTemplate: this.remarksTemplate },
        ];

        if (this.canManageUsers)
            this.columns.push({ name: '', width: 130, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });



        this.addNewApplicationToList();
    }


    ngAfterViewInit() {

        this.applicationEditor.changesSavedCallback = () => {
            this.addNewApplicationToList();
            this.editorModal.hide();
        };

        this.applicationEditor.changesCancelledCallback = () => {
            this.editedApplication = null;
            this.sourceApplication = null;
            this.editorModal.hide();
        };

    }


    addNewApplicationToList() {
        if (this.sourceApplication) {
            Object.assign(this.sourceApplication, this.editedApplication);

            let sourceIndex = this.rowsCache.indexOf(this.sourceApplication, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceApplication, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedApplication = null;
            this.sourceApplication = null;
        }
        else {
            let application = new Application();
            Object.assign(application, this.editedApplication);
            this.editedApplication = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>Application).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, application);
            this.rows.splice(0, 0, application);
        }
        this.loadApplicationData();
    }


    loadApplicationData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.applicationService.getApplicationInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(applications: Application[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        applications.forEach((application, index, applications) => {
            (<any>application).index = index + 1;
        });
        this.rowsCache = [...applications];
        this.rows = applications;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.bankCode, r.applicationDate, r.applicationVer, r.bankCode, r.kernelType, r.remarks));
    }

    onEditorModalHidden() {
        this.editingApplicationName = null;
        this.applicationEditor.resetForm(true);
    }


    newApplication() {
        this.editingApplicationName = null;
        this.sourceApplication = null;
        this.editedApplication = this.applicationEditor.newApplication(this.allApplication);
        this.editorModal.show();
    }


    editApplication(row: ApplicationEdit) {
        this.editingApplicationName = { name: row.bankId };
        this.sourceApplication = row;
        this.editedApplication = this.applicationEditor.editApplication(row, this.allApplication);
        this.editorModal.show();
    }


    deleteApplication(row: ApplicationEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.bankId + '\"?', DialogType.confirm, () => this.deleteApplicationHelper(row));
    }


    deleteApplicationHelper(row: ApplicationEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.applicationService.deleteApplication(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }




    get canViewRoles() {
        return this.applicationService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.applicationService.userHasPermission(Permission.manageUsersPermission);
    }

    get canCreateUsers() {
        return this.applicationService.userHasPermission(Permission.createUsersPermission);
    }

}
