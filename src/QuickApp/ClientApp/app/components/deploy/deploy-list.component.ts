﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { DeploymentService } from "../../services/deployment/deployment.service";
import { PosHistoryService } from "../../services/poshistory/pos-history.service";
import { DeployService } from "../../services/deploy/deploy.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { DeploymentRequest } from '../../models/deployment-request.model';
import { DeploymentRequestEdit } from '../../models/deployment-request-edit.model';
//import { DeploymentRequestComponent } from "./deployment-add.component";

import { Deploy } from '../../models/deploy/deploy.model';
import { DeployEdit } from '../../models/deploy/deploy-edit.model';
import { DeployEditComponent } from '../deploy/deploy-edit.component';
import { AddStatusComponent } from "./add-status.component";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
@Component({
    selector: 'deploy-list',
    templateUrl: './deploy-list.component.html',
    styleUrls: ['./deploy.component.css']
})
export class DeployListComponent implements OnInit, AfterViewInit {
    columns: any[] = [];
    columnDefs: any[] = [];
    private gridApi;
    private gridColumnApi;
    private gridOptions;
    rows: DeploymentRequest[] = [];
    rowsCache: DeploymentRequest[] = [];
    private deploymentEdit: DeploymentRequestEdit = new DeploymentRequestEdit();
    editedDeploy: DeployEdit;
    sourceDeploy: DeployEdit;
    editedDeployment: DeploymentRequestEdit;
    sourceDeployment: DeploymentRequestEdit;
    editingDeploymentName: {};
    editingConfigurationName: {};
    editingDeployName: {};
    loadingIndicator: boolean;
    private posHistory: DeploymentRequest = new DeploymentRequest();

    allDeployment: DeploymentRequest[] = [];
    allDeploy: DeployEdit[] = [];
    newDeploymentFlag = false;
    @Input()
    isGeneralEditor = false;

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('merchantItemTemplate')
    merchantItemTemplate: TemplateRef<any>;

    @ViewChild('outletItemTemplate')
    outletItemTemplate: TemplateRef<any>;

    @ViewChild('bankItemTemplate')
    bankItemTemplate: TemplateRef<any>;

    @ViewChild('statusItemTemplate')
    statusItemTemplate: TemplateRef<any>;

    @ViewChild('verificationItemTemplate')
    verificationItemTemplate: TemplateRef<any>;

    @ViewChild('requestTypeItemTemplate')
    requestTypeItemTemplate: TemplateRef<any>;

    @ViewChild('idMerchantTemplate')
    idMerchantTemplate: TemplateRef<any>;

    @ViewChild('idTerminalTemplate')
    idTerminalTemplate: TemplateRef<any>;

    @ViewChild('terminalTypeTemplate')
    terminalTypeTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('selectTemplate')
    selectTemplate: TemplateRef<any>;


    @ViewChild('deployModal')
    deployModal: ModalDirective;


    @ViewChild('deployEditor')
    deployEditor: DeployEditComponent;

    @ViewChild('merchantTemplate')
    merchantTemplate: TemplateRef<any>;

    @ViewChild('addStatusModal')
    addStatusModal: ModalDirective;

    @ViewChild('addStatusEditor')
    addStatusEditor: AddStatusComponent;

    constructor(private alertService: AlertService, private router: Router, private translationService: AppTranslationService,
        private deploymentService: DeploymentService,
        private deployService: DeployService, private posHistoryService: PosHistoryService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);
        this.columnDefs = [
            { headerName: 'Bank Name', field: 'bankName', width: 180, suppressSizeToFit: true },
            { headerName: 'Merchant Name', field: 'merchant', width: 250, suppressSizeToFit: true },
            { headerName: 'Merchant ID', field: 'idMerchant', width: 130, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 130, suppressSizeToFit: true },
            { headerName: 'Address', field: 'address', width: 130, suppressSizeToFit: true },
            { headerName: 'Contact Person', field: 'contactPerson', width: 130, suppressSizeToFit: true },
            { headerName: 'Contact No', field: 'contactNo1', width: 130, suppressSizeToFit: true},
            { headerName: 'Terminal Type', field: 'item', width: 250, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialKey', width: 150, suppressSizeToFit: true },
            { headerName: 'Previous S.No', field: 'previousSerial', width: 150, suppressSizeToFit: true, cellRenderer: this.previousSerial },
            { headerName: 'Request Type', field: 'requestName', width: 150, suppressSizeToFit: true },
            { headerName: 'Status', field: 'statusForDeploy', width: 150, suppressSizeToFit: true },

            
        ];

        if (this.canManageUsers)
            this.columnDefs.push({
                headerName: "Actions",
                suppressMenu: true,
                field: 'requestName',
                suppressSorting: true,
                width: 250,

                cellRenderer: this.actions,

            });


       
        this.addNewDeploymentToList();
        
    }
    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }
    onGridReady(params) {
        this.gridApi = params.api;

        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.deploymentService.getDeployInfo().subscribe(data => {
            params.api.setRowData(data);
        });

    }
    public previousSerial(params) {
        if (params.data.previousSerial == 'notreceived') {
            return (`<span style="font-size:12px;background-color:maroon;color:white">NOT RECEIVED</span>`)
        }
        else if (params.data.previousSerial == 'received') {
            return (`<span style="font-size:12px">RECEIVED</span>`)
        }
        else {
            return (`<span style="font-size:12px">USING SAME SERIAL NO</span>`);
        }

    }
    

    public actions(params) {
        if (params.data.statusName == "ConfigurationVerified" && params.data.requestName != "Delete") {

            return (`<button type= "button"  data-action-type="AddStatus"  class="btn btn-link btn-xs"  style = "background-color:maroon;color:white;text-decoration:none">Add Status</button>
           <button type= "button"  data-action-type="Deploy"  class="btn btn-link btn-xs"  style = "background-color:maroon;color:white;text-decoration:none">Deploy</button>`);
        }
        
        else if (params.data.statusName == "Deployed" && params.data.requestName != "Delete") {
            return (`<button type= "button"  data-action-type="AddStatus"  class="btn btn-link btn-xs"  style = "background-color:maroon;color:white;text-decoration:none">Add Status</button>
                <button type= "button"  data-action-type="VerifyDeploy"  class="btn btn-link btn-xs"   style = "background-color:maroon;color:white;text-decoration:none">Verify Deploy</button>`);
        }
       
        else {
            return null;
        }
    };

    public tooltipRenderer(params) {
        return ('<span title="' + params.value + '" >' + params.value + '</span>')
    }

    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;

            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {

                case "Deploy":
                    return this.onDeploy(data);
                case "AddStatus":
                    return this.onAddStatus(data);
                case "VerifyDeploy":
                    return this.onVerify(data);
               
            }
        }
    }
    public onDeploy(row: DeploymentRequestEdit) {

        this.loadDeploy(row.id);

    }
    public onAddStatus(row: DeploymentRequestEdit) {

        this.addStatus(row.id);

    }
    public onVerify(row: DeploymentRequestEdit) {

        this.loadDeploy(row.id);

    }
   
    public item(params) {
        return (params.data.primaryItem + " - " + params.data.secondaryItem + " - " + params.data.tertiaryItem + " - " + params.data.detailItem);
    }

    //addStatus(row: DeploymentRequestEdit) {

    //    this.editedDeployment = this.addStatusEditor.addDeployStatus(row, this.allDeployment);
    //    this.addStatusModal.show();
    //}
    addStatus(id) {

        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.deployService.getDeploy(id).subscribe(results => this.onDeployStatusLoadSuccessful(results), error => this.onDeployStatusLoadFailed(error));
    }
    onDeployStatusLoadSuccessful(deploy: Deploy) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.addStatusDeploy(deploy);
    }
    onDeployStatusLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    ngAfterViewInit() {

      

        this.deployEditor.changesSavedCallback = () => {
            this.addNewDeploymentToList();
            this.deployModal.hide();
        }
        this.deployEditor.changesCancelledCallback = () => {
            this.editedDeploy = null;
            this.sourceDeploy = null;
            this.deployModal.hide();
        }
        this.addStatusEditor.changesSavedCallback = () => {
            this.addNewDeploymentToList();
            this.addStatusModal.hide();
        }
        this.addStatusEditor.changesCancelledCallback = () => {
            this.editedDeployment = null;
            this.sourceDeployment = null;
            this.addStatusModal.hide();
        }

    }


    addNewDeploymentToList() {
        if (this.sourceDeployment) {
            Object.assign(this.sourceDeployment, this.editedDeployment);

            let sourceIndex = this.rowsCache.indexOf(this.sourceDeployment, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceDeployment, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedDeployment = null;
            this.sourceDeployment = null;
        }
        else {
            let deployment = new DeploymentRequest();
            Object.assign(deployment, this.editedDeployment);
            this.editedDeployment = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>DeploymentRequest).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, deployment);
            this.rows.splice(0, 0, deployment);
        }
        this.loadDeploymentData();
    }


    loadDeploymentData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.deploymentService.getDeployInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(deployments: DeploymentRequest[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        deployments.forEach((deployment, index, deployments) => {
            (<any>deployment).index = index + 1;
        });
        this.rowsCache = [...deployments];
        this.rows = deployments;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.bankName, r.merchant, r.idMerchant, r.idTerminal, r.item, r.address, r.contactPerson, r.serialKey, r.statusName, r.requestName));
    }


    onDeployModalHidden() {
        this.editingDeployName = null;
        this.deployEditor.resetForm(true);
    }
    onEditorAddStatusModalHidden() {
        this.addStatusEditor.resetForm(true);
    }

    loadDeploy(id) {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.deployService.getDeploy(id).subscribe(results => this.onDeployLoadSuccessful(results), error => this.onDeployLoadFailed(error));
        this.posHistoryService.getPosHistory(id).subscribe(results => this.onLoadSuccessful(results), error => this.onLoadFailed(error));
    }
    onDeployLoadSuccessful(deploy: Deploy) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.editDeploy(deploy);
    }
    onDeployLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }
    onLoadSuccessful(posHistory: DeploymentRequest) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.posHistory = posHistory;
        //this.editConfiguration(configuration);

    }


    onLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }
    editDeploy(deploy: DeployEdit) {
        this.editingDeployName = { name: deploy.receivedBy };
        this.sourceDeploy = deploy;
        this.editedDeploy = this.deployEditor.editDeploy(deploy, this.allDeploy);
        this.deployModal.show();
    }

    addStatusDeploy(deploy: DeployEdit) {
        this.sourceDeploy = deploy;
        this.editedDeploy = this.addStatusEditor.addDeployStatus(deploy, this.allDeploy);
        this.addStatusModal.show();
    }



    get canViewRoles() {
        return this.deploymentService.userHasPermission(Permission.viewRolesPermission)

    }

    get canManageUsers() {
        return this.deploymentService.userHasPermission(Permission.manageUsersPermission);
    }
    get canSeeSerial() {
        return this.deploymentService.userHasPermission(Permission.manageUsersPermission);
    }

}
