
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { DeployService } from "../../services/deploy/deploy.service";
import { Utilities } from "../../services/utilities";
import { Permission } from '../../models/permission.model';
import { Deploy } from '../../models/deploy/deploy.model';
import { DeployEdit } from '../../models/deploy/deploy-edit.model';
import { DeployEditComponent } from "./deploy-edit.component";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';


@Component({
    selector: 'deploy-management',
    templateUrl: './deploy.component.html',
    styleUrls: ['./deploy.component.css']
})
export class DeployComponent implements OnInit, AfterViewInit {
    columns: any[] = [];
    rows: Deploy[] = [];
    rowsCache: Deploy[] = [];
    editedDeploy: DeployEdit;
    sourceDeploy: DeployEdit;
    editingDeployName: {};
    loadingIndicator: boolean;

    allDeploy: Deploy[] = [];

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('receivedByTemplate')
    receivedByTemplate: TemplateRef<any>;

    @ViewChild('connectivityTemplate')
    connectivityTemplate: TemplateRef<any>;

    @ViewChild('MerchantLocationTemplate')
    merchantLocationTemplate: TemplateRef<any>;

    @ViewChild('latitudeTemplate')
    latitudeTemplate: TemplateRef<any>;

    @ViewChild('longitudeTemplate')
    longitudeTemplate: TemplateRef<any>;

    @ViewChild('deployDateTemplate')
    deployDateTemplate: TemplateRef<any>;

    @ViewChild('deployedByTemplate')
    deployedByTemplate: TemplateRef<any>;

    @ViewChild('imageTemplate')
    imageTemplate: TemplateRef<any>;

    @ViewChild('remarksTemplate')
    remarksTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('deployModal')
    deployModal: ModalDirective;

    @ViewChild('deployEditor')
    deployEditor: DeployEditComponent;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private deployService: DeployService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        this.columns = [
            { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'deployDate', name: gT('deploy.management.DeployDate'), width: 50, cellTemplate: this.deployDateTemplate },
            { prop: 'receivedBy', name: gT('deploy.management.ReceivedBy'), width: 120, cellTemplate: this.receivedByTemplate },
           // { prop: 'deployedBy', name: gT('deploy.management.DeployedBy'), width: 140, cellTemplate: this.deployedByTemplate },
            { prop: 'connectivity', name: gT('deploy.management.Connectivity'), width: 140, cellTemplate: this.connectivityTemplate },
            { prop: 'merchantLocation', name: gT('deploy.management.MerchantLocation'), width: 140, cellTemplate: this.merchantLocationTemplate },
            { prop: 'latitude', name: gT('deploy.management.latitude'), width: 140, cellTemplate: this.latitudeTemplate },
            { prop: 'image', name: gT('deploy.management.Image'), width: 140, cellTemplate: this.imageTemplate },
            { prop: 'remarks', name: gT('deploy.management.Remarks'), width: 140, cellTemplate: this.remarksTemplate }
        ];

        if (this.canManageUsers)
            this.columns.push({ name: '', width: 130, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });



        this.loaddeployData();
    }


    ngAfterViewInit() {

        this.deployEditor.changesSavedCallback = () => {
            this.addNewDeployToList();
            this.deployModal.hide();
        };

        this.deployEditor.changesCancelledCallback = () => {
            this.editedDeploy = null;
            this.sourceDeploy = null;
            this.deployModal.hide();
        };

    }


    addNewDeployToList() {
        if (this.sourceDeploy) {
            Object.assign(this.sourceDeploy, this.editedDeploy);

            let sourceIndex = this.rowsCache.indexOf(this.sourceDeploy, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceDeploy, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedDeploy = null;
            this.sourceDeploy = null;
        }
        else {
            let deploy = new Deploy();
            Object.assign(deploy, this.editedDeploy);
            this.editedDeploy = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>deploy).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, deploy);
            this.rows.splice(0, 0, deploy);
        }
    }


    loaddeployData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.deployService.getDeployInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(deploys: Deploy[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        deploys.forEach((deploy, index, deploys) => {
            (<any>deploy).index = index + 1;
        });
        this.rowsCache = [...deploys];
        this.rows = deploys;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.deployDate, r.connectivity, r.receivedBy, r.merchantLocation));
    }

    onEditorModalHidden() {
        this.editingDeployName = null;
        this.deployEditor.resetForm(true);
    }


    newDeploy() {
        this.editingDeployName = null;
        this.sourceDeploy = null;
        this.editedDeploy = this.deployEditor.newDeploy(this.allDeploy);
        this.deployModal.show();
    }


    editDeploy(row: DeployEdit) {
        this.editingDeployName = { name: row.deployDate };
        this.sourceDeploy = row;
        this.editedDeploy = this.deployEditor.editDeploy(row, this.allDeploy);
        this.deployModal.show();
    }


    deletedeploy(row: DeployEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.deployDate + '\"?', DialogType.confirm, () => this.deletedeployHelper(row));
    }


    deletedeployHelper(row: DeployEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.deployService.deleteDeploy(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }




    get canViewRoles() {
        return this.deployService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.deployService.userHasPermission(Permission.manageUsersPermission);
    }

}
