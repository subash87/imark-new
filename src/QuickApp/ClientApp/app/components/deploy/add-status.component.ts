﻿import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { DeployService } from "../../services/deploy/deploy.service";
import { Utilities } from '../../services/utilities';
import { Deploy } from '../../models/deploy/deploy.model';
import { DeployEdit } from '../../models/deploy/deploy-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';


@Component({
    selector: 'add-status',
    templateUrl: './add-status.component.html',
    styleUrls: ['./deploy.component.css']
})
export class AddStatusComponent implements OnInit {
    verify = [
        { value: 1, text: 'Pending' },
        { value: 2, text: 'Verified' },
        //{ value: 3, text: 'Rejected' },
    ];
    maxDate = new Date();
    minDate: Date;
    private isEditMode = false;
    private isNewDeploy = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingDeployName: string;
    private uniqueId: string = Utilities.uniqueId();
    private deploy: Deploy = new Deploy();
    private allDeploy: Deploy[] = [];
    private deployEdit: DeployEdit = new DeployEdit();
    private rowsConnectivity: Deploy[] = [];

    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;
    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('deployDate')
    private deployDate;

    @ViewChild('createdDate')
    private createdDate;

    @ViewChild('deploymentId')
    private deploymentId;



    @ViewChild('statusForDeploy')
    private statusForDeploy;



    constructor(private alertService: AlertService, private deployService: DeployService) {
    }

    ngOnInit() {
        //this.isViewOnly = true;
        if (!this.isGeneralEditor) {
            this.loadCurrentdeployData();
        }
        //let min = this.maxDate.getDate() - this.maxDate.getDay() - 6;
        //let minLast = min + 6;
        //this.minDate = new Date(this.maxDate.setDate(minLast));
        //this.maxDate = new Date();
        //this.deployDate = new Date();
    }



    private loadCurrentdeployData() {
        this.alertService.startLoadingMessage();

        if (this.canViewAllRoles) {
            this.deployService.getDeploy().subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
        }

    }


    private onCurrentUserDataLoadSuccessful(deploy: Deploy) {
        this.alertService.stopLoadingMessage();
        this.deploy = deploy;

    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.deploy = new Deploy();
    }
   
    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.deployEdit = new DeployEdit();
            Object.assign(this.deployEdit, this.deploy);
        }
        else {
            if (!this.deployEdit)
                this.deployEdit = new DeployEdit();

            this.isEditingSelf = this.deployService.updateDeploy(this.deploy) ? this.deployEdit.id == this.deploy.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveDeploy() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        if (this.isNewDeploy) {
            this.deployService.newDeploy(this.deployEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            this.deployService.updateDeploy(this.deployEdit, this.deployEdit.dispatch).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(deploy?: Deploy) {
        this.testIsdeployChanged(this.deploy, this.deployEdit);

        if (deploy)
            Object.assign(this.deployEdit, deploy);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.deploy, this.deployEdit);
        this.deployEdit = new DeployEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewDeploy)
                this.alertService.showMessage("Success", `Deploy  \"${this.deploy.deployDate}\" was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Deploy \"${this.deploy.deployDate}\" was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private testIsdeployChanged(deploy: Deploy, editedDeploy: Deploy) {

        //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
        //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);

        //let modifiedRoles = rolesAdded.concat(rolesRemoved);

        //if (modifiedRoles.length)
        //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    }



    private cancel() {
        if (this.isGeneralEditor)
            this.deployEdit = this.deploy = new DeployEdit();
        else
            this.deployEdit = new DeployEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.deployEdit = this.deploy = new DeployEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newDeploy(allDeploy: Deploy[]) {
        this.isGeneralEditor = true;
        this.isNewDeploy = true;

        this.allDeploy = [...allDeploy];
        this.editingDeployName = null;
        this.deploy = this.deployEdit = new DeployEdit();
        //this.posEdit.isEnabled = true;
        this.edit();

        return this.deployEdit;
    }

    addDeployStatus(deploy: Deploy, allDeploy: Deploy[]) {
        if (deploy) {
            this.isGeneralEditor = true;
            this.isNewDeploy = false;
            this.editingDeployName = deploy.receivedBy;
            this.deploy = new Deploy();
            this.deployEdit = new DeployEdit();
            Object.assign(this.deploy, deploy);
            Object.assign(this.deployEdit, deploy);
            this.edit();

            return this.deployEdit;
        }
        else {
            return this.newDeploy(allDeploy);
        }
    }





    get canViewAllRoles() {
        return this.deployService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.deployService.userHasPermission(Permission.assignRolesPermission);
    }
}
