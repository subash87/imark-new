﻿

import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { InventoryService } from "../../services/inventory.service";
import { Utilities } from '../../services/utilities';
import { Inventory } from '../../models/inventory.model';
import { InventoryEdit } from '../../models/inventory-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';


@Component({
    selector: 'posSpares-edit',
    templateUrl: './posSpares-edit.component.html',
    styleUrls: ['./posSpares.component.css']
})
export class PosSparesEditComponent implements OnInit {

    private isEditMode = false;
    private isNewInventory = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingInventoryName: string;
    private uniqueId: string = Utilities.uniqueId();
    private inventory: Inventory = new Inventory();
    private allInventory: Inventory[] = [];
    private inventoryEdit: InventoryEdit;
 

    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('primaryItem')
    private primaryItem;

    @ViewChild('secondaryItem')
    private SecondaryItem;

    @ViewChild('tertiaryItem')
    private secondaryItem;

    @ViewChild('detailItem')
    private detailItem;

    @ViewChild('hasSerialNo')
    private hasSerialNo;

    constructor(private alertService: AlertService, private inventoryService: InventoryService) {
    }

    ngOnInit() {
        if (!this.isGeneralEditor) {
            this.loadCurrentInventoryData();
        }
    }



    private loadCurrentInventoryData() {
        this.alertService.startLoadingMessage();

        if (this.canViewAllRoles) {
            this.inventoryService.getInventory().subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
        }
     
    }


    private onCurrentUserDataLoadSuccessful(inventory: Inventory) {
        this.alertService.stopLoadingMessage();
        this.inventory = inventory;
      
    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to save inventory to the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.inventory = new Inventory();
    }


    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }

   

    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.inventoryEdit = new InventoryEdit();
            Object.assign(this.inventoryEdit, this.inventory);
        }
        else {
            if (!this.inventoryEdit)
                this.inventoryEdit = new InventoryEdit();

            this.isEditingSelf = this.inventoryService.updateInventory(this.inventory) ? this.inventoryEdit.id == this.inventory.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveInventory() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");
        this.inventoryEdit.hasSerialNo = false;
        if (this.isNewInventory) {
            this.inventoryService.newInventory(this.inventoryEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            this.inventoryService.updateInventory(this.inventoryEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(inventory?: Inventory) {
        this.testIsInventoryChanged(this.inventory, this.inventoryEdit);

        if (inventory)
            Object.assign(this.inventoryEdit, inventory);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.inventory, this.inventoryEdit);
        this.inventoryEdit = new InventoryEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewInventory)
                this.alertService.showMessage("Success", `User \"${this.inventory.primaryItem}\" was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Changes to user \"${this.inventory.primaryItem}\" was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private testIsInventoryChanged(inventory: Inventory, editedInventory: Inventory) {

        //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
        //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);

        //let modifiedRoles = rolesAdded.concat(rolesRemoved);

        //if (modifiedRoles.length)
        //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    }



    private cancel() {
        if (this.isGeneralEditor)
            this.inventoryEdit = this.inventory = new InventoryEdit();
        else
            this.inventoryEdit = new InventoryEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.inventoryEdit = this.inventory = new InventoryEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newInventory(allInventory: Inventory[]) {
        this.isGeneralEditor = true;
        this.isNewInventory = true;

        this.allInventory = [...allInventory];
        this.editingInventoryName = null;
        this.inventory = this.inventoryEdit = new InventoryEdit();
        //this.posEdit.isEnabled = true;
        this.edit();

        return this.inventoryEdit;
    }

    editInventory(inventory: Inventory, allInventory: Inventory[]) {
        if (inventory) {
            this.isGeneralEditor = true;
            this.isNewInventory = false;
            this.editingInventoryName = inventory.primaryItem;
            this.inventory = new Inventory();
            this.inventoryEdit = new InventoryEdit();
            Object.assign(this.inventory, inventory);
            Object.assign(this.inventoryEdit, inventory);
            this.edit();

            return this.inventoryEdit;
        }
        else {
            return this.newInventory(allInventory);
        }
    }


 


    get canViewAllRoles() {
        return this.inventoryService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.inventoryService.userHasPermission(Permission.assignRolesPermission);
    }
}
