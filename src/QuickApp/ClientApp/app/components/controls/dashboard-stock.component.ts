﻿
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertService, DialogType, AlertMessage, MessageSeverity } from '../../services/alert.service';
import { Bank } from '../../models/bank/bank.model';
import { BankEdit } from '../../models/bank/bank-edit.model';
import { DashboardService } from '../../services/dashboard/dashboard.service';
require('chart.js');

import { Utilities } from "../../services/utilities";

@Component({
    selector: 'stock-demo',
    templateUrl: './dashboard-stock.component.html',
    styleUrls: ['./statistics-demo.component.css']
})
export class DashboardStockComponent implements OnInit, OnDestroy {
    rows: Bank[] = [];
    rowsCache: BankEdit[] = [];
    previousMonth: number;
    currentMonth: number;
    total: number;
    bankName: string;
    bank: Bank = new Bank();

   
    stockData: Array<any> =
        [{ data: [], label: [] }, { data: [], label: [] }, { data: [], label: [] }];

  
    stockLabels: Array<any> = ['In Stock'];
    chartOptions: any = {
        responsive: true,
        title: {
            display: false,
            fontSize: 16,
            text: 'Important Stuff'
        }
    };
    chartColors: Array<any> = [
        { // grey
            backgroundColor: '#f38b4a',
            borderColor: '#f38b4a',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#f38b4a',
            pointHoverBackgroundColor: '#f38b4a',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
            backgroundColor: '#56d798',
            borderColor: '#56d798',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#56d798',
            pointHoverBackgroundColor: '#56d798',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // something else
            backgroundColor: '#ff8397',
            borderColor: '#ff8397',
            pointBackgroundColor: '#ff8397',
            pointBorderColor: '#ff8397',
            pointHoverBackgroundColor: '#ff8397',
            pointHoverBorderColor: 'rgba(128,128,128,0.8)'
        }
    ];
   
    chartLegend: boolean = true;
    chartType: string = 'bar';

    timerReference: any;



    constructor(private alertService: AlertService, private dashboardService: DashboardService) {
    }


    ngOnInit() {
        //this.timerReference = setInterval(() => this.randomize(), 5000);
        this.loadStockData();
        //this.chartType = "line";
    }

    ngOnDestroy() {
        clearInterval(this.timerReference);
    }


    loadStockData() {

        this.dashboardService.getStockDashboardInfo().subscribe(results => {
            let stock = results.map(results => results.totalStock);
            let primary = results.map(results => results.primaryItem);
            let secondary = results.map(results => results.secondaryItem);
            let tertiary = results.map(results => results.tertiaryItem);
            let detail = results.map(results => results.detailItem);
            var counter = results.length;
            //var count = 0;
            let chart =
                [{ data: [], label: [] }];

            this.stockData = Object.assign(chart);
            for (let count=0; count < counter; count++) {
                chart.splice(0, 0, { data: [stock[count]], label: [primary[count] + " " + secondary[count] + " " + tertiary[count] + " " + detail[count]] })
            }


        })
    };

    changeChartType(type: string) {
        this.chartType = type;
    }

    showMessage(msg: string): void {
        this.alertService.showMessage("Demo", msg, MessageSeverity.info);
    }

    showDialog(msg: string): void {
        this.alertService.showDialog(msg, DialogType.prompt, (val) => this.configure(true, val), () => this.configure(false));
    }

    configure(response: boolean, value?: string) {

        if (response) {

            this.alertService.showStickyMessage("Simulating...", "", MessageSeverity.wait);

            setTimeout(() => {

                this.alertService.resetStickyMessage();
                this.alertService.showMessage("Demo", `Your settings was successfully configured to \"${value}\"`, MessageSeverity.success);
            }, 2000);
        }
        else {
            this.alertService.showMessage("Demo", "Operation cancelled by user", MessageSeverity.default);
        }
    }



    // events
    chartClicked(e: any): void {
        console.log(e);
    }

    chartHovered(e: any): void {
        console.log(e);
    }
}