﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DateRangePickerModule, IDateRange } from 'ng-pick-daterange';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { PosStockService } from "../../services/posStock/pos-stock.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { PosStock } from '../../models/posStock/pos-stock.model';
import { PosStockEdit } from '../../models/posStock/pos-stock-edit.model';
import { InventoryAdd } from '../../models/inventory-add.model';
import { InventoryAddEdit } from '../../models/inventory-add-edit.model';
import { Inventory } from '../../models/inventory.model';
import { InventoryEdit } from '../../models/inventory-edit.model';
import * as moment from 'moment';
import * as $ from 'jquery';






@Component({
    selector: 'pos-stock-history',
    templateUrl: './pos-stock-history.component.html',
    styleUrls: ['./stock.component.css']
})


export class PosStockHistoryComponent implements OnInit, AfterViewInit {
    columnDefs: any[] = [];
    columns: any[] = [];
    rows: PosStock[] = [];
    rowsCache: PosStock[] = [];
    serialId: string;
    loadingIndicator: boolean;
    posStockHistory: PosStock = new PosStock();
    allInventory: Inventory[] = [];
    private inventoryAddEdit: InventoryEdit = new InventoryEdit();
    private inventoryAdd: Inventory = new Inventory();
    public changesPosStockSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;
    constructor(private alertService: AlertService, private router: Router, private translationService: AppTranslationService, private posStockService: PosStockService) {
    }


    ngOnInit() {
        this.columnDefs = [

            { headerName: 'Bank Name', field: 'bankName', suppressSizeToFit: true, enableTooltip: true},
            { headerName: 'Serial Number', field: 'serialNumber', suppressSizeToFit: true },
            {
                headerName: 'Sold Date', field: 'stockDate', valueFormatter: (data) => moment(data.value).format('L'), suppressSizeToFit: true

            },
            //{ headerName: 'Remarks', field: 'remarks', suppressSizeToFit: true },
            //{ headerName: 'Location', field: 'location', suppressSizeToFit: true },
            { headerName: 'Created By', field: 'createdBy', suppressSizeToFit: true },
           
            //{
            //    headerName: "Actions",
            //    suppressMenu: true,
            //    suppressSorting: true,
            //    template:
            //        `<button type="button" data-action-type="Delete" class="btn btn-link btn-xs">
            //  Delete
            // </button>`
            //}
        ];

        if (this.canManageUsers)
            this.columnDefs.push({
                headerName: "Actions",
                suppressMenu: true,
                field: '',
                suppressSorting: true,
                width: 250,                
                template:
                    `<button type="button" data-action-type="Delete" class="btn btn-link btn-xs">
              Delete
             </button>`
            });

        this.loadPosStockData();


    }
    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {
                case "Delete":
                    return this.deleteStock(data);

            }
        }
    }
    

    ngAfterViewInit() {

       

    }




    loadPosStockData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.posStockService.getPosStock(-1, -1, this.serialId).subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));


    }


    onDataLoadSuccessful(stocks: PosStock[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        stocks.forEach((stock, index, stocks) => {
            (<any>stock).index = index + 1;
        });
        this.rowsCache = [...stocks];
        this.rows = stocks;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve Pos Stock.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.bankName, r.serialNumber, r.location, r.remarks));
    }
    deleteStock(row: PosStockEdit) {
        this.alertService.showDialog('Are you sure you want to delete Stock Serial Number \"' + row.serialNumber + '\"?', DialogType.confirm, () => this.deleteSupportHelper(row));
    }

    deleteSupportHelper(row: PosStockEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.posStockService.deletePosStock(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
                if (this.changesPosStockSavedCallback)
                    this.changesPosStockSavedCallback();
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the stock.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }
    loadSerialList(allInventory: Inventory[], id) {
        this.posStockHistory.inventoryAddId = id;
        this.serialId = id;
        this.allInventory = [...allInventory];
        this.inventoryAdd = this.inventoryAddEdit = new InventoryEdit();
        this.loadPosStockData();
        return this.inventoryAddEdit;
    }
   
    get canViewRoles() {
        return this.posStockService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.posStockService.userHasPermission(Permission.manageUsersPermission);
    }
    get canSeeSerial() {
        return this.posStockService.userHasPermission(Permission.manageUsersPermission);
    }

}
