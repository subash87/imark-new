﻿import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { PosHistoryService } from "../../services/poshistory/pos-history.service";
import { DeploymentService } from "../../services/deployment/deployment.service";
import { ConfigurationsService } from "../../services/configuration/configuration.service";
import { DeployService } from "../../services/deploy/deploy.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { DeploymentRequest } from '../../models/deployment-request.model';
import { DeploymentRequestEdit } from '../../models/deployment-request-edit.model';
import { DeploymentRequestComponent } from "../deployment/deployment-add.component";
import { ConfigurationEdit } from '../../models/configuration/configuration-edit.model';
import { Configuration } from '../../models/configuration/configuration.model';
import { ConfigurationEditComponent } from '../configuration/configuration-edit.component';
import { Deploy } from '../../models/deploy/deploy.model';
import { DeployEdit } from '../../models/deploy/deploy-edit.model';
import { DeployEditComponent } from '../deploy/deploy-edit.component';
import { PosHistory } from "../../models/posHistory/pos-history.model";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import { OtherStockEditComponent } from '../../components/otherstock/otherstock-edit.component';
import { OtherStockListComponent } from '../../components/otherstock/otherstock.component';
import { SupportEditComponent } from '../../components/support/support-edit.component';
import { SupportComponent } from '../../components/support/support.component';
import { PosHistoryRecordComponent } from '../../components/posHistory/history-record.component';
import { OtherStock } from '../../models/otherstock/otherstock.model';
import { OtherStockEdit } from '../../models/otherstock/otherstock-edit.model';
import { Support } from '../../models/support/support.model';
import { SupportEdit } from '../../models/support/support-edit.model';
import { PosHistoryEdit } from '../../models/posHistory/pos-edit.model';


import * as moment from 'moment';
import * as $ from 'jquery';

//import "ag-grid-enterprise";


@Component({
    selector: 'pos-spares-stock',
    templateUrl: './pod-spares-stock.component.html',
    styleUrls: ['./stock.component.css']
})
export class PosSparesStockComponent implements OnInit, AfterViewInit {
    columnDefs: any[] = [];
    private gridApi;
    private gridColumnApi;

    columns: any[] = [];
    rows: PosHistory[] = [];
    rowsCache: PosHistory[] = [];
    @Input()
    isGeneralEditor = false;

    private other: OtherStock = new OtherStock();
    private support: Support = new Support();
    editedOtherStock: OtherStockEdit;
    sourceOtherStock: OtherStockEdit;
    editedSupport: SupportEdit;
    sourceSupport: SupportEdit;
    editedPosHistory: PosHistoryEdit;
    editedPosHistory1: PosHistoryEdit;
    sourcePosHistory: PosHistoryEdit;
    allOtherStock: OtherStock[] = [];
    allSupport: Support[] = [];
    loadingIndicator: boolean;

    allPosHistory: PosHistory[] = [];
    allPosHistory1: PosHistory[] = [];



    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('supportModal')
    supportModal: ModalDirective;

    @ViewChild('individualHistoryModal')
    individualHistoryModal: ModalDirective;

    @ViewChild('supportEditor')
    supportEditor: SupportEditComponent;

    @ViewChild('supportListEditor')
    supportListEditor: SupportComponent;

    @ViewChild('otherStockEditor')
    otherStockEditor: OtherStockEditComponent;

    @ViewChild('stockListEditor')
    stockListEditor: OtherStockListComponent;

    @ViewChild('historyRecordEditor')
    historyRecordEditor: PosHistoryRecordComponent;


    //@ViewChild('stockListEditor')
    //stockListEditor: OtherStockListComponent;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private poshistoryService: PosHistoryService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);


        this.columnDefs = [
            { headerName: 'Bank Name', field: 'bankName', width: 250, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: '', width: 250, suppressSizeToFit: true, cellRenderer: this.item },
            { headerName: 'Serial Number', field: 'serialKey', width: 250, suppressSizeToFit: true},
            { headerName: 'Merchant ID', field: 'idMerchant', width: 250, suppressSizeToFit: true },
            { headerName: 'Terminal ID', field: 'idTerminal', width: 150, suppressSizeToFit: true },
            { headerName: 'Outlet', field: 'outlet', width: 150, suppressSizeToFit: true },
           
            //{
            //    headerName: "Actions",
            //    suppressMenu: true,
            //    suppressSorting: true,
            //    width: 250,
            //    template:
            //        `<button type="button" data-action-type="NewStock" class="btn btn-link btn-xs" style="background-color:maroon;color:white;text-decoration:none">
            // sell
            //  </button>
            // `
            //}
        ];

        if (this.canManageUsers)
            this.columnDefs.push({
                headerName: "Actions",
                suppressMenu: true,
                field: '',
                suppressSorting: true,
                width: 250,

                cellRenderer: this.canViewUsers,
                template:
                    `<button type="button" data-action-type="NewStock" class="btn btn-link btn-xs" style="background-color:maroon;color:white;text-decoration:none">
             sell
              </button>
             `
            });

        this.addNewPosHistoryToList();
    }
    onGridReady(params) {
        this.gridApi = params.api;

        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.poshistoryService.getPosSparesHistoryInfo().subscribe(data => {
            params.api.setRowData(data);
        });

    }
    public item(params) {
        return (params.data.primaryItem + " - " + params.data.secondaryItem + " - " + params.data.tertiaryItem + " - " + params.data.detailItem);
    }

    public loadingBank(params) {
        if (params.data.bankName == null) {
            return ('<span style="font-size=50px"><i  class="fa fa-circle-o-notch">LOADING....</i></span>');
        }
        else {
            return (params.data.bankName);
        }
    }
   

    ngAfterViewInit() {

        this.otherStockEditor.changesOtherStockSavedCallback = () => {
            this.addNewPosHistoryToList();
            this.editorModal.hide();
        };

        this.otherStockEditor.changesOtherStockCancelledCallback = () => {
            this.editedOtherStock = null;
            this.sourceOtherStock = null;
            this.editorModal.hide();
        };
      

    }
    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {
                case "NewStock":
                    return this.onActionViewClick(data);
               
            }
        }
    }
   
    public onActionViewClick(data: PosHistoryEdit) {
        this.other.deploymentRequestId = data.id;
        this.editedOtherStock = this.otherStockEditor.newOtherStock(this.allOtherStock, data.deploymentRequestId);

        this.editorModal.show();
        this.loadStockList(data.deploymentRequestId);
    }

    public loadStockList(id) {

        this.editedOtherStock = this.stockListEditor.loadStockList(this.allOtherStock, id);
    }

    onEditorModalHidden() {
        this.otherStockEditor.resetForm(true);
    }
    
  
   

    addNewPosHistoryToList() {
        if (this.sourcePosHistory) {
            Object.assign(this.sourcePosHistory, this.editedPosHistory);

            let sourceIndex = this.rowsCache.indexOf(this.sourcePosHistory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourcePosHistory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedPosHistory = null;
            this.sourcePosHistory = null;
        }
        else {
            let pos = new PosHistory();
            Object.assign(pos, this.editedPosHistory);
            this.editedPosHistory = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>PosHistory).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, pos);
            this.rows.splice(0, 0, pos);
        }
        this.loadPosHistorysData();
    }


    loadPosHistorysData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.poshistoryService.getPosSparesHistoryInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(posHistorys: PosHistory[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        posHistorys.forEach((posHistory, index, posHistorys) => {
            (<any>posHistory).index = index + 1;
        });
        this.rowsCache = [...posHistorys];
        this.rows = posHistorys;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }



    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.serialKey, r.merchant, r.idMerchant, r.idTerminal, r.bankName, r.district));
    }



    deletePosHistory(row: PosHistory) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.bankName + '\"?', DialogType.confirm, () => this.deletePosHistoryHelper(row));
    }


    deletePosHistoryHelper(row: PosHistory) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.poshistoryService.deletePosHistory(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }



    get canViewUsers() {
        return this.poshistoryService.userHasPermission(Permission.viewUsersPermission);
    }

    get canViewRoles() {
        return this.poshistoryService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.poshistoryService.userHasPermission(Permission.manageUsersPermission);
    }

}

