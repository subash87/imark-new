﻿import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { MaintenanceService } from "../../services/maintenance/maintenance.service";
import { Utilities } from '../../services/utilities';
import { PosHistory } from '../../models/posHistory/pos-history.model';
import { PosHistoryEdit } from '../../models/posHistory/pos-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { Maintenance } from '../../models/maintenance/maintenance.model';
import { MaintenanceEdit } from '../../models/maintenance/maintenance-edit.model';
import { ConfigurationsService } from '../../services/configuration/configuration.service';



@Component({
    selector: 'maintenance-status',
    templateUrl: './maintenance-add-status.component.html',
    styleUrls: ['./deployment.component.css']
})
export class MaintenanceAddStatusComponent implements OnInit {

    private isEditMode = false;
    private isNewMaintenance = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingDeployName: string;
    private uniqueId: string = Utilities.uniqueId();
    private maintenance: Maintenance = new Maintenance();
    private allMaintenance: Maintenance[] = [];
    private maintenanceEdit: MaintenanceEdit = new MaintenanceEdit();


    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;
    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;




    @ViewChild('status')
    private status;



    constructor(private alertService: AlertService, private maintenanceService: MaintenanceService) {
    }

    ngOnInit() {


    }





    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.maintenanceEdit = new MaintenanceEdit();
            Object.assign(this.maintenanceEdit, this.maintenance);
        }
        else {
            if (!this.maintenanceEdit)
                this.maintenanceEdit = new MaintenanceEdit();
            this.maintenanceEdit.status = null;
            this.isEditingSelf = this.maintenanceService.updateMaintenance(this.maintenance) ? this.maintenanceEdit.id == this.maintenance.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveMaintenance() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");


        this.maintenanceService.newMaintenanceFromDeployment(this.maintenanceEdit, this.maintenanceEdit.deploymentRequestId).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));

    }


    private saveSuccessHelper(maintenance?: Maintenance) {


        if (maintenance)
            Object.assign(this.maintenanceEdit, maintenance);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.maintenance, this.maintenanceEdit);
        this.maintenanceEdit = new MaintenanceEdit();
        this.resetForm();


        if (this.isGeneralEditor) {

            this.alertService.showMessage("Success", `  \"${this.maintenanceEdit.status}\" was created successfully`, MessageSeverity.success);

        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private cancel() {
        if (this.isGeneralEditor)
            this.maintenanceEdit = this.maintenance = new MaintenanceEdit();
        else
            this.maintenanceEdit = new MaintenanceEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.maintenanceEdit = this.maintenance = new MaintenanceEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }





    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    //newDamage(allDeploy: Deploy[]) {
    //    this.isGeneralEditor = true;
    //    this.isNewDeploy = true;

    //    this.allDeploy = [...allDeploy];
    //    this.editingDeployName = null;
    //    this.deploy = this.deployEdit = new DeployEdit();
    //    //this.posEdit.isEnabled = true;
    //    this.edit();

    //    return this.deployEdit;
    //}

    addMaintenanceStatus(maintenance: Maintenance, id) {
        //if (deploy) {
        this.isGeneralEditor = true;
        //this.isNewDeploy = false;


        this.maintenance = new Maintenance();
        this.maintenanceEdit = new MaintenanceEdit();
        Object.assign(this.maintenance, maintenance);
        Object.assign(this.maintenanceEdit, maintenance);

        this.edit();

        return this.maintenanceEdit;
        //}
        //else {
        //    return this.newDeploy(allDeploy);
        //}
    }





    get canViewAllRoles() {
        return this.maintenanceService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.maintenanceService.userHasPermission(Permission.assignRolesPermission);
    }
}
