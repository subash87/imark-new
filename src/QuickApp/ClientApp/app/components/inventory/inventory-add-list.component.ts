﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DateRangePickerModule, IDateRange } from 'ng-pick-daterange';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { InventoryAddService } from "../../services/inventory-add.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { Inventory } from '../../models/inventory.model';
import { InventoryAdd } from '../../models/inventory-add.model';
import { InventoryAddEdit } from '../../models/inventory-add-edit.model';
import { InventoryAddEditComponent } from "./inventory-add-edit.component";
import { SerialAddComponent } from "./serial-no.component";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import * as moment from 'moment';
import * as $ from 'jquery';





@Component({
    selector: 'inventory-list',
    templateUrl: './inventory-add-list.component.html',
    styleUrls: ['./inventory.component.css']
})


export class InventoryAddListComponent implements OnInit, AfterViewInit {
    columnDefs: any[] = [];
    columns: any[] = [];
    rows: InventoryAdd[] = [];
    rowsCache: InventoryAdd[] = [];
    editedInventory: InventoryAddEdit;
    sourceInventory: InventoryAddEdit;
    editingInventoryName: {};
    loadingIndicator: boolean;
    inventoryAdd: InventoryAdd = new InventoryAdd();
    allInventory: InventoryAdd[] = [];
    public edited: boolean;
    public datefilter: boolean;
   
    @Input()
    isGeneralEditor = false;

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('dateRangeTemplate')
    dateRangeTemplate: TemplateRef<any>;

    @ViewChild('PrimaryItemTemplate')
    primaryItemTemplate: TemplateRef<any>;

    @ViewChild('secondaryItemTemplate')
    secondaryItemTemplate: TemplateRef<any>;

    @ViewChild('tertiaryItemTemplate')
    tertiaryItemTemplate: TemplateRef<any>;

    @ViewChild('detailItemTemplate')
    detailItemTemplate: TemplateRef<any>;

    @ViewChild('hasSerialNoTemplate')
    hasSerialNoTemplate: TemplateRef<any>;

    @ViewChild('totalNumberTemplate')
    totalNumberTemplate: TemplateRef<any>;

    @ViewChild('boughtDateTemplate')
    boughtDateTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('inventoryAddEditor')
    inventoryAddEditor: InventoryAddEditComponent;

    @ViewChild('serialEditor')
    serialEditor: SerialAddComponent;


    constructor(private alertService: AlertService, private router: Router, private translationService: AppTranslationService, private inventoryAddService: InventoryAddService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        //this.columns = [

        //    { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
        //    { prop: 'primaryItem', name: gT('inventory.management.PrimaryItem'), width: 50, cellTemplate: this.primaryItemTemplate },
        //    { prop: 'secondaryItem', name: gT('inventory.management.SecondaryItem'), width: 90, cellTemplate: this.secondaryItemTemplate },
        //    { prop: 'tertiaryItem', name: gT('inventory.management.TertiaryItem'), width: 120, cellTemplate: this.tertiaryItemTemplate },
        //    { prop: 'detailItem', name: gT('inventory.management.DetailItem'), width: 140, cellTemplate: this.detailItemTemplate },
        //    //{ prop: 'hasSerialNo', name: gT('inventory.management.HasSerialNo'), width: 140, cellTemplate: this.hasSerialNoTemplate },
        //    { prop: 'totalNumber', name: gT('inventory.management.TotalNumber'), width: 140, cellTemplate: this.totalNumberTemplate },
        //    //{ prop: 'stock', name: "Stock", width: 140, cellTemplate: this.hasSerialNoTemplate },
        //    { prop: 'buyDate', name: gT('inventory.management.BuyDate'), width: 140, cellTemplate: this.boughtDateTemplate },
        //];

        //if (this.canManageUsers)
        //    this.columns.push({ name: '', width: 280, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });

        this.columnDefs = [
            {


                headerName: 'Main Item', field: 'primaryItem',width:150
            },
            { headerName: 'Sub Item', field: 'secondaryItem', width: 100 },
            { headerName: 'Third Item', field: 'tertiaryItem', width: 100 },
            { headerName: 'Detail Item', field: 'detailItem', width: 100 },
            { headerName: 'Total', field: 'totalNumber', width: 140 },

            { headerName: 'Available', field: 'available', width: 100 },
            { headerName: 'Configure/Reconfigure', field: 'reConfigureNumber', width: 180 },
            { headerName: 'Pending Deploy', field: 'pendingDeploy', width: 150 },
            { headerName: 'Deployed', field: 'deployNumber', width: 100, cellRenderer: this.deployed },
            { headerName: 'Sold', field: 'soldNumber', width: 100, cellRenderer: this.sold },
            { headerName: 'Maintenance', field: 'maintenanceNumber', width: 120, cellRenderer: this.maintenance },
            { headerName: 'Not Received', field: 'notReceivedNumber', width: 150, cellRenderer: this.notReceived },
            { headerName: 'Damage', field: 'damageNumber', width: 120, cellRenderer: this.damage },
            { headerName: 'Add Serial No.', field: 'remainingNumber', width: 150, cellRenderer: this.options },

            //{ headerName: 'In Stock', field: 'stock', width: 150 },
            {
                headerName: 'Buy Date', field: 'buyDate', width: 150, valueFormatter: (data) => moment(data.value).format('lll'), suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },


            {
                headerName: "Actions",
                suppressMenu: true,
                suppressSorting: true,
                template:
                    `<button type="button" data-action-type="Edit" class="btn btn-link btn-xs">
              Edit
             </button>
             <button type="button" data-action-type="Delete" class="btn btn-link btn-xs">
              Delete
             </button>
             <button type="button" data-action-type="SerialNumber" class="btn btn-link btn-xs">
              Serial Number
             </button>`
            }
        ];
        this.addNewInventoryToList();
 

    }
    options(params) {
        if (params.data.remainingNumber > 0) {
           return (`<span style="background-color:red;color:white">Add Serial Number</span>`)
        }
        else {
            return (`<span style="color:black">COMPLETED</span>`);
        }

    }
    sold(params) {
        if (params.data.soldNumber == 0) {
            return (`<span>---------</span>`)
        }
        else {
            return (params.data.soldNumber);
        }

    }
    deployed(params) {
        if (params.data.deployNumber == 0) {
            return (`<span>---------</span>`)
        }
        else {
            return (params.data.deployNumber);
        }

    }
    maintenance(params) {
        if (params.data.maintenanceNumber == 0) {
            return (`<span>---------</span>`)
        }
        else {
            return (params.data.maintenanceNumber);
        }

    }
    notReceived(params) {
        if (params.data.notReceivedNumber == 0) {
            return (`<span>---------</span>`)
        }
        else {
            return (params.data.notReceivedNumber);
        }

    }
    damage(params) {
        if (params.data.damageNumber == 0) {
            return (`<span>---------</span>`)
        }
        else {
            return (params.data.damageNumber);
        }

    }
    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {
                case "Edit":
                    return this.onEdit(data);
                case "Delete":
                    return this.onDelete(data);
                case "SerialNumber":
                    return this.onSerialNumber(data);

            }
        }
    }
    onEdit(row: InventoryAddEdit) {
        this.editInventory(row);
    }
    onDelete(row: InventoryAddEdit) {
        this.deleteInventory(row);
    }
    onSerialNumber(row: InventoryAddEdit) {
        this.serialPage(row)
    }

    ngAfterViewInit() {

        this.inventoryAddEditor.changesSavedCallback = () => {
            this.addNewInventoryToList();
            this.editorModal.hide();
        };

        this.inventoryAddEditor.changesCancelledCallback = () => {
            this.editedInventory = null;
            this.sourceInventory = null;
            this.editorModal.hide();
        };
 
    }

    public setReturnValue(dateRange) {      
        let startDate = dateRange['from'];
        let endDate = dateRange['to'];
        let sDate = moment(startDate);      
        let eDate = moment(endDate);
        if (sDate != null && eDate!=null) {
            let result = this.rows.filter(function (inventoryAdd, index, inventorysAdd) {
                if (moment(inventoryAdd.buyDate) >= sDate && moment(inventoryAdd.buyDate) <= eDate) {
                    (<any>inventoryAdd).index = index + 1;
                    return inventoryAdd;
                }
                else {
                    return false;
                }
            });
            return this.rows = result;
        }
    }


    addNewInventoryToList() {
        if (this.sourceInventory) {
            Object.assign(this.sourceInventory, this.editedInventory);

            let sourceIndex = this.rowsCache.indexOf(this.sourceInventory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceInventory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedInventory = null;
            this.sourceInventory = null;
        }
        else {
            let inventoryAdd = new InventoryAdd();
            Object.assign(inventoryAdd, this.editedInventory);
            this.editedInventory = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>InventoryAdd).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, inventoryAdd);
            this.rows.splice(0, 0, inventoryAdd);
        }
        this.loadData()
    }



    loadData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.edited = false;

        this.inventoryAddService.getAddInventoryList().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
       
       
    }


    onDataLoadSuccessful(inventorys: InventoryAdd[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        inventorys.forEach((inventory, index, inventorys) => {
            (<any>inventory).index = index + 1;
        });
        this.rowsCache = [...inventorys];
        this.rows = inventorys;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.primaryItem, r.secondaryItem, r.tertiaryItem, r.detailItem));
    }

    onEditorModalHidden() {
        this.editingInventoryName = null;
        this.inventoryAddEditor.resetForm(true);
    }


    editInventory(row: InventoryAddEdit) {
        this.editingInventoryName = { name: row.primaryItem };
        this.sourceInventory = row;
        this.editedInventory = this.inventoryAddEditor.editInventory(row, this.allInventory);
        this.editorModal.show();
    }
    

    serialPage(row: InventoryAddEdit) {
        this.editingInventoryName = { name: row.primaryItem };
    
        this.sourceInventory = row;
        this.editedInventory = this.serialEditor.serialPage(row, this.allInventory);
        this.edited = true;
    }

   
    deleteInventory(row: InventoryAddEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.primaryItem + '\"?', DialogType.confirm, () => this.deleteInventoryHelper(row));
    }


    deleteInventoryHelper(row: InventoryAddEdit) {

        this.alertService.startLoadingMessage("Deleting...");   
        this.loadingIndicator = true;

        this.inventoryAddService.deleteAddInventory(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }




    get canViewRoles() {
        return this.inventoryAddService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.inventoryAddService.userHasPermission(Permission.manageUsersPermission);
    }
    get canSeeSerial() {
        return this.inventoryAddService.userHasPermission(Permission.manageUsersPermission);
    }

}
