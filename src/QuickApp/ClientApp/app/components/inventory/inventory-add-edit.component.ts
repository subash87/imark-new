﻿
import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { InventoryAddService } from "../../services/inventory-add.service";
import { Utilities } from '../../services/utilities';
import { InventoryAdd } from '../../models/inventory-add.model';
import { InventoryAddEdit } from '../../models/inventory-add-edit.model';
import { Permission } from '../../models/permission.model';
import { BootstrapDatepickerDirective } from '../../directives/bootstrap-datepicker.directive';

@Component({
    selector: 'inventory-add-edit',
    templateUrl: './inventory-add-edit.component.html',
    styleUrls: ['./inventory.component.css']
})
export class InventoryAddEditComponent implements OnInit {
    maxDate = new Date();
    private isEditMode = false;
    private isNewInventory = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingInventoryName: string;
    private uniqueId: string = Utilities.uniqueId();
    private inventoryAdd: InventoryAdd = new InventoryAdd();
    private allInventory: InventoryAdd[] = [];
    private inventoryAddEdit: InventoryAddEdit;


    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;





    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('primaryItem')
    private primaryItem;

    @ViewChild('secondaryItem')
    private SecondaryItem;

    @ViewChild('tertiaryItem')
    private secondaryItem;

    @ViewChild('detailItem')
    private detailItem;

    @ViewChild('hasSerialNo')
    private hasSerialNo;

    @ViewChild('totalNumber')
    private totalNumber;

    @ViewChild('buyDate')
    private buyDate;

    constructor(private alertService: AlertService, private inventoryAddService: InventoryAddService) {
    }

    ngOnInit() {
        if (!this.isGeneralEditor) {
            this.loadCurrentInventoryData();
        }
    }



    private loadCurrentInventoryData() {
        this.alertService.startLoadingMessage();

        if (this.canViewAllRoles) {
            this.inventoryAddService.getInventoryAdd().subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
        }
       
    }


    private onCurrentUserDataLoadSuccessful(inventoryAdd: InventoryAdd) {
        this.alertService.stopLoadingMessage();
        this.inventoryAdd = inventoryAdd;

    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.inventoryAdd = new InventoryAdd();
    }



    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }




    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.inventoryAddEdit = new InventoryAddEdit();
            Object.assign(this.inventoryAddEdit, this.inventoryAdd);
        }
        else {
            if (!this.inventoryAddEdit)
                this.inventoryAddEdit = new InventoryAddEdit();

            this.isEditingSelf = this.inventoryAddService.updateAddInventory(this.inventoryAddEdit)? this.inventoryAddEdit.id == this.inventoryAdd.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveInventory() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        if (this.isNewInventory) {
            this.inventoryAddService.newAddInventory(this.inventoryAddEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            this.inventoryAddService.updateAddInventory(this.inventoryAddEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(inventoryAdd?: InventoryAdd) {
        this.testIsInventoryChanged(this.inventoryAdd, this.inventoryAddEdit);

        if (inventoryAdd)
            Object.assign(this.inventoryAddEdit, inventoryAdd);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.inventoryAdd, this.inventoryAddEdit);
        this.inventoryAddEdit = new InventoryAddEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewInventory)
                this.alertService.showMessage("Success", `User \"${this.inventoryAdd.primaryItem}\" was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Changes to user \"${this.inventoryAdd.primaryItem}\" was saved successfully`, MessageSeverity.success);
        }

     
        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private testIsInventoryChanged(inventoryAdd: InventoryAdd, editedInventory: InventoryAdd) {

        //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
        //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);

        //let modifiedRoles = rolesAdded.concat(rolesRemoved);

        //if (modifiedRoles.length)
        //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    }



    private cancel() {
        if (this.isGeneralEditor)
            this.inventoryAddEdit = this.inventoryAdd = new InventoryAddEdit();
        else
            this.inventoryAddEdit = new InventoryAddEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.inventoryAddEdit = this.inventoryAdd = new InventoryAddEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newInventory(allInventory: InventoryAdd[]) {
        this.isGeneralEditor = true;
        this.isNewInventory = true;

        this.allInventory = [...allInventory];
        this.editingInventoryName = null;
        this.inventoryAdd = this.inventoryAddEdit = new InventoryAddEdit();
        this.edit();

        return this.inventoryAddEdit;
    }



    editInventory(inventoryAdd: InventoryAdd, allInventory: InventoryAdd[]) {
        if (inventoryAdd) {
            this.isGeneralEditor = true;
            this.isNewInventory = false;
            this.editingInventoryName = inventoryAdd.primaryItem;
            this.inventoryAdd = new InventoryAdd();
            this.inventoryAddEdit = new InventoryAddEdit();
            Object.assign(this.inventoryAdd, inventoryAdd);
            Object.assign(this.inventoryAddEdit, inventoryAdd);
            this.edit();

            return this.inventoryAddEdit;
        }
        else {
            return this.newInventory(allInventory);
        }
    }
    get canViewUsers() {
        return this.inventoryAddService.userHasPermission(Permission.viewUsersPermission);
    }


    get canViewAllRoles() {
        return this.inventoryAddService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.inventoryAddService.userHasPermission(Permission.assignRolesPermission);
    }
}
