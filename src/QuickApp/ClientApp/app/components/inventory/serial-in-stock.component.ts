﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DateRangePickerModule, IDateRange } from 'ng-pick-daterange';
import { Router } from '@angular/router';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { InventoryAddService } from "../../services/inventory-add.service";
import { Utilities } from "../../services/utilities";
import { User } from '../../models/user.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { InventoryAdd } from '../../models/inventory-add.model';
import { InventoryAddEdit } from '../../models/inventory-add-edit.model';
import { Inventory } from '../../models/inventory.model';
import { InventoryEdit } from '../../models/inventory-edit.model';
import { PosStockEditComponent } from '../stock/pos-stock-edit.component';
import { SerialListInStockComponent } from '../inventory/serial-list-in-stock.component';
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import * as moment from 'moment';
import * as $ from 'jquery';
import { PosStock } from '../../models/posStock/pos-stock.model';





@Component({
    selector: 'serial-in-stock',
    templateUrl: './serial-in-stock.component.html',
    styleUrls: ['./inventory.component.css']
})


export class SerialInStockComponent implements OnInit, AfterViewInit {
    columnDefs: any[] = [];
    bankColDefs: any[] = [];
    columns: any[] = [];
    rows: Inventory[] = [];
    rowsCache: Inventory[] = [];
    rowsSold: PosStock[] = [];
    rowsSoldCache: PosStock[] = [];
    editedInventory: InventoryEdit;
    sourceInventory: InventoryEdit;
    editingInventoryName: {};
    loadingIndicator: boolean;
    inventoryAdd: Inventory = new Inventory();
    allInventory: Inventory[] = [];
    public edited: boolean;
    public datefilter: boolean;
    private gridApi;
    private gridColumnApi;
    private showSold = false;
    private showStock = true;
    @Input()
    isGeneralEditor = false;


    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('editorHistoryModal')
    editorHistoryModal: ModalDirective;

    //@ViewChild('serialListEditor')
    //serialListEditor: PosStockEditComponent;

    @ViewChild('serialListEditor')
    serialListEditor: SerialListInStockComponent;


    constructor(private alertService: AlertService, private router: Router, private translationService: AppTranslationService, private inventoryAddService: InventoryAddService) {
    }


    ngOnInit() {
        this.columnDefs = [

            { headerName: 'Terminal Type', field: 'item', width: 400, suppressSizeToFit: true },
            { headerName: 'Available', field: 'available', width: 200, suppressSizeToFit: true },
     

        ];

        if (this.canCreateUsers)
            this.columnDefs.push({
                headerName: "Actions",
                suppressMenu: true,
                field: '',
                suppressSorting: true,
                width: 250,

                cellRenderer: this.canViewUsers,
                template:
                    `<button type="button" data-action-type="AddStock" class="btn btn-link btn-xs"style="background-color:maroon;color:white;text-decoration:none">
              In Stock
             </button>`
            });


        this.bankColDefs = [

            { headerName: 'Bank Name', field: 'bankName', width: 150, suppressSizeToFit: true },
            { headerName: 'Terminal Type', field: 'terminalType', width: 180, suppressSizeToFit: true },
            { headerName: 'Serial Number', field: 'serialNumber', width: 150, suppressSizeToFit: true },
            { headerName: 'Remarks', field: 'remarks', width: 150, suppressSizeToFit: true },
            { headerName: 'Created By', field: 'createdBy', width: 150, suppressSizeToFit: true },
            {
                headerName: 'Sold Date', field: 'stockDate', valueFormatter: (data) => moment(data.value).format('lll'), width: 180, suppressSizeToFit: true, filter: 'date',
                filterParams: {
                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                        //using moment js
                        var dateAsString = moment(cellValue).format('DD/MM/YYYY');
                        var dateParts = dateAsString.split("/");
                        var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]));

                        if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                            return 0
                        }

                        if (cellDate < filterLocalDateAtMidnight) {
                            return -1;
                        }

                        if (cellDate > filterLocalDateAtMidnight) {
                            return 1;
                        }
                    }
                }
            },


        ];


        this.addNewInventoryToList();
        this.loadSoldData();


    }
    public onRowClicked(e) {
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute("data-action-type");

            switch (actionType) {
                case "AddStock":
                    return this.StockClick(data);
              

            }
        }
    }
    StockClick(row: Inventory) {
        this.loadingIndicator = true;

        this.editedInventory = this.serialListEditor.loadSerialList(this.allInventory, row.id, row);
        this.editorModal.show();
    }
   

    public item(params) {
        return (params.data.primaryItem + " - " + params.data.secondaryItem + " - " + params.data.tertiaryItem + " - " + params.data.detailItem);
    }
    //public remaining(params) {
    //    if (params.data.remainingNumber==0) {
    //        return (`<span style="background-color:red;color:white">STOCK-OUT<span/>`);
    //    }
    //    else {
    //        return(params.data.remainingNumber)
    //    }
    //}
    public onBtExport() {

        this.gridApi.exportDataAsCsv();
    }
    onGridReady(params) {



        this.inventoryAddService.getPosStockInfo().subscribe(data => {
            params.api.setRowData(data);

        });
    }
    onGridSoldHistoryReady(params) {
        this.gridApi = params.api;


        this.inventoryAddService.getPosStockHistoryInfo().subscribe(data => {
            params.api.setRowData(data);

        });
    }

    showSoldHistory() {
        this.showSold = true
        this.showStock = false
    }
    hideSoldHistory() {
        this.showSold = false
        this.showStock = true
    }

    ngAfterViewInit() {


        this.serialListEditor.changesPosStockSavedCallback = () => {
            this.addNewInventoryToList();
            this.editorModal.hide();
        };

        this.serialListEditor.changesCancelledCallback = () => {
            this.editedInventory = null;
            this.sourceInventory = null;
            this.editorModal.hide();
        };

    }


    addNewInventoryToList() {
        if (this.sourceInventory) {
            Object.assign(this.sourceInventory, this.editedInventory);

            let sourceIndex = this.rowsCache.indexOf(this.sourceInventory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceInventory, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedInventory = null;
            this.sourceInventory = null;
        }
        else {
            let inventoryAdd = new Inventory();
            Object.assign(inventoryAdd, this.editedInventory);
            this.editedInventory = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>Inventory).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, inventoryAdd);
            this.rows.splice(0, 0, inventoryAdd);
        }
        this.loadData();
    }



    loadData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.edited = false;

        this.inventoryAddService.getPosStockInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));


    }


    onDataLoadSuccessful(inventorys: Inventory[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        inventorys.forEach((inventory, index, inventorys) => {
            (<any>inventory).index = index + 1;
        });
        this.rowsCache = [...inventorys];
        this.rows = inventorys;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }

    loadSoldData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.edited = false;

        this.inventoryAddService.getPosStockHistoryInfo().subscribe(results => this.onSoldDataLoadSuccessful(results), error => this.onSoldDataLoadFailed(error));


    }


    onSoldDataLoadSuccessful(soldPos: PosStock[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        soldPos.forEach((sold, index, soldPos) => {
            (<any>sold).index = index + 1;
        });
        this.rowsSoldCache = [...soldPos];
        this.rowsSold = soldPos;
    }


    onSoldDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }

    onSearchChanged(value: string) {
        this.rowsSold = this.rowsSoldCache.filter(r => Utilities.searchArray(value, false, r.bankName, r.remarks, r.createdBy, r.serialNumber, r.item));
    }

    onEditorModalHidden() {
        this.serialListEditor.resetForm(true);
        //this.editorHistoryModal.hide();
    }
    onEditorStockModalHidden() {
        this.editorHistoryModal.hide();
    }


    get canCreateUsers() {
        return this.inventoryAddService.userHasPermission(Permission.createUsersPermission);
    }

    get canViewUsers() {
        return this.inventoryAddService.userHasPermission(Permission.viewUsersPermission);
    }

    get canViewRoles() {
        return this.inventoryAddService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.inventoryAddService.userHasPermission(Permission.manageUsersPermission);
    }
    get canSeeSerial() {
        return this.inventoryAddService.userHasPermission(Permission.manageUsersPermission);
    }

}
