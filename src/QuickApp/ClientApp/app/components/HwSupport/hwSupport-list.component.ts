﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { HwSupportService } from "../../services/hwSupport/hwSupport.service";
import { Utilities } from "../../services/utilities";
import { Permission } from '../../models/permission.model';
import { Support } from '../../models/support/support.model';
import { SupportEdit } from '../../models/support/support-edit.model';
import { HwSupportEditComponent } from "./hwSupport-edit.component";
//import { HwSupportListComponent } from "./hwSupport-list.component";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import { Subject } from 'rxjs/Subject';
import { HwSupportEdit } from '../../models/hwSupport/hwSupport-edit.model';
import { HwSupport } from '../../models/hwSupport/hwSupport.model';


@Component({
    selector: 'hwSupport-list',
    templateUrl: './hwSupport-list.component.html',
    styleUrls: ['./support.component.css']
})
export class HwSupportListComponent implements OnInit, AfterViewInit {
    columnDefs: any[] = [];
    columns: any[] = [];
    rows: HwSupport[] = [];
    rowsCache: HwSupport[] = [];

    editedSupport: HwSupportEdit;
    sourceSupport: HwSupportEdit;
    editingSupportName: {};
    loadingIndicator: boolean;
    private supportEdit: SupportEdit = new SupportEdit();
    private support: Support = new Support();
    allSupport: HwSupport[] = [];

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('bankNameTemplate')
    bankNameTemplate: TemplateRef<any>;

    @ViewChild('contactPersonTemplate')
    contactPersonTemplate: TemplateRef<any>;

    @ViewChild('contactNoTemplate')
    contactNoTemplate: TemplateRef<any>;
        
    @ViewChild('issueTemplate')
    issueTemplate: TemplateRef<any>;

    @ViewChild('resolvedByTemplate')
    resolvedByTemplate: TemplateRef<any>;

    @ViewChild('supportDateTemplate')
    supportDateTemplate: TemplateRef<any>;

    @ViewChild('verifiedByTemplate')
    verifiedByTemplate: TemplateRef<any>;

    @ViewChild('verifiedDateTemplate')
    verifiedDateTemplate: TemplateRef<any>;

    @ViewChild('phoneTemplate')
    phoneTemplate: TemplateRef<any>;

    @ViewChild('fieldTemplate')
    fieldTemplate: TemplateRef<any>;

    @ViewChild('statusTemplate')
    statusTemplate: TemplateRef<any>;

    @ViewChild('priorityTemplate')
    priorityTemplate: TemplateRef<any>;

    @ViewChild('remarksTemplate')
    remarksTemplate: TemplateRef<any>;

    @ViewChild('verificationTemplate')
    verificationTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('supportEditor')
    supportEditor: HwSupportEditComponent;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private hwSupportService: HwSupportService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        this.columns = [
            { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'bankName', name: "Bank Name", width: 100, cellTemplate: this.bankNameTemplate },
            { prop: 'contactPerson', name: "Contact Person", width: 140, cellTemplate: this.contactPersonTemplate },
            { prop: 'contactNo', name: "Contact No", width: 140, cellTemplate: this.contactNoTemplate },
            { prop: 'issue', name: "Issue", width: 140, cellTemplate: this.issueTemplate },
            { prop: 'phone', name: "Phone", width: 140, cellTemplate: this.phoneTemplate },
            { prop: 'field', name: "Field", width: 140, cellTemplate: this.fieldTemplate },
            { prop: 'priority', name: "Priority", width: 140, cellTemplate: this.priorityTemplate },
            { prop: 'remarks', name: "Remarks", width: 140, cellTemplate: this.remarksTemplate },
            { prop: 'resolvedBy', name: "Resolved By", width: 140, cellTemplate: this.resolvedByTemplate },
            { prop: 'supportDate', name: "Support Date", width: 140, cellTemplate: this.supportDateTemplate },
            { prop: 'verifiedBy', name: "Verified By", width: 140, cellTemplate: this.verifiedByTemplate },
            { prop: 'verifiedDate', name: "Verified Date", width: 140, cellTemplate: this.verifiedDateTemplate },
        ];

        if (this.canManageUsers)
            this.columns.push({ name: '', width: 130, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });

        this.loadSupportData();


    }

    ngAfterViewInit() {

        this.supportEditor.changesSavedCallback = () => {
            this.addNewSupportToList();
            this.editorModal.hide();
        };

        this.supportEditor.changesCancelledCallback = () => {
            this.editedSupport = null;
            this.sourceSupport = null;
            this.editorModal.hide();
        };

    }


    addNewSupportToList() {
        //if (this.sourceSupport) {
        //    Object.assign(this.sourceSupport, this.editedSupport);

        //    let sourceIndex = this.rowsCache.indexOf(this.sourceSupport, 0);
        //    if (sourceIndex > -1)
        //        Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

        //    sourceIndex = this.rows.indexOf(this.sourceSupport, 0);
        //    if (sourceIndex > -1)
        //        Utilities.moveArrayItem(this.rows, sourceIndex, 0);

        //    this.editedSupport = null;
        //    this.sourceSupport = null;
        //}
        //else {
        //    let support = new Support();
        //    Object.assign(support, this.editedSupport);
        //    this.editedSupport = null;

        //    let maxIndex = 0;
        //    for (let u of this.rowsCache) {
        //        if ((<any>u).index > maxIndex)
        //            maxIndex = (<any>u).index;
        //    }

        //    (<any>Support).index = maxIndex + 1;

        //    this.rowsCache.splice(0, 0, support);
        //    this.rows.splice(0, 0, support);
        //}
        //this.loadSupportData();
    }


    loadSupportData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.hwSupportService.getSupportInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(supports: HwSupport[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        supports.forEach((support, index, supports) => {

            (<any>support).index = index + 1;
        });
        this.rowsCache = [...supports];
        this.rows = supports;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.contactPerson, r.resolvedBy, r.remarks, r.verifiedBy, r.status));
    }
   

    onEditorModalHidden() {
        this.editingSupportName = null;
        this.supportEditor.resetForm(true);
    }


    ///check point----------
    editSupport(row: HwSupportEdit) {
   
        this.sourceSupport = row;
        this.editedSupport = this.supportEditor.editSupportList(row, this.allSupport);
        this.editorModal.show();
    }


    deleteSupport(row: HwSupportEdit) {
        this.alertService.showDialog('Are you sure you want to delete this item??', DialogType.confirm, () => this.deleteSupportHelper(row));
    }


    deleteSupportHelper(row: HwSupportEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.hwSupportService.deleteSupport(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }



    get canViewUsers() {
        return this.hwSupportService.userHasPermission(Permission.viewUsersPermission);
    }

    get canViewRoles() {
        return this.hwSupportService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.hwSupportService.userHasPermission(Permission.manageUsersPermission);
    }

}
