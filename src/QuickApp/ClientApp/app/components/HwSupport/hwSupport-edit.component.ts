﻿import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { AlertService, MessageSeverity } from '../../services/alert.service';
import { HwSupportService } from "../../services/hwSupport/hwSupport.service";
import { Utilities } from '../../services/utilities';
import { Support } from '../../models/support/support.model';
import { SupportEdit } from '../../models/support/support-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { HardwareSupportEdit } from '../../models/hardwareSupport/hardware-support-edit.model';
import { HwSupport } from '../../models/hwSupport/hwSupport.model';
import { HwSupportEdit } from '../../models/hwSupport/hwSupport-edit.model';

@Component({
    selector: 'support-hw',
    templateUrl: './hwSupport-edit.component.html',
    styleUrls: ['./support.component.css']
})
export class HwSupportEditComponent implements OnInit {

    private isEditMode = false;
    private isNewSupport = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingSupportName: string;
    private uniqueId: string = Utilities.uniqueId();
    private hwSupport: HwSupport = new HwSupport();
    private allSupport: HwSupport[] = [];
    private hwSupportEdit: HwSupportEdit = new HwSupportEdit();
 

    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('phone')
    private phone;

    @ViewChild('contactPerson')
    private contactPerson;

    @ViewChild('contactNo')
    private contactNo;

    @ViewChild('field')
    private field;

    @ViewChild('status')
    private status;

    @ViewChild('priority')
    private priority;

    
    @ViewChild('issue')
    private issue;

    @ViewChild('resolvedBy')
    private resolvedBy;

    @ViewChild('remarks')
    private remarks;

    @ViewChild('deploymentRequestId')
    private deploymentRequestId;

    constructor(private alertService: AlertService, private hwSupportService: HwSupportService) {
    }

    ngOnInit() {
        if (!this.isGeneralEditor) {
            this.loadCurrentSupportData();
        }
    }



    private loadCurrentSupportData() {
        this.alertService.startLoadingMessage();

        if (this.canViewAllRoles) {
            this.hwSupportService.getSupport().subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
        }
     
    }


    private onCurrentUserDataLoadSuccessful(hwSupport: HwSupport) {
        this.alertService.stopLoadingMessage();
        this.hwSupport = hwSupport;
      
    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to save support to the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.hwSupport = new HwSupport();
    }


    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }

   

    private edit(id) {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.hwSupportEdit.hwSupportId = id;
            //this.supportEdit = new SupportEdit();
            Object.assign(this.hwSupportEdit, this.hwSupport);
        }
        else {
            if (!this.hwSupportEdit) {}
                //this.supportEdit = new SupportEdit();
            this.hwSupportEdit.hwSupportId = id;
            this.isEditingSelf = this.hwSupportService.updateSupport(this.hwSupport) ? this.hwSupportEdit.id == this.hwSupport.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }

    private edited() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            //this.supportEdit = new SupportEdit();
            Object.assign(this.hwSupportEdit, this.hwSupport);
        }
        else {
            if (!this.hwSupportEdit) { }
            //this.supportEdit = new SupportEdit();
            this.hwSupportEdit.verification = "Verified";
            this.isEditingSelf = this.hwSupportService.updateSupport(this.hwSupport) ? this.hwSupportEdit.id == this.hwSupport.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveSupport() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");
      
        if (this.isNewSupport) {
           
            this.hwSupportService.newSupport(this.hwSupportEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            this.hwSupportEdit.verification = "Verified";
            this.hwSupportService.updateSupport(this.hwSupportEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(support?: HwSupport) {
        
        if (support)
            Object.assign(this.hwSupportEdit, support);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.hwSupport, this.hwSupportEdit);
        this.hwSupportEdit = new HwSupportEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewSupport)
                this.alertService.showMessage("Success", `User \"${this.hwSupport.id}\" was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Changes to user \"${this.hwSupport.id}\" was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }

    
    
    private cancel() {
       
        if (this.isGeneralEditor)
            this.hwSupportEdit = this.hwSupport = new HwSupportEdit();
        else
            this.hwSupportEdit = new HwSupportEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        
        this.hwSupportEdit = this.hwSupport = new HwSupportEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    //newHwSupport(allSupport: HwSupport[],id) {
    //    this.isNewSupport = true;
    //    this.isEditMode = true;
    //    return this.hwSupportEdit;
    //}

    newHwSupport(allSupport: HwSupport[],id) {
        this.isGeneralEditor = true;
        this.isNewSupport = true;
      
        this.hwSupportEdit.id = id;
        this.allSupport = [...allSupport];        
        this.hwSupport = this.hwSupportEdit = new HwSupportEdit();
        this.edit(id);

        return this.hwSupportEdit;
    }

    newSupportList(allSupport: HwSupport[]) {
        this.isGeneralEditor = true;
        this.isNewSupport = true;
        this.allSupport = [...allSupport];
        //this.editingSupportName = null;
        this.hwSupport = this.hwSupportEdit = new HwSupportEdit();
        //this.posEdit.isEnabled = true;
        this.edited();

        return this.hwSupportEdit;
    }

    editSupport(support: Support, allSupport: Support[],id) {
        //if (support) {
        //    this.isGeneralEditor = true;
        //    this.isNewSupport = false;
        //    this.supportEdit.deploymentRequestId = id;
        //    this.editingSupportName = support.id;
        //    this.support = new Support();
        //    this.supportEdit = new SupportEdit();
        //    Object.assign(this.support, support);
        //    Object.assign(this.supportEdit, support);
        //    this.edit(id);

        //    return this.supportEdit;
        //}
        //else {
        //    return this.newSupport(allSupport,id);
        //}
    }
    editSupportList(support: HwSupport, allSupport: HwSupport[]) {
        if (support) {
            this.isGeneralEditor = true;
            this.isNewSupport = false;
            this.editingSupportName = support.id;
            this.hwSupport = new HwSupport();
            this.hwSupportEdit = new HwSupportEdit();
            Object.assign(this.hwSupport, support);
            Object.assign(this.hwSupportEdit, support);
            this.edited();

            return this.hwSupportEdit;
        }
        else {
            return this.newSupportList(allSupport);
        }
    }


 


    get canViewAllRoles() {
        return this.hwSupportService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.hwSupportService.userHasPermission(Permission.assignRolesPermission);
    }
}
