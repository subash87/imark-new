﻿

import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { SupportService } from "../../services/support/support.service";
import { Utilities } from '../../services/utilities';
import { Support } from '../../models/support/support.model';
import { SupportEdit } from '../../models/support/support-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';


@Component({
    selector: 'support-edit',
    templateUrl: './support-edit.component.html',
    styleUrls: ['./support.component.css']
})
export class SupportEditComponent implements OnInit {

    private isEditMode = false;
    private isNewSupport = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingSupportName: string;
    private uniqueId: string = Utilities.uniqueId();
    private support: Support = new Support();
    private allSupport: Support[] = [];
    private supportEdit: SupportEdit = new SupportEdit();
 

    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('phone')
    private phone;

    @ViewChild('contactPerson')
    private contactPerson;

    @ViewChild('contactNo')
    private contactNo;

    @ViewChild('field')
    private field;

    @ViewChild('status')
    private status;

    @ViewChild('priority')
    private priority;

    
    @ViewChild('issue')
    private issue;

    @ViewChild('resolvedBy')
    private resolvedBy;

    @ViewChild('remarks')
    private remarks;

    @ViewChild('deploymentRequestId')
    private deploymentRequestId;

    constructor(private alertService: AlertService, private supportService: SupportService) {
    }

    ngOnInit() {
        if (!this.isGeneralEditor) {
           // this.loadCurrentSupportData();
        }
    }



    private loadCurrentSupportData() {
        this.alertService.startLoadingMessage();

        if (this.canViewAllRoles) {
            this.supportService.getSupport().subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
        }
     
    }


    private onCurrentUserDataLoadSuccessful(support: Support) {
        this.alertService.stopLoadingMessage();
        this.support = support;
      
    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to save support to the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.support = new Support();
    }


    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }

   

    private edit(id) {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.supportEdit.deploymentRequestId = id;
            //this.supportEdit = new SupportEdit();
            Object.assign(this.supportEdit, this.support);
        }
        else {
            if (!this.supportEdit) {}
                //this.supportEdit = new SupportEdit();
                this.supportEdit.deploymentRequestId = id;
            this.isEditingSelf = this.supportService.updateSupport(this.support) ? this.supportEdit.id == this.support.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }

    private edited() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            //this.supportEdit = new SupportEdit();
            Object.assign(this.supportEdit, this.support);
        }
        else {
            if (!this.supportEdit) { }
            //this.supportEdit = new SupportEdit();
            this.supportEdit.verification = "Verified";
            this.isEditingSelf = this.supportService.updateSupport(this.support) ? this.supportEdit.id == this.support.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveSupport() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");
      
        if (this.isNewSupport) {
           
            this.supportService.newSupport(this.supportEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            this.supportEdit.verification = "Verified";
            this.supportService.updateSupport(this.supportEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(support?: Support) {
        //this.testIsSupportChanged(this.support, this.supportEdit);

        if (support)
            Object.assign(this.supportEdit, support);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.support, this.supportEdit);
        this.supportEdit = new SupportEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewSupport)
                this.alertService.showMessage("Success", `Support was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Changes to user \"${this.support.bankName}\" was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    //private testIsSupportChanged(support: Support, editedSupport: Support) {

    //    //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
    //    //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);

    //    //let modifiedRoles = rolesAdded.concat(rolesRemoved);

    //    //if (modifiedRoles.length)
    //    //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    //}



    private cancel() {
        if (this.isGeneralEditor)
            this.supportEdit = this.support = new SupportEdit();
        else
            this.supportEdit = new SupportEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.supportEdit = this.support = new SupportEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }

    newSupport(allSupport: Support[],id) {
        this.isGeneralEditor = true;
        this.isNewSupport = true;
      
        this.supportEdit.deploymentRequestId = id;
        this.allSupport = [...allSupport];
        
        this.support = this.supportEdit = new SupportEdit();
       this.edit(id);

        return this.supportEdit;
    }

    newSupportList(allSupport: Support[]) {
        this.isGeneralEditor = true;
        this.isNewSupport = true;
        this.allSupport = [...allSupport];
        //this.editingSupportName = null;
        this.support = this.supportEdit = new SupportEdit();
        //this.posEdit.isEnabled = true;
        this.edited();

        return this.supportEdit;
    }

    editSupport(support: Support, allSupport: Support[],id) {
        if (support) {
            this.isGeneralEditor = true;
            this.isNewSupport = false;
            this.supportEdit.deploymentRequestId = id;
            this.editingSupportName = support.id;
            this.support = new Support();
            this.supportEdit = new SupportEdit();
            Object.assign(this.support, support);
            Object.assign(this.supportEdit, support);
            this.edit(id);

            return this.supportEdit;
        }
        else {
            return this.newSupport(allSupport,id);
        }
    }
    editSupportList(support: Support, allSupport: Support[]) {
        if (support) {
            this.isGeneralEditor = true;
            this.isNewSupport = false;
            this.editingSupportName = support.id;
            this.support = new Support();
            this.supportEdit = new SupportEdit();
            Object.assign(this.support, support);
            Object.assign(this.supportEdit, support);
            this.edited();

            return this.supportEdit;
        }
        else {
            return this.newSupportList(allSupport);
        }
    }


 


    get canViewAllRoles() {
        return this.supportService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.supportService.userHasPermission(Permission.assignRolesPermission);
    }
}
