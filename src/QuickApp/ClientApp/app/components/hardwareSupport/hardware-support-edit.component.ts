﻿

import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { HardwareSupportService } from "../../services/hardwareSupport/hardware-support.service";
import { Utilities } from '../../services/utilities';
import { HardwareSupport } from '../../models/hardwareSupport/hardware-support.model';
import { HardwareSupportEdit } from '../../models/hardwareSupport/hardware-support-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { Bank } from '../../models/bank/bank.model';
import { BankService } from "../../services/bank/bank.service";
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, FormControl, ReactiveFormsModule } from '@angular/forms'

@Component({
    selector: 'hardware-support-edit',
    templateUrl: './hardware-support-edit.component.html',
    styleUrls: ['./hardware-support.component.css']
})
export class HardwareSupportEditComponent implements OnInit {
    rows: Bank[] = [];
    private isEditMode = false;
    private isNewHardwareSupport = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingHardwareSupportName: string;
    private uniqueId: string = Utilities.uniqueId();
    private hardwareSupport: HardwareSupport = new HardwareSupport();
    private hardwareSupportEdit: HardwareSupportEdit = new HardwareSupportEdit();
    private allHardwareSupport: HardwareSupport[] = [];
    //private hardwareSupportEdit: HardwareSupportEdit;
    bankControl = new FormControl('', [Validators.required]);
    private bank: Bank = new Bank();
    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('primaryItem')
    private primaryItem;

    @ViewChild('secondaryItem')
    private SecondaryItem;

    @ViewChild('tertiaryItem')
    private tertiaryItem;

    @ViewChild('detailItem')
    private detailItem;

    @ViewChild('serialNo')
    private serialNo;

    @ViewChild('purchaseDate')
    purchaseDate;

    @ViewChild('manufacturer')
    manufacturer;

    @ViewChild('contactPerson')
    contactPerson;

    @ViewChild('contactNo')
    contactNo;

    @ViewChild('bankId')
    bankId;

    @ViewChild('bankName')
    bankName;

    constructor(private alertService: AlertService, private hardwareSupportService: HardwareSupportService, private bankService: BankService) {
    }

    ngOnInit() {
        if (!this.isGeneralEditor) {
            this.loadCurrentHardwareSupportData();
        }
        this.loadCurrentBankData();
    }

    ///To populate Bank Names in drop down list
    private loadCurrentBankData() {
        this.bankService.getBankInfo().subscribe(results => this.onBankDataLoadSuccessful(results), error => this.onBankDataLoadFailed(error));
    }


    private onBankDataLoadSuccessful(banks: Bank[]) {
        banks.forEach((bank, index, banks) => {
            (<any>bank).index = index + 1;
        });
        this.rows = banks;
    }

    private onBankDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.bank = new Bank();
    }


    private loadCurrentHardwareSupportData() {
        this.alertService.startLoadingMessage();

        if (this.canViewAllRoles) {
            this.hardwareSupportService.getHardwareSupport().subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
        }

    }


    private onCurrentUserDataLoadSuccessful(hardwareSupport: HardwareSupport) {
        this.alertService.stopLoadingMessage();
        this.hardwareSupport = hardwareSupport;

    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to save hardwareSupport to the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.hardwareSupport = new HardwareSupport();
    }


    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }



    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.hardwareSupportEdit = new HardwareSupportEdit();
            Object.assign(this.hardwareSupportEdit, this.hardwareSupport);
        }
        else {
            if (!this.hardwareSupportEdit)
                this.hardwareSupportEdit = new HardwareSupportEdit();

            this.isEditingSelf = this.hardwareSupportService.updateHardwareSupport(this.hardwareSupport) ? this.hardwareSupportEdit.id == this.hardwareSupport.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveHardwareSupport() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");
        //this.hardwareSupportEdit.hasSerialNo = true;

        if (this.isNewHardwareSupport) {
            this.hardwareSupportService.newHardwareSupport(this.hardwareSupportEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            this.hardwareSupportService.updateHardwareSupport(this.hardwareSupportEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(hardwareSupport?: HardwareSupport) {
        this.testIsHardwareSupportChanged(this.hardwareSupport, this.hardwareSupportEdit);

        if (hardwareSupport)
            Object.assign(this.hardwareSupportEdit, hardwareSupport);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.hardwareSupport, this.hardwareSupportEdit);
        this.hardwareSupportEdit = new HardwareSupportEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewHardwareSupport)
                this.alertService.showMessage("Success", `User \"${this.hardwareSupport.primaryItem}\" was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Changes to user \"${this.hardwareSupport.primaryItem}\" was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private testIsHardwareSupportChanged(hardwareSupport: HardwareSupport, editedHardwareSupport: HardwareSupport) {

        //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
        //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);

        //let modifiedRoles = rolesAdded.concat(rolesRemoved);

        //if (modifiedRoles.length)
        //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    }



    private cancel() {
        if (this.isGeneralEditor)
            this.hardwareSupportEdit = this.hardwareSupport = new HardwareSupportEdit();
        else
            this.hardwareSupportEdit = new HardwareSupportEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.hardwareSupportEdit = this.hardwareSupport = new HardwareSupportEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newHardwareSupport(allHardwareSupport: HardwareSupport[]) {
        this.isGeneralEditor = true;
        this.isNewHardwareSupport = true;

        this.allHardwareSupport = [...allHardwareSupport];
        this.editingHardwareSupportName = null;
        this.hardwareSupport = this.hardwareSupportEdit = new HardwareSupportEdit();
        this.edit();

        return this.hardwareSupportEdit;
    }

    editHardwareSupport(hardwareSupport: HardwareSupport, allHardwareSupport: HardwareSupport[]) {
        if (hardwareSupport) {
            this.isGeneralEditor = true;
            this.isNewHardwareSupport = false;
            this.editingHardwareSupportName = hardwareSupport.primaryItem;
            this.hardwareSupport = new HardwareSupport();
            this.hardwareSupportEdit = new HardwareSupportEdit();
            Object.assign(this.hardwareSupport, hardwareSupport);
            Object.assign(this.hardwareSupportEdit, hardwareSupport);
            this.edit();

            return this.hardwareSupportEdit;
        }
        else {
            return this.newHardwareSupport(allHardwareSupport);
        }
    }





    get canViewAllRoles() {
        return this.hardwareSupportService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.hardwareSupportService.userHasPermission(Permission.assignRolesPermission);
    }
}
