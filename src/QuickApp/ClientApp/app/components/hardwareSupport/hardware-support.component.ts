﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { HardwareSupportService } from "../../services/hardwareSupport/hardware-support.service";
import { Utilities } from "../../services/utilities";
import { Permission } from '../../models/permission.model';
import { HardwareSupport } from '../../models/hardwareSupport/hardware-support.model';
import { HardwareSupportEdit } from '../../models/hardwareSupport/hardware-support-edit.model';
import { HardwareSupportEditComponent } from "./hardware-support-edit.component";
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import { HwSupport } from '../../models/hwSupport/hwSupport.model';
import { HwSupportEdit } from '../../models/hwSupport/hwSupport-edit.model';
import { HwSupportComponent } from "../../components/hwSupport/hwSupport.component";
import { HwSupportEditComponent } from "../../components/hwSupport/hwSupport-edit.component";

@Component({
    selector: 'hardware-support-management',
    templateUrl: './hardware-support.component.html',
    styleUrls: ['./hardware-support.component.css']
})
export class HardwareSupportComponent implements OnInit, AfterViewInit {
    columns: any[] = [];
    rows: HardwareSupport[] = [];
    rowsCache: HardwareSupport[] = [];
    editingHardwareSupportName: {};

    editedHardwareSupport: HardwareSupportEdit;
    sourceHardwareSupport: HardwareSupportEdit;
    
    allSupport: HwSupport[] = [];
    editedHwSupport: HwSupportEdit;
    sourceHwSupport: HwSupportEdit;
        
    hwEditSupportName: {};

    loadingIndicator: boolean;
    hardwareSupport: HardwareSupport = new HardwareSupport();
   // hwEditSupport: HwSupportEdit = new HwSupportEdit();

    allHardwareSupport: HardwareSupport[] = [];

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('primaryItemTemplate')
    primaryItemTemplate: TemplateRef<any>;

    @ViewChild('bankTemplate')
    bankTemplate: TemplateRef<any>;

    //@ViewChild('secondaryItemTemplate')
    //secondaryItemTemplate: TemplateRef<any>;

    //@ViewChild('tertiaryItemTemplate')
    //tertiaryItemTemplate: TemplateRef<any>;

    //@ViewChild('detailItemTemplate')
    //detailItemTemplate: TemplateRef<any>;

    //@ViewChild('serialNoTemplate')
    //serialNoTemplate: TemplateRef<any>;

    @ViewChild('purchaseDateTemplate')
    purchaseDateTemplate: TemplateRef<any>;

    @ViewChild('manufacturerTemplate')
    manufacturerTemplate: TemplateRef<any>;

    @ViewChild('contactPersonTemplate')
    contactPersonTemplate: TemplateRef<any>;

    @ViewChild('contactNoTemplate')
    contactNoTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('hardwareSupportEditor')
    hardwareSupportEditor: HardwareSupportEditComponent;

    @ViewChild('HwSupportEditor')
    HwSupportEditor: HwSupportEditComponent;

    @ViewChild('hwSupportListEditor')
    hwSupportListEditor: HwSupportComponent;

    @ViewChild('hwSupportModal')
    hwSupportModal: ModalDirective;

   
    constructor(private alertService: AlertService, private translationService: AppTranslationService, private hardwareSupportService: HardwareSupportService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        this.columns = [
            { prop: "index", name: '#', width: 15, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'primaryItem', name: gT('hardwareSupport.management.PrimaryItem'), width: 69, cellTemplate: this.primaryItemTemplate },
            { prop: 'bankName', name: gT('Bank Name'), width: 69, cellTemplate: this.bankTemplate },
            //{ prop: 'secondaryItem', name: gT('hardwareSupport.management.SecondaryItem'), width: 64, cellTemplate: this.secondaryItemTemplate },
            //{ prop: 'tertiaryItem', name: gT('hardwareSupport.management.TertiaryItem'), width: 70, cellTemplate: this.tertiaryItemTemplate },
            //{ prop: 'detailItem', name: gT('hardwareSupport.management.DetailItem'), width: 72, cellTemplate: this.detailItemTemplate },
            //{ prop: 'serialNo', name: gT('hardwareSupport.management.SerialNo'), width: 65, cellTemplate: this.serialNoTemplate },
            { prop: 'purchaseDate', name: gT('hardwareSupport.management.PurchaseDate'), width: 97, cellTemplate: this.purchaseDateTemplate },
            { prop: 'manufacturer', name: gT('hardwareSupport.management.Manufacturer'), width: 90, cellTemplate: this.manufacturerTemplate },
            { prop: 'contactPerson', name: gT('hardwareSupport.management.ContactPerson'), width: 101, cellTemplate: this.contactPersonTemplate },
            { prop: 'contactNo', name: gT('hardwareSupport.management.ContactNo'), width: 76, cellTemplate: this.contactNoTemplate },

        ];

        if (this.canManageUsers)
            this.columns.push({ name: '', width: 200, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });


        this.addNewHardwareSupportToList();

    }


    ngAfterViewInit() {

        this.hardwareSupportEditor.changesSavedCallback = () => {
            this.addNewHardwareSupportToList();
            this.editorModal.hide();
        };

        this.hardwareSupportEditor.changesCancelledCallback = () => {
            this.editedHardwareSupport = null;
            this.sourceHardwareSupport = null;
            this.editorModal.hide();
        };

        this.HwSupportEditor.changesSavedCallback = () => {
            this.addNewHardwareSupportToList();
            this.hwSupportModal.hide();
        };

        this.HwSupportEditor.changesCancelledCallback = () => {
            this.editedHardwareSupport = null;
            this.sourceHardwareSupport = null;
            this.hwSupportModal.hide();
        };

    }

    
    public hwSupport(data: HwSupportEdit) {
              
        this.editedHwSupport = this.HwSupportEditor.newHwSupport(this.allSupport, data.id);     
        this.loadSupportList(data.id);
        this.hwSupportModal.show();        
    }

    public loadSupportList(id) {

        this.editedHwSupport = this.hwSupportListEditor.loadSupportList(this.allSupport, id);
    }

    addNewHardwareSupportToList() {
        if (this.sourceHardwareSupport) {
            Object.assign(this.sourceHardwareSupport, this.editedHardwareSupport);

            let sourceIndex = this.rowsCache.indexOf(this.sourceHardwareSupport, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceHardwareSupport, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedHardwareSupport = null;
            this.sourceHardwareSupport = null;
        }
        else {
            let hardwareSupport = new HardwareSupport();
            Object.assign(hardwareSupport, this.editedHardwareSupport);
            this.editedHardwareSupport = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>HardwareSupport).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, hardwareSupport);
            this.rows.splice(0, 0, hardwareSupport);
        }
        this.loadHardwareSupportData();
    }


    loadHardwareSupportData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.hardwareSupportService.getHardwareSupportInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
        //this.hardwareSupport.serialNo = true;
        //this.inventoryService.getInventoryInfo(-1, -1, this.inventory.hasSerialNo).subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(hardwareSupports: HardwareSupport[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        hardwareSupports.forEach((hardwareSupport, index, hardwareSupports) => {
            (<any>HardwareSupport).index = index + 1;
        });
        this.rowsCache = [...hardwareSupports];
        this.rows = hardwareSupports;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.primaryItem, r.secondaryItem, r.tertiaryItem, r.detailItem));
    }

    onEditorModalHidden() {
        this.editingHardwareSupportName = null;
        this.hardwareSupportEditor.resetForm(true);
    }
    onHwModalHidden() {
        this.editingHardwareSupportName = null;
        this.HwSupportEditor.resetForm(true);
    }

    //row: HardwareSupportEdit
    newHardwareSupport() {
        this.editingHardwareSupportName = null;
        this.sourceHardwareSupport = null;
        this.editedHardwareSupport = this.hardwareSupportEditor.newHardwareSupport(this.allHardwareSupport);
        this.editorModal.show();
    }


    editHardwareSupport(row: HardwareSupportEdit) {
        this.editingHardwareSupportName = { name: row.primaryItem };
        this.sourceHardwareSupport = row;
        this.editedHardwareSupport = this.hardwareSupportEditor.editHardwareSupport(row, this.allHardwareSupport);
        this.editorModal.show();
    }

   
    
    deleteHardwareSupport(row: HardwareSupportEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.primaryItem + '\"?', DialogType.confirm, () => this.deleteHardwareSupportHelper(row));
    }


    deleteHardwareSupportHelper(row: HardwareSupportEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.hardwareSupportService.deleteHardwareSupport(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
                error => {
                    this.alertService.stopLoadingMessage();
                    this.loadingIndicator = false;

                    this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                        MessageSeverity.error, error);
                });
    }


    get canCreateUsers() {
        return this.hardwareSupportService.userHasPermission(Permission.createUsersPermission);
    }

    get canViewUsers() {
        return this.hardwareSupportService.userHasPermission(Permission.viewUsersPermission);
    }

    get canViewRoles() {
        return this.hardwareSupportService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.hardwareSupportService.userHasPermission(Permission.manageUsersPermission);
    }

}
