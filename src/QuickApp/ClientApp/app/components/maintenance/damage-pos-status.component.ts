﻿import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { PosHistoryService } from "../../services/poshistory/pos-history.service";
import { Utilities } from '../../services/utilities';
import { DamagePos } from '../../models/configuration/configuration.model';
import { DamagePosEdit } from '../../models/configuration/configuration-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';


@Component({
    selector: 'damage-pos-status',
    templateUrl: './damage-pos-status.component.html',
    styleUrls: ['./maintenance.component.css']
})
export class AddDamagePosComponent implements OnInit {

    private isEditMode = false;
    private isNewDamage = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingDeployName: string;
    private uniqueId: string = Utilities.uniqueId();
    private damagePos: DamagePos = new DamagePos();
    private allDamage: DamagePos[] = [];
    private damagePosEdit: DamagePosEdit = new DamagePosEdit();


    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;
    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;




    @ViewChild('status')
    private status;



    constructor(private alertService: AlertService, private damagePosService: PosHistoryService) {
    }

    ngOnInit() {
      
       
    }



    

    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.damagePosEdit = new DamagePosEdit();
            Object.assign(this.damagePosEdit, this.damagePos);
        }
        else {
            if (!this.damagePosEdit)
                this.damagePosEdit = new DamagePosEdit();

            this.isEditingSelf = this.damagePosService.damagePos(this.damagePos) ? this.damagePosEdit.id == this.damagePos.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveDamagePos() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");


        this.damagePosService.damagePos(this.damagePosEdit, this.damagePosEdit.deploymentRequestId).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));

    }


    private saveSuccessHelper(damage?: DamagePos) {


        if (damage)
            Object.assign(this.damagePosEdit, damage);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.damagePos, this.damagePosEdit);
        this.damagePosEdit = new DamagePosEdit();
        this.resetForm();


        if (this.isGeneralEditor) {

            this.alertService.showMessage("Success", `Damage Pos  \"${this.damagePos.status}\" was created successfully`, MessageSeverity.success);
         
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }




    private cancel() {
        if (this.isGeneralEditor)
            this.damagePosEdit = this.damagePos = new DamagePosEdit();
        else
            this.damagePosEdit = new DamagePosEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.damagePosEdit = this.damagePos = new DamagePosEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    //newDamage(allDeploy: Deploy[]) {
    //    this.isGeneralEditor = true;
    //    this.isNewDeploy = true;

    //    this.allDeploy = [...allDeploy];
    //    this.editingDeployName = null;
    //    this.deploy = this.deployEdit = new DeployEdit();
    //    //this.posEdit.isEnabled = true;
    //    this.edit();

    //    return this.deployEdit;
    //}

    addDamageStatus(damage: DamagePos, id) {
        //if (deploy) {
            this.isGeneralEditor = true;
            //this.isNewDeploy = false;
        //this.editingDeployName = deploy.receivedBy;
        this.damagePos = new DamagePos();
        this.damagePosEdit = new DamagePosEdit();
        Object.assign(this.damagePos, damage);
        Object.assign(this.damagePosEdit, damage);
            this.edit();

        return this.damagePosEdit;
        //}
        //else {
        //    return this.newDeploy(allDeploy);
        //}
    }





    get canViewAllRoles() {
        return this.damagePosService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.damagePosService.userHasPermission(Permission.assignRolesPermission);
    }
}
