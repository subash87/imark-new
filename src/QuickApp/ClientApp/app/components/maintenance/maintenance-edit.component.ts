﻿

import { Component, OnInit, ViewChild, Input, TemplateRef } from '@angular/core';
import { FormBuilder, FormControl, FormArray, FormGroup, Validators } from '@angular/forms';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { MaintenanceService } from "../../services/maintenance/maintenance.service";
import { Utilities } from '../../services/utilities';
import { Maintenance } from '../../models/maintenance/maintenance.model';
import { MaintenanceEdit } from '../../models/maintenance/maintenance-edit.model';
import { MaintenanceViewModel } from '../../models/maintenance/maintenanceViewModel.model';
import { MaintenanceViewModelEdit } from '../../models/maintenance/maintenanceView-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import * as moment from 'moment';

@Component({
    selector: 'maintenance-edit',
    templateUrl: './maintenance-edit.component.html',
    styleUrls: ['./maintenance.component.css']
})
export class MaintenanceEditComponent implements OnInit {
    columnDefs: any[] = [];


    columns: any[] = [];
    rows: MaintenanceViewModel[] = [];
    rowsCache: MaintenanceViewModel[] = [];

    maintainStatus = [
        { value: 1, text: 'Under Repair' },
        { value: 2, text: 'Repaired' },
        //{ value: 3, text: 'Faulty' },
        { value: 4, text: 'Terminate'}
    ];
    maintainVerify = [
        { value: 1, text: 'Pending' },
        { value: 2, text: 'Verify' },
    ];
    private isEditMode = false;
    private isNewMaintenance = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingMaintenanceName: string;
    private mainId: string;
    private uniqueId: string = Utilities.uniqueId();
    private maintenance: Maintenance = new Maintenance();
    private allMaintenance: Maintenance[] = [];
    private maintenanceEdit: MaintenanceEdit = new MaintenanceEdit();
    private maintenanceViewModel: MaintenanceViewModel = new MaintenanceViewModel();
    editedIssue: MaintenanceViewModelEdit;
    sourceIssue: MaintenanceViewModelEdit;
    private invoiceForm: FormGroup;
    //itemRows: MaintenanceViewModel[] = [];
    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('serialNumberId')
    private serialNumberId;

    @ViewChild('serialKey')
    private serialKey;

    @ViewChild('status')
    private status;

    @ViewChild('resolvedBy')
    private resolvedBy;

    @ViewChild('remarks')
    private remarks;

    @ViewChild('verification')
    private verification;

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('issueTemplate')
    issueTemplate: TemplateRef<any>;

    @ViewChild('remarksTemplate')
    remarksTemplate: TemplateRef<any>;

    @ViewChild('createdByTemplate')
    createdByTemplate: TemplateRef<any>;

    @ViewChild('createdDateTemplate')
    createdDateTemplate: TemplateRef<any>;

    constructor(private alertService: AlertService, private maintenanceService: MaintenanceService, private _fb: FormBuilder) {
    }

    ngOnInit() {
        this.columnDefs = [
            //{ headerName: 'Status', field: 'status', suppressSizeToFit: true },
            //{ headerName: 'Resolved By', field: 'resolvedBy', suppressSizeToFit: true, cellRenderer:this.item },
            //{ headerName: 'Terminate', field: 'terminate', suppressSizeToFit: true, cellRenderer: this.item1  },
            { headerName: 'Issue', field: 'issue', suppressSizeToFit: true },
            { headerName: 'Remarks', field: 'remarks', suppressSizeToFit: true },
            { headerName: 'Created By', field: 'createdBy', suppressSizeToFit: true },
            
            {
                headerName: 'Created Date', field: 'createdDate', valueFormatter: (data) => moment(data.value).format('L'), suppressSizeToFit: true

            },

        ];


        this.addMaintenanceToList();
        this.invoiceForm = this._fb.group({

            //serialNumberId: [],
            //serialKey: [],
            //deploymentRequestId: [],
            status: ['', Validators.required],
            terminate: [''],
            resolvedBy: [''],
            //issue:[],
            //verifiedBy: [],
            //verifiedDate: [],
            //verification:[],
            remarks:[''],
            itemRows: this._fb.array([this.initItemRows()])
        });
        this.loadCurrentMaintenanceData();
    }

    //public item(params) {
    //    if (params.data.resolvedBy == null) {
    //        return (`<span style="background-color:red;color:white">NOT RESOLVED YET</span>`);
    //    }
       
    //    else {
    //        return (params.data.resolvedBy);
    //    }
    //}
    //public item1(params) {
    //    if (params.data.terminate == null) {
    //        return (`<span style="background-color:red;color:white">NOT TERMINATED YET</span>`);
    //    }

    //    else {
    //        return (params.data.terminate);
    //    }
    //}

    initItemRows(): FormGroup  {
        return this._fb.group({
            // list all your form controls here, which belongs to your form array
            issue: [''],
            remarks: [''],
   
        });
    }
    addNewRow() {
        // control refers to your form array
        const control = <FormArray>this.invoiceForm.controls['itemRows'];
        //add new formgroup
        control.push(this.initItemRows());
    }
    saveItemRows() {
        this.maintenanceEdit = this.invoiceForm.getRawValue();
        
    }
    deleteRow(index: number) {
        // control refers to your formarray
        const control = <FormArray>this.invoiceForm.controls['itemRows'];
        // remove the chosen row
        control.removeAt(index);
    }

    addMaintenanceToList() {
        if (this.sourceIssue) {
            Object.assign(this.sourceIssue, this.editedIssue);

            let sourceIndex = this.rowsCache.indexOf(this.sourceIssue, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceIssue, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedIssue = null;
            this.sourceIssue = null;
        }
        else {
            let issueAdd = new MaintenanceViewModel();
            Object.assign(issueAdd, this.editedIssue);
            this.editedIssue = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>issueAdd).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, issueAdd);
            this.rows.splice(0, 0, issueAdd);
        }
        this.loadCurrentMaintenanceData();
    }


    private loadCurrentMaintenanceData() {
        this.alertService.startLoadingMessage();

            this.maintenanceService.getMaintenanceIssueInfo(-1, -1, this.mainId).subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
        

    }


    private onCurrentUserDataLoadSuccessful(issues: MaintenanceViewModel[]) {
        this.alertService.stopLoadingMessage();
       

        issues.forEach((issue, index, issues) => {
            (<any>issue).index = index + 1;
        });
        this.rowsCache = [...issues];
        this.rows = issues;

    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to save inventory to the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.maintenanceViewModel = new MaintenanceViewModel();
    }

    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.issue, r.remarks, r.createdBy));
    }
    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }



    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.maintenanceEdit = new MaintenanceEdit();
            Object.assign(this.maintenanceEdit, this.maintenance);
        }
        else {
            if (!this.maintenanceEdit)
                //this.maintenanceEdit = new MaintenanceEdit();
            //this.maintenanceEdit = this.invoiceForm.getRawValue();
                this.isEditingSelf = this.maintenanceService.updateMaintenance(this.maintenance) ? this.maintenanceEdit.id == this.mainId: false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private save() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        const rawValue = this.invoiceForm.getRawValue();
        this.maintenanceEdit.itemRow = Object.assign({}, rawValue);
        this.maintenanceEdit.status = rawValue['status'];
        this.maintenanceEdit.resolvedBy = rawValue['resolvedBy'];
        this.maintenanceEdit.remarks = rawValue['remarks'];
        this.maintenanceEdit.terminate = rawValue['terminate'];
        this.maintenanceService.updateMaintenance(this.maintenanceEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        
    }


    private saveSuccessHelper(maintenance?: Maintenance) {
        this.testIsMaintenanceChanged(this.maintenance, this.maintenanceEdit);

        if (maintenance)
            Object.assign(this.maintenanceEdit, maintenance);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.maintenance, this.maintenanceEdit);
        this.maintenanceEdit = new MaintenanceEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewMaintenance)
                this.alertService.showMessage("Success", ` \"${this.maintenance.status}\" was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", ` \"${this.maintenance.status}\" was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private testIsMaintenanceChanged(maintenance: Maintenance, editedMaintenance: Maintenance) {

        //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
        //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);

        //let modifiedRoles = rolesAdded.concat(rolesRemoved);

        //if (modifiedRoles.length)
        //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    }



    private cancel() {
        if (this.isGeneralEditor)
            this.maintenanceEdit = this.maintenance = new MaintenanceEdit();
        else
            this.maintenanceEdit = new MaintenanceEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.maintenanceEdit = this.maintenance = new MaintenanceEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newMaintenance(allMaintenance: Maintenance[]) {
        this.isGeneralEditor = true;
        this.isNewMaintenance = true;

        this.allMaintenance = [...allMaintenance];
        this.editingMaintenanceName = null;
        this.maintenance = this.maintenanceEdit = new MaintenanceEdit();
        //this.posEdit.isEnabled = true;
        this.edit();

        return this.maintenanceEdit;
    }

    editMaintenance(maintenance: Maintenance, allMaintenance: Maintenance[],id) {
        if (maintenance) {
            this.isGeneralEditor = true;
            this.isNewMaintenance = false;
            this.editingMaintenanceName = maintenance.id;
            this.mainId = id;
            this.maintenance = new Maintenance();
            this.maintenanceEdit = new MaintenanceEdit();
            Object.assign(this.maintenance, maintenance);
            Object.assign(this.maintenanceEdit, maintenance);
            this.loadCurrentMaintenanceData();
            this.edit();
          
            return this.maintenanceEdit;
        }
        else {
            return this.newMaintenance(allMaintenance);
        }
    }

    get canViewAllRoles() {
        return this.maintenanceService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.maintenanceService.userHasPermission(Permission.assignRolesPermission);
    }
}
