﻿import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { ConfigurationsService } from "../../services/configuration/configuration.service";

import { Utilities } from "../../services/utilities";
import { Permission } from '../../models/permission.model';
import { Configuration } from '../../models/configuration/configuration.model'
import { ConfigurationEdit } from '../../models/configuration/configuration-edit.model';
import { SideBarComponent } from '../../components/sidebar/sidebar.component';
import { ConfigurationEditComponent } from "./configuration-edit.component";



@Component({
    selector: 'configuration-management',
    templateUrl: './configuration.component.html',
    styleUrls: ['./configuration.component.css']
})
export class ConfigurationsComponent implements OnInit, AfterViewInit {
    columns: any[] = [];
    rows: Configuration[] = [];
    rowsCache: Configuration[] = [];

    editedConfiguration: ConfigurationEdit;
    sourceConfiguration: ConfigurationEdit;
    editingConfigurationName: {};
    loadingIndicator: boolean;

    allConfiguration: Configuration[] = [];

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('deploymentIdTemplate')
    deploymentIdTemplate: TemplateRef<any>;

    @ViewChild('checkerIdTemplate')
    checkerIdTemplate: TemplateRef<any>;

    @ViewChild('serialNumberIdTemplate')
    serialNumberIdTemplate: TemplateRef<any>;

    @ViewChild('applicationDateTemplate')
    applicationDateTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('configurationEditor')
    configurationEditor: ConfigurationEditComponent;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private configurationService: ConfigurationsService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        this.columns = [
            { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'deploymentId', name: gT('configuration.management.DeploymentId'), width: 50, cellTemplate: this.deploymentIdTemplate },
            { prop: 'checkerId', name: gT('configuration.management.CheckerId'), width: 90, cellTemplate: this.deploymentIdTemplate },
            { prop: 'serialNumberId', name: gT('configuration.management.SerialNumberId'), width: 120, cellTemplate: this.serialNumberIdTemplate},
            { prop: 'applicationDate', name: gT('configuration.management.ApplicationDate'), width: 140, cellTemplate: this.applicationDateTemplate },
        ];
       
        if (this.canManageUsers)
            this.columns.push({ name: '', width: 130, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });

       

        this.loadConfigurationData();
    }


    ngAfterViewInit() {

        this.configurationEditor.changesConfigSavedCallback = () => {
            this.addNewConfigurationToList();
            this.editorModal.hide();
        };

        this.configurationEditor.changesCancelledCallback = () => {
            this.editedConfiguration = null;
            this.sourceConfiguration = null;
            this.editorModal.hide();
        };
    
    }


    addNewConfigurationToList() {
        if (this.sourceConfiguration) {
            Object.assign(this.sourceConfiguration, this.editedConfiguration);

            let sourceIndex = this.rowsCache.indexOf(this.sourceConfiguration, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceConfiguration, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedConfiguration = null;
            this.sourceConfiguration = null;
        }
        else {
            let configuration = new Configuration();
            Object.assign(configuration, this.editedConfiguration);
            this.editedConfiguration = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>Configuration).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, configuration);
            this.rows.splice(0, 0, configuration);
        }
    }


    loadConfigurationData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.configurationService.getConfigurationInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
        
    }


    onDataLoadSuccessful(configurations: Configuration[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        configurations.forEach((configuration, index, configurations) => {
            (<any>configuration).index = index + 1;
        });
        this.rowsCache = [...configurations];
        this.rows = configurations;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }

   

    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.applicationVersionId, r.serialNumberId, r.deploymentRequestId));
    }

    onEditorModalHidden() {
        this.editingConfigurationName = null;
        this.configurationEditor.resetForm(true);
    }


    newConfiguration() {
        this.editingConfigurationName = null;
        this.sourceConfiguration = null;
        this.editedConfiguration = this.configurationEditor.newConfiguration(this.allConfiguration);
        this.editorModal.show();
    }


    editConfiguration(row: ConfigurationEdit) {
        this.editingConfigurationName = { name: row.id };
        this.sourceConfiguration = row;
        this.editedConfiguration = this.configurationEditor.editConfiguration(row, this.allConfiguration);
        this.editorModal.show();
    }


    deleteConfiguration(row: ConfigurationEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.id + '\"?', DialogType.confirm, () => this.deleteConfigurationHelper(row));
    }


    deleteConfigurationHelper(row: ConfigurationEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.configurationService.deleteConfiguration(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }



   
    get canViewRoles() {
        return this.configurationService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.configurationService.userHasPermission(Permission.manageUsersPermission);
    }
   
}
