﻿import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { AlertService, MessageSeverity } from '../../services/alert.service';
import { ConfigurationsService } from "../../services/configuration/configuration.service";
import { Utilities } from '../../services/utilities';
import { Configuration } from '../../models/configuration/configuration.model';
import { ConfigurationEdit } from '../../models/configuration/configuration-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';




@Component({
    selector: 'add-configuration-status',
    templateUrl: './add-configuration-status.component.html',
    styleUrls: ['./configuration.component.css']
})
export class AddSatusConfigurationComponent implements OnInit {

    private isEditMode = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingDeployName: string;
    private uniqueId: string = Utilities.uniqueId();
    private configuration: Configuration = new Configuration();
    private allConfiguration: Configuration[] = [];
    private configurationEdit: ConfigurationEdit = new ConfigurationEdit();


    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;
    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;




    @ViewChild('status')
    private status;



    constructor(private alertService: AlertService, private configService: ConfigurationsService) {
    }

    ngOnInit() {


    }





    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.configurationEdit = new ConfigurationEdit();
            Object.assign(this.configurationEdit, this.configuration);
        }
        else {
            if (!this.configurationEdit)
                this.configurationEdit = new ConfigurationEdit();
            this.configurationEdit.status = null;
            this.isEditingSelf = this.configService.updateConfigurationStatus(this.configuration) ? this.configurationEdit.id == this.configuration.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }
    private saveStatus() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        this.configService.updateConfigurationStatus(this.configurationEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));

    }


    private saveSuccessHelper(config?: Configuration) {

        Object.assign(this.configurationEdit, config);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        //this.showValidationErrors = false;
        Object.assign(this.configuration, this.configurationEdit);
        this.configurationEdit = new ConfigurationEdit();
        this.resetForm();



        this.alertService.showMessage("Success", `Changes to user \"${this.configuration.status}\" was saved successfully`, MessageSeverity.success);

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }





    private cancel() {
        if (this.isGeneralEditor)
            this.configurationEdit = this.configuration = new ConfigurationEdit();
        else
            this.configurationEdit = new ConfigurationEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.configurationEdit = this.configuration = new ConfigurationEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    //newDamage(allDeploy: Deploy[]) {
    //    this.isGeneralEditor = true;
    //    this.isNewDeploy = true;

    //    this.allDeploy = [...allDeploy];
    //    this.editingDeployName = null;
    //    this.deploy = this.deployEdit = new DeployEdit();
    //    //this.posEdit.isEnabled = true;
    //    this.edit();

    //    return this.deployEdit;
    //}

    addConfigStatus(config: Configuration, allPos: Configuration[]) {
        //if (deploy) {
        this.isGeneralEditor = true;
        //this.isNewDeploy = false;


        this.configuration = new Configuration();
        this.configurationEdit = new ConfigurationEdit();
        Object.assign(this.configuration, config);
        Object.assign(this.configurationEdit, config);

        this.edit();

        return this.configurationEdit;
        //}
        //else {
        //    return this.newDeploy(allDeploy);
        //}
    }





    get canViewAllRoles() {
        return this.configService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.configService.userHasPermission(Permission.assignRolesPermission);
    }
}
