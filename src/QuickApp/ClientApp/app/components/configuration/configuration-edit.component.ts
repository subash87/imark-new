﻿
import { Component, AfterViewInit, OnInit, ViewChild, TemplateRef, Input, OnDestroy  } from '@angular/core';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { ConfigurationsService } from "../../services/configuration/configuration.service";
import { InventoryAddService } from "../../services/inventory-add.service";
import { ApplicationService } from "../../services/applicationVersion/application.service";
import { SerialNoService } from "../../services/serial-no.service";
import { PosHistoryService } from "../../services/poshistory/pos-history.service";
import { Utilities } from '../../services/utilities';
import { Configuration } from '../../models/configuration/configuration.model';
import { ConfigurationEdit } from '../../models/configuration/configuration-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';
import { InventoryAdd } from '../../models/inventory-add.model';
import { Application } from '../../models/applicationVersion/application.model';
import { InventoryAddEdit } from '../../models/inventory-add-edit.model';
import { SerialAddComponent } from "../inventory/serial-no.component";
import { Checker } from '../../models/checker/checker.model';
import { SerialNo } from '../../models/serial-no.model';
import { SerialNoEdit } from '../../models/serial-no-edit.model';
import { Maintenance } from '../../models/maintenance/maintenance.model';
import { MaintenanceEdit } from '../../models/maintenance/maintenance-edit.model';
import { MaintenanceService } from '../../services/maintenance/maintenance.service';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, FormControl, ReactiveFormsModule } from '@angular/forms'
import { PosHistoryEdit } from '../../models/posHistory/pos-edit.model';
import { PosHistory } from "../../models/posHistory/pos-history.model";
import { AddSatusConfigurationComponent } from "./add-configuration-status.component";

@Component({
    selector: 'configuration-edit',
    templateUrl: './configuration-edit.component.html',
    styleUrls: ['./configuration.component.css']
})
export class ConfigurationEditComponent implements OnInit {
    rows: InventoryAdd[] = [];
    rows1: Application[] = [];
    columns: any[] = [];
    rowsSerialCache: SerialNo[] = [];
    rowsSerial: SerialNo[] = [];

    verify = [
        { value: 1, text: 'Pending' },
        { value: 2, text: 'Verified' },
        //{ value: 3, text: 'Rejected' },
    ];
    received = [
        { value: 1, text: 'Received' },
        { value: 2, text: 'Not Received' },
    ];
    private serialRef = null;
    private selectedLink: string = "no";  
    private selectedLink1: string = "yes";  
    private selectedLink2: string = "yes";
    private selectedMaintenance: string = "no";
    private isReceived: string;
    private isEditMode = false;
    private isNewConfiguration = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private statusFlag = false;
    private stockFlag = false;
    private maintenanceFlag = false;
    private noMaintenanceFlag = false;
    private serialFlag = true;
    private showSerialNo = false;
    private editingConfigurationName: string;
    private uniqueId: string = Utilities.uniqueId();
    private configuration: Configuration = new Configuration();
    private application: Application = new Application();
    private inventoryAdd: InventoryAdd = new InventoryAdd();
    private serialEdit: SerialNoEdit = new SerialNoEdit();
    private serial: SerialNo = new SerialNo();
    private maintenanceEdit: MaintenanceEdit = new MaintenanceEdit();
    private maintenance: Maintenance = new Maintenance();
    //private inventorys: Inventory = new Inventory();
    private serialAdd: SerialNo;
    editedSerial: SerialNoEdit;
    private serialId: string;
    private allConfiguration: Configuration[] = [];
    private configurationEdit: ConfigurationEdit = new ConfigurationEdit();
    loadingIndicator: boolean;

    allInventory: InventoryAdd[] = [];
    allSerial: SerialNo[] = [];
    editedInventory: InventoryAddEdit;
    sourceInventory: InventoryAddEdit;

    editedStatusConfig: ConfigurationEdit;

    applicationControl = new FormControl('', [Validators.required]);
    verificationControl = new FormControl('', [Validators.required]);
    public formResetToggle = true;

    public selectedSNo = false;

    @ViewChild('configStatusModal')
    configStatusModal: ModalDirective;

    @ViewChild('configStatusEditor')
    configStatusEditor: AddSatusConfigurationComponent;

    public changesConfigSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;
   
    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;
    @Input()
    edited = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('deploymentId')
    private deploymentId;

    @ViewChild('checkerId')
    private checkerId;

    @ViewChild('inventoryAddId')
    private inventoryAddId;

    @ViewChild('serialKey')
    private serialKey;



    @ViewChild('status')
    private status;

    @ViewChild('applicationId')
    private applicationId;

    @ViewChild('serialNumberId')
    private serialNumberId;

    @ViewChild('verificationItem')
    private verification;

    @ViewChild('serialEditor')
    serialEditor: SerialAddComponent;
    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;
    @ViewChild('serialKeyTemplate')
    serialKeyTemplate: TemplateRef<any>;
    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private configurationService: ConfigurationsService,
        private inventoryAddService: InventoryAddService, private applicationService: ApplicationService, private serialService: SerialNoService, private maintenanceService: MaintenanceService) {
    }

    ngOnInit() {
       
        if (!this.isGeneralEditor) {
            this.loadCurrentConfigurationData();
            this.loadSerialData();
        }
        //this.loadCurrentApplicationData();
        this.loadSerialData();
    }

    ngAfterViewInit() {



        this.configStatusEditor.changesSavedCallback = () => {
            this.configStatusModal.hide();
        }
        this.configStatusEditor.changesCancelledCallback = () => {

            this.configStatusModal.hide();
        }
        //this.maintenanceStatusEditor.changesSavedCallback = () => {
        //    this.addNewNotReceivedToList();
        //    this.maintenanceStatusModal.hide();
        //}
        //this.maintenanceStatusEditor.changesCancelledCallback = () => {
        //    //this.editedMaintenance = null;
        //    this.maintenanceStatusModal.hide();
        //}

    }

    setradio(e: string): void {

        this.selectedLink = e;

    }

    isSelected(name: string): boolean {

        if (!this.selectedLink) { // if no radio button is selected, always return false so every nothing is shown  
            return false;
        }

        return (this.selectedLink === name); // if current radio button is selected, return true, else return false  
    }   
    setReceived(e: string): void {

        this.selectedLink1 = e;
        //!this.isSelectedserialNo("push");
        this.isReceived = e;
    }
    setNotReceived(e: string): void {

        this.selectedLink1 = e;
        //!this.isSelectedserialNo("push");
        this.notReceived(this.configuration);

    }

    isSelectedReceived(name: string): boolean {

        if (!this.selectedLink1) {  
            return false;
        }

        return (this.selectedLink1 === name);
    }   
    isSelectedNotReceived(name: string): boolean {

        if (!this.selectedLink1) {
            return false;
        }

        return (this.selectedLink1 === name);
    }  
    setSerialNo(e: string): void {

        this.selectedLink2 = e;

    }
    setMaintenance(e: string): void {

        this.selectedMaintenance = e;

    }
    isMaintenance(name: string): boolean {

        if (!this.selectedMaintenance) { // if no radio button is selected, always return false so every nothing is shown  
            return false;
        }
       
        return (this.selectedMaintenance === name); // if current radio button is selected, return true, else return false  
    }   
    setStatus(e: string): void {

        this.selectedMaintenance = e;

    }
    isSetStatus(name: string): boolean {

        if (!this.selectedMaintenance) { // if no radio button is selected, always return false so every nothing is shown  
            return false;
        }

        return (this.selectedMaintenance === name); // if current radio button is selected, return true, else return false  
    }   

    isSelectedserialNo(name: string): boolean {

        if (!this.selectedLink2) {
            return false;
        }

        return (this.selectedLink2 === name);
    }   

    private loadCurrentConfigurationData() {
        this.alertService.startLoadingMessage();

        if (this.canViewAllRoles) {
            this.configurationService.getConfiguration().subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
         
        }
      
     
    }


    private onCurrentUserDataLoadSuccessful(configuration: Configuration) {
        this.alertService.stopLoadingMessage();
        this.configurationEdit = configuration;
      
    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.configuration = new Configuration();
    }


    //private loadCurrentInventoryAddData(page, pageSize, deploymentRequestId ) {
    //    this.inventoryAddService.getAddInventoryInfo(page,pageSize, deploymentRequestId).subscribe(results => this.onInventoryAddDataLoadSuccessful(results), error => this.onInventoryAddDataLoadFailed(error));
    //}


    //private onInventoryAddDataLoadSuccessful(inventorysAdd: InventoryAdd[]) {
    //    inventorysAdd.forEach((inventoryAdd, index, inventorysAdd) => {
    //        (<any>inventoryAdd).index = index + 1;
    //    });
    //    this.allInventory = inventorysAdd;
    //}

    //private onInventoryAddDataLoadFailed(error: any) {
    //    this.alertService.stopLoadingMessage();
    //    this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
    //        MessageSeverity.error, error);

    //    this.inventoryAdd = new InventoryAdd();
    //}
    public loadSerialData() {
     
        this.serialService.getSerialInfo(this.serialId ,'config').subscribe(results => this.onSerialDataLoadSuccessful(results), error => this.onSerialDataDataLoadFailed(error));
    }


    private onSerialDataLoadSuccessful(serials: SerialNo[]) {
        serials.forEach((ser, index, serials) => {
            (<any>ser).index = index + 1;
        });
        this.rowsSerialCache = [...serials];
        this.rowsSerial = serials;
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
       
        let gT = (key: string) => this.translationService.getTranslation(key);
        this.columns = [
            { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'serialKey', name: gT('inventory.management.SerialNumber'), width: 150, cellTemplate: this.serialKeyTemplate },
        ];

        if (this.canManageUsers)
            this.columns.push({ name: 'Action', width: 130, cellTemplate: this.actionsTemplate, resizeable: false, canAutoResize: false, sortable: false, draggable: false });
        
    }

    private onSerialDataDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve Serial Keys from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
       
        this.serial = new SerialNo();
    }
    loadSerialList(allSerial: SerialNo[], id) {
        //this.isGeneralEditor = true;
        this.configurationEdit.deploymentRequestId = id;
        this.serialId = id;
        this.allSerial = [...allSerial];
        this.serial = this.serialEdit = new SerialNoEdit();
        this.loadSerialData();
        this.loadCurrentApplicationData(id);
        return this.serialEdit;
    }

    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }

    private loadCurrentApplicationData(id) {
        this.applicationService.getApplicationWithBankInfo(id,-1,-1).subscribe(results => this.onApplicationLoadSuccessful(results), error => this.onApplicationFailed(error));
    }

    private onApplicationLoadSuccessful(applications: Application[]) {
        applications.forEach((application, index, applications) => {
            (<any>application).index = index + 1;
        });
        this.rows1 = applications;
    }

    private onApplicationFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.application = new Application();
    }
    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.configurationEdit = new ConfigurationEdit();
            Object.assign(this.configurationEdit, this.configuration);
            //if (this.maintenanceFlag == true || this.statusFlag == true || this.serialFlag == true ) {
            //    this.configurationEdit.serialKey = null;
            //}
        }
        else {
            if (!this.configurationEdit) 
                this.configurationEdit = new ConfigurationEdit();
            //if (this.configurationEdit && this.hideSerialNo == true)
            //    this.configurationEdit.status = null;
            //if (this.configurationEdit && (this.maintenanceFlag == true || this.statusFlag == true || this.stockFlag == true ))
            //    this.configurationEdit.serialKey = null;
            //if (this.configurationEdit && this.maintenanceFlag == true || this.statusFlag == true || this.showSerialNo == false) {
            //    this.configurationEdit.serialNumberId = null;
            //    this.configurationEdit.serialKey = null;
            //}
            this.isEditingSelf = this.configurationService.updateConfiguration(this.configuration) ? this.configurationEdit.id == this.configuration.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }

    private saveConfiguration() {
        
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

      
        if (this.isNewConfiguration) {
            this.configurationService.newConfiguration(this.configurationEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            this.configurationService.updateConfiguration(this.configurationEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(configuration?: ConfigurationEdit) {
        //this.testIsConfigurationChanged(this.configuration, this.configurationEdit);

        if (configuration)
            Object.assign(this.configurationEdit, configuration);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.showValidationErrors = false;
        Object.assign(this.configuration, this.configurationEdit);
        this.configurationEdit = new ConfigurationEdit();
        this.configuration = new Configuration();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewConfiguration)
                this.alertService.showMessage("Success", `User \"${this.configuration.serialKey}\" has been added successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Changes to user \"${this.configuration.serialKey}\" has been added successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesConfigSavedCallback)
            this.changesConfigSavedCallback();
        
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        if (this.isNewConfiguration) {
            this.alertService.showStickyMessage("Save Error", "Dublicate Serial Number/ Please Select a Serial Number", MessageSeverity.error, error);
            this.alertService.showStickyMessage(error, null, MessageSeverity.error);
        }
        this.alertService.showStickyMessage("Save Error", "Dublicate Serial Number/ Please Select a Serial Number", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }


    //private testIsConfigurationChanged(configuration: Configuration, editedConfiguration: Configuration) {

    //    //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
    //    //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);

    //    //let modifiedRoles = rolesAdded.concat(rolesRemoved);

    //    //if (modifiedRoles.length)
    //    //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    //}

    //serialPage(row: InventoryAddEdit) {
        
    //    this.sourceInventory = row;
    //    this.editedInventory = this.serialEditor.serialPage(row, this.allInventory);
    //    //this.edited = true;
    //}

   

    private cancel() {
        if (this.isGeneralEditor)
            this.configurationEdit = this.configuration = new ConfigurationEdit();
        else
            this.configurationEdit = new ConfigurationEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.configurationEdit = this.configuration = new ConfigurationEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }

    notReceived(row: Configuration) {

        this.alertService.showDialog('Are you sure you  didnt receive the previous serial Number \"' + row.serialKey + '\"?', DialogType.confirm, () => this.retrieveHelper2(row));
    }

    //}
    retrieveHelper2(row: Configuration) {
        this.alertService.startLoadingMessage("Requesting...");
        this.loadingIndicator = true;
        //this.editedStatusConfig = this.configStatusEditor.addConfigStatus(row, this.allConfiguration);
        //this.configStatusModal.show();
        this.configurationService.notReceivedSerial(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.statusFlag = true;
            },
                error => {
                    this.alertService.stopLoadingMessage();
                    this.loadingIndicator = false;

                    this.alertService.showStickyMessage("Request Error", `You have already confirmed that you didnt receive previous S.No  "${Utilities.getHttpResponseMessage(error)}"`,
                        MessageSeverity.error, error);
            });
       
    }

    onConfigStatusModalHidden() {
        this.configStatusEditor.resetForm(true);
    } 

    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newConfiguration(allConfiguration: Configuration[]) {
        this.isGeneralEditor = true;
        this.isNewConfiguration = true;

        this.allConfiguration = [...allConfiguration];
        this.editingConfigurationName = null;
        this.configuration = this.configurationEdit = new ConfigurationEdit();
        //this.posEdit.isEnabled = true;
        this.edit();

        return this.configurationEdit;
    }

    editConfiguration(configuration: Configuration, allConfiguration: Configuration[]) {
        if (configuration) {
            this.serialRef = configuration.serialKey;
            this.isGeneralEditor = true;
            this.isNewConfiguration = false;
            this.editingConfigurationName = configuration.id;
            this.configuration = new Configuration();
            this.configurationEdit = new ConfigurationEdit();
            //this.configurationEdit.serialKey = null;
            Object.assign(this.configuration, configuration);
            Object.assign(this.configurationEdit, configuration);
            this.edit();
            this.selectedLink = "no";
            this.statusFlag = false;
            this.maintenanceFlag = false;
            this.selectedLink1 = "no";
            this.selectedLink2 = "no";
            this.selectedMaintenance = "no";
            //this.loadCurrentInventoryAddData(-1, -1,this.configuration.deploymentRequestId);
            return this.configurationEdit;
        }
        else {
            return this.newConfiguration(allConfiguration);
        }
    }

    onSearchChanged(value: string) {
        this.rowsSerial = this.rowsSerialCache.filter(r => Utilities.searchArray(value, false, r.serialKey));
    }
    deleteConfiguration(row: ConfigurationEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.id + '\"?', DialogType.confirm, () => this.deleteConfigurationHelper(row));
    }


    deleteConfigurationHelper(row: ConfigurationEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.configurationService.deleteConfiguration(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                //this.rowsCache = this.rowsCache.filter(item => item !== row)
                //this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }

    serialConfiguration(row: ConfigurationEdit) {
        this.alertService.showDialog('Are you sure you want to send Serial Number \"' + '' +'' +'to' +' Maintenance' + '\"?', DialogType.confirm, () => this.serialConfigurationHelper(row));
    }


    serialConfigurationHelper(row: ConfigurationEdit) {

        this.alertService.startLoadingMessage("pushing to maintenance...");
        this.loadingIndicator = true;
        this.maintenanceEdit.deploymentRequestId = this.configurationEdit.deploymentRequestId;
        this.maintenanceEdit.applicationVersionId = this.configurationEdit.applicationVersionId;
        this.maintenanceEdit.serialNumberId = this.configurationEdit.serialNumberId;
        this.maintenanceEdit.inventoryAddId = this.configurationEdit.inventoryAddId;
        this.maintenanceEdit.inventoryId = this.configurationEdit.inventoryId;
        this.maintenanceService.newMaintenance(this.maintenanceEdit, this.configurationEdit.serialNumberId)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;
                this.maintenanceFlag = true;
                this.alertService.showMessage("Success", `Serial Number+""+ \"${this.configurationEdit.serialKey}\"has been sent to maintenance`, MessageSeverity.success);
                //this.rowsCache = this.rowsCache.filter(item => item !== row)
                //this.rows = this.rows.filter(item => item !== row)
            },
            error => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.alertService.showStickyMessage("Dublicate Serial Number", `Error: "${Utilities.getHttpResponseMessage(error)}"`,
                    MessageSeverity.error, error);
            });
    }
    serialStock(row: ConfigurationEdit) {
        this.alertService.showDialog('Are you sure you want to send Serial Number \"' + this.configurationEdit.serialKey + '' + 'to' + ' Stock' + '\"?', DialogType.confirm, () => this.serialConfigurationHelper1(row));
    }


    serialConfigurationHelper1(row: ConfigurationEdit) {

        this.alertService.startLoadingMessage("pushing to stock...");
        this.stockFlag = true;
        this.alertService.showMessage("Success", `Serial Number+""+ \"${this.configurationEdit.serialKey}\"has sent to stock`, MessageSeverity.success);
        this.alertService.stopLoadingMessage();
          
    }



    get canViewAllRoles() {
        return this.configurationService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.configurationService.userHasPermission(Permission.assignRolesPermission);
    }
    get canManageUsers() {
        return this.configurationService.userHasPermission(Permission.manageUsersPermission);
    }
}
