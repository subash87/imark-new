﻿
import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AlertService, DialogType, MessageSeverity } from '../../services/alert.service';
import { AppTranslationService } from "../../services/app-translation.service";
import { BankService } from "../../services/bank/bank.service";
import { Utilities } from "../../services/utilities";
import { Permission } from '../../models/permission.model';
import { Bank } from '../../models/bank/bank.model';
import { BankEdit } from '../../models/bank/bank-edit.model';
import { BankEditComponent } from "./bank-edit.component";


@Component({
    selector: 'bank-management',
    templateUrl: './bank.component.html',
    styleUrls: ['./bank.component.css']
})
export class BankComponent implements OnInit, AfterViewInit {
    columns: any[] = [];
    rows: Bank[] = [];
    rowsCache: Bank[] = [];
    editedBank: BankEdit;
    sourceBank: BankEdit;
    //anyType object variable.
    editingBankName: {};
    loadingIndicator: boolean;

    allBank: Bank[] = [];

    @ViewChild('indexTemplate')
    indexTemplate: TemplateRef<any>;

    @ViewChild('bankNameTemplate')
    bankNameTemplate: TemplateRef<any>;

    @ViewChild('phoneTemplate')
    phoneTemplate: TemplateRef<any>;

    @ViewChild('addressTemplate')
    addressTemplate: TemplateRef<any>;

    @ViewChild('contactPerson1Template')
    contactPerson1Template: TemplateRef<any>;

    @ViewChild('contactPerson2Template')
    contactPerson2Template: TemplateRef<any>;

    @ViewChild('contact2Template')
    contact2Template: TemplateRef<any>;

    @ViewChild('statusTemplate')
    statusTemplate: TemplateRef<any>;

    @ViewChild('bankCodeTemplate')
    bankCodeTemplate: TemplateRef<any>;

    @ViewChild('actionsTemplate')
    actionsTemplate: TemplateRef<any>;

    @ViewChild('editorModal')
    editorModal: ModalDirective;

    @ViewChild('bankEditor')
    bankEditor: BankEditComponent;

    constructor(private alertService: AlertService, private translationService: AppTranslationService, private bankService: BankService) {
    }


    ngOnInit() {

        let gT = (key: string) => this.translationService.getTranslation(key);

        this.columns = [
            { prop: "index", name: '#', width: 40, cellTemplate: this.indexTemplate, canAutoResize: false },
            { prop: 'bankName', name: gT('bank.management.BankName'), width: 100, cellTemplate: this.bankNameTemplate },
            { prop: 'bankCode', name: "Bank Code", width: 100, cellTemplate: this.bankCodeTemplate },
            { prop: 'address', name: gT('bank.management.Address'), width: 120, cellTemplate: this.addressTemplate },
            { prop: 'contactPerson1', name: gT('bank.management.contactPerson1'), width: 120, cellTemplate: this.contactPerson1Template },
            { prop: 'contactNo1', name: gT('bank.management.contactNo1'), width: 90, cellTemplate: this.phoneTemplate },
            { prop: 'contactPerson2', name: gT('bank.management.contactPerson2'), width: 120, cellTemplate: this.contactPerson2Template },
            { prop: 'contactNo2', name: gT('bank.management.contactNo2'), width: 120, cellTemplate: this.contact2Template },
            { prop: 'status', name: "Status", width: 80, cellTemplate: this.statusTemplate },
        ];

        if (this.canManageUsers)
            this.columns.push({
                name: '', width: 130, cellTemplate: this.actionsTemplate, resizeable: false,
                canAutoResize: false, sortable: false, draggable: false
            });



        this.addNewBankToList();
    }


    ngAfterViewInit() {

        this.bankEditor.changesSavedCallback = () => {
            this.addNewBankToList();
            this.editorModal.hide();
        };

        this.bankEditor.changesCancelledCallback = () => {
            this.editedBank = null;
            this.sourceBank = null;
            this.editorModal.hide();
        };

    }


    addNewBankToList() {
        if (this.sourceBank) {
            Object.assign(this.sourceBank, this.editedBank);

            let sourceIndex = this.rowsCache.indexOf(this.sourceBank, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rowsCache, sourceIndex, 0);

            sourceIndex = this.rows.indexOf(this.sourceBank, 0);
            if (sourceIndex > -1)
                Utilities.moveArrayItem(this.rows, sourceIndex, 0);

            this.editedBank = null;
            this.sourceBank = null;
        }
        else {
            let bank = new Bank();
            Object.assign(bank, this.editedBank);
            this.editedBank = null;

            let maxIndex = 0;
            for (let u of this.rowsCache) {
                if ((<any>u).index > maxIndex)
                    maxIndex = (<any>u).index;
            }

            (<any>Bank).index = maxIndex + 1;

            this.rowsCache.splice(0, 0, bank);
            this.rows.splice(0, 0, bank);
        }
        this.loadBankData();
    }


    loadBankData() {
        this.alertService.startLoadingMessage();
        this.loadingIndicator = true;
        this.bankService.getBankInfo().subscribe(results => this.onDataLoadSuccessful(results), error => this.onDataLoadFailed(error));
    }


    onDataLoadSuccessful(banks: Bank[]) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        banks.forEach((bank, index, banks) => {
            (<any>bank).index = index + 1;
        });
        this.rowsCache = [...banks];
        this.rows = banks;
    }


    onDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.loadingIndicator = false;

        this.alertService.showStickyMessage("Load Error", `Unable to retrieve users from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);
    }


    onSearchChanged(value: string) {
        this.rows = this.rowsCache.filter(r => Utilities.searchArray(value, false, r.bankName, r.address, r.contactPerson1, r.contactPerson2));
    }

    onEditorModalHidden() {
        this.editingBankName = null;
        this.bankEditor.resetForm(true);
    }


    newBank() {
        this.editingBankName = null;
        this.sourceBank = null;
        this.editedBank = this.bankEditor.newBank(this.allBank);
        this.editorModal.show();
    }


    editBank(row: BankEdit) {
        this.editingBankName = { name: row.bankName };
        this.sourceBank = row;
        this.editedBank = this.bankEditor.editBank(row, this.allBank);
        this.editorModal.show();
    }


    deleteBank(row: BankEdit) {
        this.alertService.showDialog('Are you sure you want to delete \"' + row.bankName + '\"?',
            DialogType.confirm, () => this.deleteBankHelper(row));
    }


    deleteBankHelper(row: BankEdit) {

        this.alertService.startLoadingMessage("Deleting...");
        this.loadingIndicator = true;

        this.bankService.deleteBank(row)
            .subscribe(results => {
                this.alertService.stopLoadingMessage();
                this.loadingIndicator = false;

                this.rowsCache = this.rowsCache.filter(item => item !== row)
                this.rows = this.rows.filter(item => item !== row)
            },
                error => {
                    this.alertService.stopLoadingMessage();
                    this.loadingIndicator = false;

                    this.alertService.showStickyMessage("Delete Error", `An error occured whilst deleting the user.\r\nError: "${Utilities.getHttpResponseMessage(error)}"`,
                        MessageSeverity.error, error);
                });
    }




    get canViewRoles() {
        return this.bankService.userHasPermission(Permission.viewRolesPermission)
    }

    get canManageUsers() {
        return this.bankService.userHasPermission(Permission.manageUsersPermission);
    }

    get canCreateUsers() {
        return this.bankService.userHasPermission(Permission.createUsersPermission);
    }

}
