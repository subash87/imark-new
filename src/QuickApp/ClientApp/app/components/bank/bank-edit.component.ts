﻿
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { AlertService, MessageSeverity } from '../../services/alert.service';
import { BankService } from "../../services/bank/bank.service";
import { Utilities } from '../../services/utilities';
import { Bank } from '../../models/bank/bank.model';
import { BankEdit } from '../../models/bank/bank-edit.model';
import { Role } from '../../models/role.model';
import { Permission } from '../../models/permission.model';


@Component({
    selector: 'bank-edit',
    templateUrl: './bank-edit.component.html',
    styleUrls: ['./bank.component.css']
})
export class BankEditComponent implements OnInit {
    private isEditMode = false;
    private isNewBank = false;
    private isSaving = false;
    private isEditingSelf = false;
    private showValidationErrors = false;
    private editingBankName: string;
    private uniqueId: string = Utilities.uniqueId();
    private bank: Bank = new Bank();
    private allBank: Bank[] = [];
    private bankEdit: BankEdit;

    public formResetToggle = true;

    public changesSavedCallback: () => void;
    public changesFailedCallback: () => void;
    public changesCancelledCallback: () => void;

    @Input()
    isViewOnly: boolean;

    @Input()
    isGeneralEditor = false;

    @ViewChild('f')
    private form;

    //ViewChilds Required because ngIf hides template variables from global scope
    @ViewChild('bankName')
    private bankName;

    @ViewChild('contactNo1')
    private contactNo1;

    @ViewChild('address')
    private address;      

    @ViewChild('contactNo2')
    private contactNo2;

    @ViewChild('contactPerson1')
    private contactPerson1;

    @ViewChild('contactPerson2')
    private contactPerson2;

    @ViewChild('status')
    private status;

    @ViewChild('bankCode')
    private bankCode;

    statuses = [
        { value: 1, text: 'Rental' },
        { value: 2, text: 'Partial Rental' },
        { value: 3, text: 'Both' },
    ];

    constructor(private alertService: AlertService, private bankService: BankService) {
    }

    ngOnInit() {
        //isGeneralEditor for filtering directives..
        if (!this.isGeneralEditor) {
            this.loadCurrentBankData();
            
        }
    }



    private loadCurrentBankData() {
        this.alertService.startLoadingMessage();

        if (this.canViewAllRoles) {
            this.bankService.getBank().subscribe(results => this.onCurrentUserDataLoadSuccessful(results), error => this.onCurrentUserDataLoadFailed(error));
        }

    }


    private onCurrentUserDataLoadSuccessful(bank: Bank) {
        this.alertService.stopLoadingMessage();
        this.bank = bank;

    }

    private onCurrentUserDataLoadFailed(error: any) {
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Load Error", `Unable to retrieve user data from the server.\r\nErrors: "${Utilities.getHttpResponseMessage(error)}"`,
            MessageSeverity.error, error);

        this.bank = new Bank();
    }


    private showErrorAlert(caption: string, message: string) {
        this.alertService.showMessage(caption, message, MessageSeverity.error);
    }



    private edit() {
        if (!this.isGeneralEditor) {
            this.isEditingSelf = true;
            this.bankEdit = new BankEdit();
            Object.assign(this.bankEdit, this.bank);
        }
        else {
            if (!this.bankEdit)
                this.bankEdit = new BankEdit();

            this.isEditingSelf = this.bankService.updateBank(this.bank) ? this.bankEdit.id == this.bank.id : false;
        }

        this.isEditMode = true;
        this.showValidationErrors = true;
    }


    private saveBank() {
        this.isSaving = true;
        this.alertService.startLoadingMessage("Saving changes...");

        if (this.isNewBank) {
            this.bankService.newBank(this.bankEdit).subscribe(user => this.saveSuccessHelper(user), error => this.saveFailedHelper(error));
        }
        else {
            this.bankService.updateBank(this.bankEdit).subscribe(response => this.saveSuccessHelper(), error => this.saveFailedHelper(error));
        }
    }


    private saveSuccessHelper(bank?: Bank) {
        //this.testIsBankChanged(this.bank, this.bankEdit);

        if (bank)
            Object.assign(this.bankEdit, bank);

        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        //this.showValidationErrors = false;
        Object.assign(this.bank, this.bankEdit);
        this.bankEdit = new BankEdit();
        this.resetForm();


        if (this.isGeneralEditor) {
            if (this.isNewBank)
                this.alertService.showMessage("Success", `User \"${this.bank.bankName}\" was created successfully`, MessageSeverity.success);
            else if (!this.isEditingSelf)
                this.alertService.showMessage("Success", `Changes to user \"${this.bank.bankName}\" was saved successfully`, MessageSeverity.success);
        }

        this.isEditMode = false;


        if (this.changesSavedCallback)
            this.changesSavedCallback();
    }


    private saveFailedHelper(error: any) {
        this.isSaving = false;
        this.alertService.stopLoadingMessage();
        this.alertService.showStickyMessage("Save Error", "The below errors occured whilst saving your changes:", MessageSeverity.error, error);
        this.alertService.showStickyMessage(error, null, MessageSeverity.error);

        if (this.changesFailedCallback)
            this.changesFailedCallback();
    }



    private testIsBankChanged(bank: Bank, editedBank: Bank) {

        //let posAdded = this.isNewPos ? editedPos.brand : editedPos.brand;
        //let posRemoved = this.isNewPos ? [] : pos.brand.(role => editedPos.brand.indexOf(role) == -1);

        //let modifiedRoles = rolesAdded.concat(rolesRemoved);

        //if (modifiedRoles.length)
        //    setTimeout(() => this.accountService.onRolesUserCountChanged(modifiedRoles));
    }



    private cancel() {
        if (this.isGeneralEditor)
            this.bankEdit = this.bank = new BankEdit();
        else
            this.bankEdit = new BankEdit();

        this.showValidationErrors = false;
        this.resetForm();

        this.alertService.showMessage("Cancelled", "Operation cancelled by user", MessageSeverity.default);
        this.alertService.resetStickyMessage();

        if (!this.isGeneralEditor)
            this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }


    private close() {
        this.bankEdit = this.bank = new BankEdit();
        this.showValidationErrors = false;
        this.resetForm();
        this.isEditMode = false;

        if (this.changesCancelledCallback)
            this.changesCancelledCallback();
    }




    resetForm(replace = false) {
        this.isSaving = false;

        if (!replace) {
            this.form.reset();
        }
        else {
            this.formResetToggle = false;

            setTimeout(() => {
                this.formResetToggle = true;
            });
        }
    }


    newBank(allBank: Bank[]) {
        this.isGeneralEditor = true;
        this.isNewBank = true;

        this.allBank = [...allBank];
        this.editingBankName = null;
        this.bank = this.bankEdit = new BankEdit();
        //this.posEdit.isEnabled = true;
        this.edit();

        return this.bankEdit;
    }

    editBank(bank: Bank, allBank: Bank[]) {
        if (bank) {
            this.isGeneralEditor = true;
            this.isNewBank = false;
            this.editingBankName = bank.bankName;
            this.bank = new Bank();
            this.bankEdit = new BankEdit();
            Object.assign(this.bank, bank);
            Object.assign(this.bankEdit, bank);
            this.edit();

            return this.bankEdit;
        }
        else {
            return this.newBank(allBank);
        }
    }





    get canViewAllRoles() {
        return this.bankService.userHasPermission(Permission.viewRolesPermission);
    }

    get canAssignRoles() {
        return this.bankService.userHasPermission(Permission.assignRolesPermission);
    }
}
