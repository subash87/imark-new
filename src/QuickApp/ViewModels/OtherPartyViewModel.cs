﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class OtherPartyViewModel
    {
        public Guid Id { get; set; }
        public string PartyName { get; set; }
        public string PartyAddress { get; set; }
        public string PhoneNo { get; set; }
    }

}
