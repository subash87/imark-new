﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class ApplicationVersionViewModel
    {
        public Guid Id { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public Guid? BankId { get; set; }
        public string BankCode { get; set; }
        public string ApplicationVer { get; set; }
        public string KernelType { get; set; }
        public string Remarks { get; set; }
    }
}
