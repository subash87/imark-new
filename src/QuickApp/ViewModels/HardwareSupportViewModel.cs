﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models;

namespace QuickApp.ViewModels
{
    public class HardwareSupportViewModel
    {
        public Guid Id { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string SerialNo { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string Manufacturer { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }        
        public Guid BankId { get; set;}
        public string BankName { get; set;}
    }

}
