﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class SerialNumberViewModel
    {
        public Guid Id { get; set; }
        public string SerialKey { get; set; }
        public Guid InventoryAddId { get; set; }
        public bool IsSold { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string BankName { get; set; }
        public string Status { get; set; }
        public string MaintenanceSerial { get; set; }
        public int TotalSerialKey { get; set; }
        public int RemainingSerialKey { get; set; }
        //For stock
        public Guid SerialNumberId { get; set; }
        public bool IsDeployed { get; set; }

    }
}
