﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class OtherStockViewModel
    {
        public Guid Id { get; set; }
        public Guid? DeploymentRequestId { get; set; }
        public string DeploymentRequest { get; set; }
        public Guid InventoryAddId { get; set; }
        public string InventoryAdd { get; set; }
        public Guid? OtherPartyId { get; set; }
        public string OtherParty { get; set; }
        public DateTime StockDate { get; set; }
        public int SoldOut { get; set; }
        public string Remarks { get; set; }
        public string ReceivedBy { get; set; }
        public string DistributedBy { get; set; }

        public string ContactNo { get; set; }
    }
    public class OtherStockAddViewModel
    {
        public Guid Id { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public string DeploymentRequest { get; set; }
        public Guid InventoryAddId { get; set; }
        public string InventoryAdd { get; set; }
        public Guid? OtherPartyId { get; set; }
        public string OtherParty { get; set; }
        public DateTime StockDate { get; set; }
        public int TotalNumber { get; set; }
        public DateTime BuyDate { get; set; }
        public Guid BankId { get; set; }
        public string BankName { get; set; }
        public Guid InventoryId { get; set; }
        public string Inventory { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public bool HasSerialNo { get; set; }
        public string PartyName { get; set; }
        public string PartyAddress { get; set; }
        public string PhoneNo { get; set; }
        public int SoldOut { get; set; }
        public string Remarks { get; set; }
        public int Remaining { get; set; }
        public string Merchant { get; set; }
        public string IdMerchant { get; set; }
        public string Outlet { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string IdTerminal { get; set; }
        public string TerminalType { get; set; }
        public string CreatedBy { get; set; }
        public string ReceivedBy { get; set; }
        public string DistributedBy { get; set; }


    }
}
