﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class MaintenanceViewModel
    {
        public Guid Id { get; set; }
        public Guid SerialNumberId { get; set; }
        public string SerialKey { get; set; }
        public string SerialNumber { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public Guid ApplicationVersionId { get; set; }
        public string Status { get; set; }
        public string Terminate { get; set; }
        public string Issue { get; set; }
        public string ResolvedBy { get; set; }
        public string Remarks { get; set; }
        public string Verification { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public ItemRowViewModel ItemRow { get; set; }
        public IEnumerable <MaintenanceIssueViewModel> MaintenanceVM { get; set; }
        //public IEnumerable<MaintenanceIssueViewModel> MaintenanceIssue { get; set; }
        public IEnumerable<ItemRowViewModel> ItemRows { get; set; }
        public Guid InventoryId { get; set; }
        public Guid InventoryAddId { get; set; }
        public string InventoryName { get; set; }
        public string Bank { get; set; }
        public string Merchant { get; set; }
        public DateTime DeployDate { get; set; }
        public DateTime BuyDate { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string BankName { get; set; }
        public string IdTerminal { get; set; }
        public string IdMerchant { get; set; }
        public string BankCode { get; set; }
        public string Item { get; set; }
        public string ApplicationFormat { get; set; }
 

        public string Outlet { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public DateTime CreatedDate { get; set; }

    }
    public class ItemRowViewModel
    {
        public IEnumerable<ItemRowsViewModel> ItemRows { get; set; }
   

    }
    public class ItemRowsViewModel
    {
        public Int32 Id { get; set; }
        public Guid MaintenanceId { get; set; }
        public string Issue { get; set; }
        public MaintenanceViewModel MaintenanceViewModel { get; set; }
        public string Remarks { get; set; }
        //public string Status { get; set; }
        //public string Terminate { get; set; }
        //public string ResolvedBy { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

    }
    public class MaintenanceIssueViewModel
    {
        public Int32 Id { get; set; }
        public string Issue { get; set; }
        public string Remarks { get; set; }
        public MaintenanceViewModel MaintenanceViewModel { get; set; }
        //public ItemRowViewModel ItemRowVm { get; set; }
        public ItemRowViewModel ItemRowViewModel { get; set; }
        public Guid MaintenanceId { get; set; }

    }
}
