﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class PosStockViewModel
    {
        public Guid Id { get; set; }
        public Guid BankId { get; set; }
        public string BankName { get; set; }
        public string TerminalType { get; set; }
        public Guid SerialNumberId { get; set; }
        public Guid InventoryId { get; set; }
        public Guid InventoryAddId { get; set; }
        public Guid? DeploymentId { get; set; }
        public string SerialNumber { get; set; }
        public string SerialKey { get; set; }
        public DateTime StockDate { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string Location { get; set; }
        public string CreatedBy { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string BankStatus { get; set; }
        public int Total { get; set; }
        public int Deployed { get; set; }



    }
}
