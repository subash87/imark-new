﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class ConfigurationViewModel
    {
        public Guid Id { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public string Verification { get; set; }
        public Guid SerialNumberId { get; set; }
        public string SerialKey { get; set; }
        public bool IsSale { get; set; }
        public string PreviousSerial { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public Guid ApplicationVersionId { get; set; }
        public string BankCode { get; set; }
        public DateTime? ApplicationDate { get; set; }
        public string ApplicationVersion { get; set; }
        public string KernelType { get; set; }
        public string Remarks { get; set; }
        public string StatusName { get; set; }
        public Guid InventoryId { get; set; }
        public Guid InventoryAddId { get; set; }
        public string Status { get; set; }
        public string RequestName { get; set; }
        public string Version { get; set; }
        public int TotalConfigSerial { get; set; }


    }
}
