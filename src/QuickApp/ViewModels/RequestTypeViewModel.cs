﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class RequestTypeViewModel
    {
        public Guid Id { get; set; }
        public string RequestName { get; set; }
    }

}
