﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class SupportViewModel
    {
        public Guid Id { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public bool Phone { get; set; }
        public bool Field { get; set; }
        public bool Status { get; set; }
        public bool Priority { get; set; }
        public string Issue { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string ResolvedBy { get; set; }
        public string Remarks { get; set; }
        public string Verification { get; set; }
        public string VerifiedBy { get; set; }
        public DateTime VerifiedDate { get; set; }
        public string BankName { get; set; }
        public string Merchant { get; set; }
        public string MerchantLocation { get; set; }
        public string IdMerchant { get; set; }
        public string Outlet { get; set; }
        public string IdTerminal { get; set; }
        public string Address { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string SerialKey { get; set; }
        public string Item { get; set; }
        public Guid ApplicationVersionId { get; set; }
        public Guid? BankId { get; set; }
        public string BankCode { get; set; }
        public string ApplicationVer { get; set; }
        public string ApplicationDate { get; set; }
        public string KernelType { get; set; }
        public string ApplicationFormat { get; set; }
        public string ReceivedBy { get; set; }
        public string ReceivedContactNo { get; set; }
        public DateTime SupportDate { get; set; }

    }
}
