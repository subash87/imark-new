﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickApp.ViewModels
{
    public class InventoryAddViewModel
    {
        public Guid Id { get; set; }
        public int TotalNumber { get; set; }
        public DateTime BuyDate { get; set; }
        public string Remarks { get; set; }
        public Guid InventoryId { get; set; }
        public string Inventory { get; set; }
        public int RemainingNumber { get; set; }
        public string PrimaryItem { get; set; }
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
    }
    public class InventoryandAddViewModel
    {
        public Guid Id { get; set; }
        public int TotalNumber { get; set; }
        public int TotalSoldDeploy { get; set; }
        public string Item { get; set; }
        public int RemainingNumber { get; set; }
        public int SoldNumber { get; set; }
        public int SoldOut { get; set; }
        public int DeployNumber { get; set; }
        public int MaintenanceNumber { get; set; }
        public int NotReceivedNumber { get; set; }
        public int DamageNumber { get; set; }
        public int AvailableAfterConfiguration { get; set; }
        public int AvailableAfterSold { get; set; }
        public int AvailableAfterDeploy { get; set; }
        public int Available { get; set; }
        public int PendingDeploy { get; set; }
        public int ConfigureNumber { get; set; }
        public int PendingDeploymentRequest { get; set; }
        public int ReConfigureNumber { get; set; }
        public DateTime BuyDate { get; set; }
        public string Remarks { get; set; }
        public Guid DeploymentRequestId { get; set; }
        public Guid InventoryId { get; set; }
        public Guid BankId { get; set; }
        public Guid SerialNumberId { get; set; }
        public string SerialNumber { get; set; }
        public int SerialCount { get; set; }
        public string SerialKey { get; set; }
        public Guid InventoryAddId { get; set; }
        public string Inventory { get; set; }
        public string PrimaryItem { get; set; }
       
        public string SecondaryItem { get; set; }
        public string TertiaryItem { get; set; }
        public string DetailItem { get; set; }
        public string MainItem { get; set; }
        public string SubItem { get; set; }
        public string ThirdItem { get; set; }
        public string DetailInventoryAddItem { get; set; }

        public bool HasSerialNo { get; set; }
        public int Stock { get; set; }
        public int UnDeployed { get; set; }
        public int Deployed { get; set; }
        public int Total { get; set; }
        public string BankName { get; set; }
        public string BankStatus { get; set; }

    }
}
